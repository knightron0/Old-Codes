#include <iostream>
using namespace std;

int main(){
    long long n = 0;
    long long k = 0;
    long long totalk = 0;
    cin>>n>>k;
    totalk = k + 1;
    long long dip = 0;
    long long cert = 0;
    long long left = 0;
    long long half = n/2;
    for(long long i = half;i > 0;i--){
        if(i % totalk == 0){
            dip = i / totalk;
            cert = i - dip;
            break;
        }
    }
    left = n-(dip + cert);
    cout<<dip<<" "<<cert<<" "<<left;
    return 0;
}
