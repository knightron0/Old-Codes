#include <iostream>
using namespace std;

int main(){
    int k;
    cin>>k;
    int ans[k];
    for(int i = 0;i<k;i++){
        int f1 = 0;
        int f2 = 0;
        cin>>f1;
        cin>>f2;
        int temp1;
        int temp2;
        int ar1 = 0;
        int ar2 = 0;
        for(int j = 0;j<f1;j++){
            cin>>temp1;
            ar1 += temp1;
            temp1 = 0;
        }
        for(int j = 0;j<f2;j++){
            cin>>temp2;
            ar2 += temp2;
            temp2 = 0;
        }
        if(ar1 > ar2){
            ans[i] = 0;
        } else {
            ans[i] = 1;
        }
    }
    for(int i = 0;i<k;i++){
        if(ans[i] == 0){
            cout<<"Snow";
        } else {
            cout<<"Death";
        }
    }
    return 0;
}

