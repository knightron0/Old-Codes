#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;
int prefixsum(int arr[],int n,int index){
    int toreturn = 0;
    for(int i = 0;i<index;i++){
        toreturn += arr[i];
    }
    return toreturn;
}
int suffixsum(int arr[],int n,int index){
    int toreturn1 = 0;
    for(int i = (n - (n - index + 1));i<n;i++){
        toreturn1 += arr[i];
    }
    return toreturn1;
}

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    int t;
    cin>>t;
    for(int l = 0;l<t;l++){
        int k = 0;
        int n = 0;
        int ans = 0;
        cin>>n;
        int a[n];
        for(int i = 0;i<n;i++){
            cin>>a[i];
        }
        for(int i = 0;i<n;i++){
            if(k > (suffixsum(a,n,i) + prefixsum(a,n,i))){
                k = (suffixsum(a,n,i) + prefixsum(a,n,i));
                ans = i;
            }
        }
        cout<<(suffixsum(a,n,1) + prefixsum(a,n,1));

    }
    return 0;
}
