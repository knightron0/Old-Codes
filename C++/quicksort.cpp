#include <iostream>
#include <algorithm>
#include <vector>
#include<string>
using namespace std;


string quicksort(int n, vector<int> & f){
    vector<int> right;
    vector<int> left;
    string ans;
    if(n > 1){
        int mid = (int)f.size()/2;
        for(int i = 0; i<= mid;i++){
              if(i != mid){
                  if(f[i] >= mid){
                      right.push_back(f[i]);
                  } else {
                      left.push_back(f[i]);
                  }
              }
        }
        if (left.size() > 0){
            ans += quicksort(left.size(),left);
            ans += " ";
        }
        ans += to_string(f[mid]);
        ans += " ";
        if (right.size() > 0){
            ans += quicksort(right.size(),right);
            ans += " ";
        }
        return ans;
    } else {
        ans += to_string(f[n-1]);
        ans += " ";
        return ans;
    }
}
int main(){
    int sizeofarray;
    vector<int> ar;
    cin >> sizeofarray;
    int a[sizeofarray];
    for(int i= 0; i<sizeofarray;i++){
        cin>>a[i];
    }
    for(int i= 0; i<sizeofarray;i++){
        ar.push_back(a[i]);
    }
    cout<<quicksort(sizeofarray, ar)<<endl;
}
