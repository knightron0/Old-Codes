#include <iostream>
using namespace std;

int main(){
    int t;
    cin>>t;
    for(int l = 0; l < t;l++){
        int n = 0;
        int ans = 0;
        cin>>n;
        int a[n];
        for(int i = 0; i < n;i++){
            cin>>a[i];
        }
        for(int i = 1; l < n-1;i++){
            if(a[i-1] > a[i] && a[i] < a[i+1] ){
                ans += 1;
            }
        }
        cout<<ans;
    }
    return 0;
}
