#include <iostream>
#include <cmath>
#include <math.h>
#include <stdio.h>
using namespace std;

int main(){
    long long n,k;
    cin>>n>>k;
    long long a[n];
    for(long long i = 0;i<n;i++){
        cin>>a[i];
    }
    long long ans;
    for(long long i = 0;i<n;i++){
        for(long long j = i+1;j<n;j++){
            if(abs(a[i]-a[j]) >= k){
                ans += 1;
            }
        }
    }
    cout<<ans;
}
