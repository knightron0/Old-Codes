#include <iostream>
#include <vector>
using namespace std;

int main(){
    int n;
    cin>>n;
    int num[n];
    for(int i = 0; i< n;i++){
        cin>>num[i];
    }
    int counts;
    for(int i = 0; i<n;i++){
        if(num[i] == 1){
            counts++;
        }
    }
    int maxn = 0;
    int tenn = 0;
    for(int i = 0; i<n;i++){
        if(num[i] == 0){
            tenn++;
            if(maxn<tenn){
                maxn = tenn;
            }
        } else {
            if(maxn<tenn){
                maxn = tenn;
            }
            tenn = 0;
        }

    }
    cout<<maxn + counts<<endl;
    return 0;
}
