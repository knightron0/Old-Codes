#include <iostream>
#include <math.h>
using namespace std;

long long check(long long k){
    long long check = 1;
    for(int i = 0;i <= floor(sqrt(k)); i++){
        if(pow(2,i) == k){
            check = 0;
        } else if (check == 1){
            check = 1;
        }
    }
    return check;
}
int main(){
    long long t;
    cin>>t;
    long long ans = 0;
    long long a[t];
    for(long long i = 0;i < t;i++){
        cin>>a[i];
    }
    for (long long i = 0;i < t;i++){
        for(long long j = 1;j<=a[i];j++){
            if(check(j) == 0){
                ans += -j;
            } else if(check(j) == 1){
                ans += j;
            }
        }
        cout<<ans<<endl;
    }
    return 0;
}
