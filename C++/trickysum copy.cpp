#include <iostream>
#include <cmatch>
#include <math.h>
using namespace std;

int isPowerOfTwo(long long n) {
  if (n == 0){
    return 0;
  }
  while (n != 1) {
    if (n%2 != 0){
        return 0;
    }
    n = n/2;
  }
  return 1;
}

int main(){
    int t;
    cin>>t;
    for (int i = 0; i< t;i++){
        long long ans;
        long long n;
        cin>>n;
        for (long long j = 0;j<n;j+=){
            if(isPowerOfTwo(j) == 1){
                ans -= j;
            }
            else {
                ans += j;
            }
        }
        cout<<ans<<endl;
    }
    return 0;

}
