#include <iostream>
using namespace std;

int main(){
    int t;
    cin>>t;
    for(int l = 0;l<t;l++){
        int p = 0;
        int k = 0;
        int n = 0;
        int ans = 0;
        cin>>p>>k>>n;
        int boxes = 0;
        for(int i = p;i <= n;i+=p){
            boxes += 1;
            ans += 1;
        }
        while (boxes >= k){
            for(int i = boxes;i >= k;i -= k){
                boxes -= k;
                boxes += 1;
                ans += 1;
            }

        }
        cout<<ans<<endl;
    }
}
