#include <iostream>
using namespace std;

int main(){
    int n;
    int ans = 1;
    cin>>n;
    if (n > 0){
        for(int i = 1;i<=n;i++){
            if (i % 2 != 0){
                ans = ans*2;
            } else {
                ans = ans + 1;
            }
        }
    }
    cout << ans <<" m"<< endl;
    return 0;
}
