#include <iostream>
using namespace std;

unsigned long factorial(int k){
    unsigned long ans;
    if(k==1){
        return 1;
    } else if(k != 0){
        ans = k * factorial(k-1);
        return ans;
    }
}
int main(){
    int x;
    cin>>x;
    cout<<factorial(x)<<endl;
    return 0;
}
