#include <cmath>
#include <iostream>
#include <string>
#include <vector>
using namespace std;
int isprime(int num){
    int counts = 0;
    for (int i = 2; i < num ; i++) {
		if (num % i == 0) {
			return 1;
		}
	}
	return 0;
}
string findfactors(int multiple){
    string ans;
    for(int i = 2;i<= multiple;i++){
        if(multiple % i == 0){
            if(isprime(i) == 0){
                ans += to_string(i);
                ans += " ";
            }
        }
    }
    return ans;
}
int main(){
    for(int i = 2; i<= 100000; i++){
        cout<<i<<": "<<findfactors(i)<<endl;
    }
}
