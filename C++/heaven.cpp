#include <iostream>
#include <cmath>
#include <math.h>
using namespace std;

int main(){
    int t;
    cin>>t;
    for(int l = 0;l<t;l++){
        int n;
        int v1;
        int v2;
        float time1;
        float time2;
        cin>>n>>v1>>v2;
        time2 = (n)/v2;
        time1 = ((sqrt(2)*n))/v1;
        cout<<time1<<"  "<<time2<<endl;
        if(time2<time1){
            cout<<"Elevator"<<endl;
        } else {
            cout<<"Stairs"<<endl;
        }
    }
    return 0;
}
