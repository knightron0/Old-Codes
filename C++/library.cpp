#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main(){
    int n;
    cin>>n;
    int book;
    vector<int>books;
    for(int i = 0;i< n;i++){
        book = 0;
        cin>>book;
        books.push_back(book);
    }
    int k;
    cin>>k;
    vector<int>taken;
    int temp;
    for(int i = 0;i< k;i++){
        temp = 0;
        cin>>temp;
        taken.push_back(temp);
    }
    vector<int> ans;
    for (int i = 0; i< taken.size();i++){
        ans.push_back(books[taken[i]-1]);
        books.erase(books.begin()+i);
    }
    for (int i = 0; i< ans.size();i++){
        cout<<ans[i]<<endl;
    }
}
