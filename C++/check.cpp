#include <iostream>
using namespace std;
int findCandidate(int a[], int size)
{
    int maj_index = 0, count = 1;
    int i;
    for (i = 1; i < size; i++)
    {
        if (a[maj_index] == a[i])
            count++;
        else
            count--;
        if (count == 0)
        {
            maj_index = i;
            count = 1;
        }
    }
    return a[maj_index];
}

int main(){
    int k;
    cin>>k;
    int a[k];
    for(int i= 0; i< k;i++){
        cin>>a[i];
    }
    cout<<findCandidate(a, k);
}
