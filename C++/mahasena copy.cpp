#include <iostream>
using namespace std;

int main(){
    int t;
    cin>>t;
    int a[t];
    for(int i = 0;i<t;i++){
        cin>>a[i];
    }
    int lucky = 0;
    int unlucky = 0;
    for(int i = 0;i<t;i++){
        if(a[i] % 2 == 0){
            lucky = lucky+ 1;
        } else {
            unlucky = unlucky + 1;
        }
    }
    if(lucky > unlucky){
        cout<<"READY FOR BATTLE"<<endl;
    } else {
        cout<<"NOT READY"<<endl;
    }
    return 0;
}
