#include <iostream>
using namespace std;

int main(){
    int t;
    cin>>t;
    for(int l = 0;l<t;l++){
        int n;
        cin>>n;
        int ques[n];
        int ans[n];
        for(int i = 0;i<n;i++){
            cin>>ques[i];
        }
        for(int i = 0;i<n;i++){
            cin>>ans[i];
        }
        int real[n];
        for(int i = 0;i<n;i++){
            if(ques[i] % 2 == 0){
                real[i] = 0;
            } else {
                real[i] = 1;
            }
        }
        int tf = 0;
        for(int i = 0;i<n;i++){
            if(real[i] == ans[i]){
                tf += 1;
            }
        }
        int realans = -1;
        if (n == tf) {
            realans = -1;
        }
        int countl = 0;
        for(int i = 1;i<n;i++){
            if(real[i] != ans[i] and ans[i-1] == 1){
                countl += 1;
                realans = i;
            }
        }
        if(countl > 1){
            realans = -1;
        }
        cout<<realans;
    }
    return 0;
}
