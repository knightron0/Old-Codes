#include <iostream>
#include <cmath>
#include <math.h>
using namespace std;

int main(){
    int n;
    cin>>n;
    int a[n];
    for(int i = 0;i<n;i++){
        cin>>a[i];
    }
    int ans = 0;
    for(int i = 0;i<n;i++){
        for(int j = i+1;j<n;j++){
            ans += abs(a[i]-a[j]);
        }
    }
    cout<<ans<<endl;
    return 0;
}
