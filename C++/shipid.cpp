#include <iostream>
using namespace std;

int main(){
    int t;
    cin>>t;
    for(int i = 0;i<t;i++){
        char l;
        cin>>l;
        if (l == 'D' || l == 'd'){
            cout<<"Destroyer"<<endl;
        } else if (l == 'F' || l == 'f'){
            cout<<"Frigate"<<endl;
        } else if (l == 'C' || l == 'c'){
            cout<<"Cruiser"<<endl;
        } else if (l == 'B' || l == 'b'){
            cout<<"BattleShip"<<endl;
        }
    }
    return 0;
}
