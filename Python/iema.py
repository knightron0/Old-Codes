t = int(input())
vowels = ["a","i","e","o","u"]
for h in range(0,t):
    n,k = input().split()
    s = input()
    k = int(k)
    vwls = 0
    for i in s:
        if i in vowels:
            vwls += 1
    if k < vwls:
        print("Good")
    else:
        print("Bad")
