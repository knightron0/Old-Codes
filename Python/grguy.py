n,k = input().split()
n = int(n)
k = int(k)
a= input().split()
c = input().split()
for i in range(0,n):
    a[i] = int(a[i])
for i in range(0,len(c)):
    c[i] = int(c[i])
inhand = False
pos = 0
for i in c:
    if i == 1:
        if pos != 0:
            pos -= 1
    elif i == 2:
        if pos != n-1:
            pos += 1
    elif i == 3:
        if a[pos] > 0 and inhand == False:
            inhand = True
            a[pos] -= 1
    elif i == 4:
        if a[pos] != k and inhand == True:
            inhand = False
            a[pos] += 1
    elif i == 0:
        break
ans = ""
for i in a:
    ans += str(i)
    ans += " "
print(ans)
