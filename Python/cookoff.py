t = int(input())
for l in range(t):
    n = int(input())
    cw = False
    spl = False
    es = False
    me = False
    hd = False
    for i in range(n):
        temp = str(input())
        if temp == "easy" and es == False:
            es = True
        elif temp == "simple" and spl == False:
            spl = True
        elif temp == "cakewalk" and cw == False:
            cw = True
        elif temp == "hard" or temp == "medium-hard":
            hd = True
        elif temp == "medium" or temp == "easy-medium":
            me = True
    if es == True and spl == True and cw == True and me == True and hd == True:
        print("Yes")
    else:
        print("No")

