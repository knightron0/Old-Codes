def isPowerOfTwo(n):
    n = int(n)
    if n == 0:
        return 0
    while n != 1:
        if n%2 != 0:
            return 0
        n = n/2
        n = int(n)
    return 1
t = input()
for i in range(int(t)):
    n = input()
    ans = 0
    for j in range(1,int(n)+1):
        if isPowerOfTwo(j) == 1:
            ans -= j
        else:
            ans += j
    print(ans)
