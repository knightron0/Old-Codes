import pygame
from pygame.locals import *

from random import randint

pygame.init()
screen = pygame.display.set_mode((400, 300))
done = False
blue =  (  0,   0, 255)
screen.fill((0,0,0))
while not done:
        for event in pygame.event.get():
                if event.type == pygame.QUIT:
                        done = True
        pygame.draw.circle(screen, blue, (60, 250), 40)
        screen.fill((0, 0, 0))
        pygame.display.update
        
