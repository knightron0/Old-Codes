t = int(input())
for l in range(t):
    n,k = input().split()
    n = int(n)
    k = int(k)
    s = []
    s = input().split()
    xs = []
    b = []
    e = []
    imps = []
    for i in range(0,n):
        if s[i] == "X":
            xs.append(i)
        if s[i] == "Z":
            for j in range(len(xs)-1,-1,-1):
                if (i - xs[j] + 1) % 3 == 0:
                    b.append(xs[j])
                    e.append(i)
    for i in range(n-k+1):
        imps.append(0)
        for j in range(0,len(b)):
            if i <= b[j] and b[j] <= i+k-1 or  i <= e[j] and e[j] <= i+k-1:
                imps[i] +=1
            elif b[j] <= i and i <= e[j]:
                imps[i] += 1
    print(min(imps))
