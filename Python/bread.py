t = int(input())
for l in range(t):
    n,k = input().split()
    n = int(n)
    k = int(k)
    a = input().split()
    sums = len(a)
    for i in a:
        sums += int(i)
        
    if sums % k == 1:
        print(int((sums - 1)/k))
    else:
        print(int((sums + (sums%k)) / k))
    
