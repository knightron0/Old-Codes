size = int(input())
array = []
array = input().split()
def fastsort(size, array):
    mid = int(size/2)
    left = []
    right = []
    ans = ""
    if len(array) > 1:
        for i in range(0, size):
            if i != mid:
                if int(array[i]) >= int(array[mid]):
                    right.append(array[i])
                else:
                    left.append(array[i])
        ans += str(fastsort(len(left), left))
        ans += " "
        ans += str(array[mid])
        ans += " "
        ans += str(fastsort(len(right), right))
        ans += " "
        return ans
    elif len(array) == 1:
        ans += str(array[0])
        return ans
    else:
        return ""
print(fastsort(size,array))
