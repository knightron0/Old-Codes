def reverse(n,starts,ends):
    start = starts-1
    end = ends-1
    n = str(n)
    revn = n[:start]
    for i in range(end,start-1,-1):
        revn += n[i]
    revn += n[end+1:]
    return revn
string = input()
t = int(input())
for l in range(t):
    i,j,k,l = input().split()
    i = int(i)
    j = int(j)
    k = int(k)
    l = int(l)
    after = reverse(string,i,j)
    tocheck = after[k-1:l]
    toafter = reverse(tocheck,1,len(tocheck))
    if tocheck == toafter:
        print("Yes")
    else:
        print("No")

