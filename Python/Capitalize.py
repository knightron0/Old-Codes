s = input("String: ")
in_space = False
revs = ""
for x in s:
    if x == s[0]:
        revs += x.upper()
        in_space = False
    elif x == " ":
        in_space = True
        revs += x
    elif x != " " and in_space == True:
        revs += x.upper()
        in_space = False
    else:
        revs += x.lower()
        in_space = False
print(revs)
