import math

t = int(input())
for i in range(0,t):
    n,k = input().split()
    n = int(n)
    k = int(k)
    gcd = math.gcd(n,k)
    lcm = (n*k)/math.gcd(n,k)
    print(gcd,int(lcm))
