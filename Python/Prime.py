num = int(input("Number: "))
num1 = int(input("Another number: "))
isPrime = False
Finalpri = []

def factorize(n):
    pricount = {}
    for i in range(2, n+1):
        factorCount = 0
        while n % i == 0:
            factorCount += 1
            n = n // i
        isPrime = True
        for j in range(2, i): 
            if i % j == 0:
                isPrime = False
                break
        if isPrime and factorCount > 0:
            pricount[i] = factorCount
        if num == 1:
            break
    return pricount

def lcm(n1, n2):
    factor1 = factorize(n1)
    factor2 = factorize(n2)
    allfactor = {}

    for f in factor1:
        if f in factor2:
            allfactor[f] = max(factor1[f], factor2[f])
        else:
            allfactor[f] = factor1[f]
    for f in factor2:
        if f not in factor1:
            allfactor[f] = factor2[f]

    lcmNum = 1
    for f in allfactor:
        lcmNum = lcmNum * (f**allfactor[f])
    return lcmNum

def hcf(num, num1):
    allfactors = {}
    factor1 = factorize(num)
    factor2 = factorize(num1)
    for s in factor1:
        if s in factor2:
            allfactors[s] = min(factor1[s], factor2[s])
    hcfnum = 1
    for s in allfactors:
        hcfnum = hcfnum * (s**allfactors[s])
    return hcfnum

print("Factors num1: ", factorize(num))
print("Factors num2: ", factorize(num1))
print("Lcm: ", lcm(num, num1))
print("Hcf: ", hcf(num, num1))



