def fibo(n):
    if n in memo:
        return memo[n]
    elif n <= k:
        return 1
    else:
        toreturn = 0
        for i in range(k,0,-1):
            toreturn += fibo(n-i)
        return toreturn
memo = {}
n,k = input().split()
k = int(k)
n = int(n)
for i in range(1,k+1):
    memo[i] = 1
ans = fibo(n)
print(ans)
