t = int(input())
def ispal(n):
    n = str(n)
    revn = ""
    for i in range(len(n)-1,-1,-1):
        revn += n[i]
    if revn == n:
        return True
    else:
        return False
for i in range(0,t):
    ans = 0
    n,k = input().split()
    n = int(n)
    k = int(k)
    for j in range(n,k+1):
        if ispal(j) == True:
            ans += j
    print(ans)
        
