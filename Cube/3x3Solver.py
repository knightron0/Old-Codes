cube = {}
count = 0
ans = []
test = {}
top = 'g'
bottom = 'b'
right = 'r'
left = 'o'
up = 'w'
down = 'y'
eg1 = [up, left, down, right]
eg2 = [top, left, bottom, right]
eg3 = [top, left, bottom, right]
eg4 = [up, top, down, bottom]
eg5 = [up, top, down, bottom]
eg6 = [up, left, down, right]
finalcube = {}
for i in top, up, down, left, right, bottom:
    finalcube[i] = []
finalcube[up] = ['r','y','y','r','w','g','b','b','w']
finalcube[left] = ['y','y','y','b','o','g','w','g','r']
finalcube[top] = ['o','o','g','y','g','r','w','r','r']
finalcube[right] = ['r','o','g','b','r','b','b','g','b']
finalcube[bottom] = ['o','o','g','y','b','w','w','w','o']
finalcube[down] = ['b','w','y','r','y','w','g','o','o']
def printcube():
    print("      "+finalcube[up][0],finalcube[up][1],finalcube[up][2])
    print("      "+finalcube[up][3],finalcube[up][4],finalcube[up][5])
    print("      "+finalcube[up][6],finalcube[up][7],finalcube[up][8])
    print(finalcube[left][0],finalcube[left][1],finalcube[left][2],finalcube[top][0],finalcube[top][1],finalcube[top][2],finalcube[right][0],finalcube[right][1],finalcube[right][2],finalcube[bottom][0],finalcube[bottom][1],finalcube[bottom][2])
    print(finalcube[left][3],finalcube[left][4],finalcube[left][5],finalcube[top][3],finalcube[top][4],finalcube[top][5],finalcube[right][3],finalcube[right][4],finalcube[right][5],finalcube[bottom][3],finalcube[bottom][4],finalcube[bottom][5])
    print(finalcube[left][6],finalcube[left][7],finalcube[left][8],finalcube[top][6],finalcube[top][7],finalcube[top][8],finalcube[right][6],finalcube[right][7],finalcube[right][8],finalcube[bottom][6],finalcube[bottom][7],finalcube[bottom][8])
    print("      "+finalcube[down][0],finalcube[down][1],finalcube[down][2])
    print("      "+finalcube[down][3],finalcube[down][4],finalcube[down][5])
    print("      "+finalcube[down][6],finalcube[down][7],finalcube[down][8])
class moves:
    def R(movetype):
        temp = ['a', 'b', 'c']
        if movetype == 1:
            temp[0] = finalcube[eg4[3]][0]
            temp[1] = finalcube[eg4[3]][3]
            temp[2] = finalcube[eg4[3]][6]
            for i in range(3,-1, -1):
                if i != 0:
                    finalcube[eg4[i-1]][8],temp[0] = temp[0],finalcube[eg4[i-1]][8]
                    finalcube[eg4[i-1]][5],temp[1] = temp[1],finalcube[eg4[i-1]][5]
                    finalcube[eg4[i-1]][2],temp[2] = temp[2],finalcube[eg4[i-1]][2]
                elif i == 0:
                    finalcube[eg4[3]][0],temp[0] = temp[0],finalcube[eg4[3]][0]
                    finalcube[eg4[3]][3],temp[1] = temp[1],finalcube[eg4[3]][3]
                    finalcube[eg4[3]][6],temp[2] = temp[2],finalcube[eg4[3]][6]
            temp[0] = finalcube[right][6]
            temp[1] = finalcube[right][3]
            temp[2] = finalcube[right][0]
            finalcube[right][0],temp[0] = temp[0],finalcube[right][0]
            finalcube[right][1],temp[1] = temp[1],finalcube[right][1]
            finalcube[right][2],temp[2] = temp[2],finalcube[right][2]
            finalcube[right][5],temp[1] = temp[1],finalcube[right][5]
            finalcube[right][8],temp[2] = temp[2],finalcube[right][8]
            finalcube[right][7],temp[1] = temp[1],finalcube[right][7]
            finalcube[right][6],temp[2] = temp[2],finalcube[right][6]
            finalcube[right][3],temp[1] = temp[1],finalcube[right][3]
            finalcube[right][0],temp[2] = temp[2],finalcube[right][0]
        elif movetype == 2:
            temp[0] = finalcube[eg4[3]][0]
            temp[1] = finalcube[eg4[3]][3]
            temp[2] = finalcube[eg4[3]][6]
            for i in range(3,-1, -1):
                if i != 0:
                    finalcube[eg4[i-1]][8],temp[0] = temp[0],finalcube[eg4[i-1]][8]
                    finalcube[eg4[i-1]][5],temp[1] = temp[1],finalcube[eg4[i-1]][5]
                    finalcube[eg4[i-1]][2],temp[2] = temp[2],finalcube[eg4[i-1]][2]
                elif i == 0:
                    finalcube[eg4[3]][0],temp[0] = temp[0],finalcube[eg4[3]][0]
                    finalcube[eg4[3]][3],temp[1] = temp[1],finalcube[eg4[3]][3]
                    finalcube[eg4[3]][6],temp[2] = temp[2],finalcube[eg4[3]][6]
            temp[0] = finalcube[right][6]
            temp[1] = finalcube[right][3]
            temp[2] = finalcube[right][0]
            finalcube[right][0],temp[0] = temp[0],finalcube[right][0]
            finalcube[right][1],temp[1] = temp[1],finalcube[right][1]
            finalcube[right][2],temp[2] = temp[2],finalcube[right][2]
            finalcube[right][5],temp[1] = temp[1],finalcube[right][5]
            finalcube[right][8],temp[2] = temp[2],finalcube[right][8]
            finalcube[right][7],temp[1] = temp[1],finalcube[right][7]
            finalcube[right][6],temp[2] = temp[2],finalcube[right][6]
            finalcube[right][3],temp[1] = temp[1],finalcube[right][3]
            finalcube[right][0],temp[2] = temp[2],finalcube[right][0]
            temp[0] = finalcube[eg4[3]][0]
            temp[1] = finalcube[eg4[3]][3]
            temp[2] = finalcube[eg4[3]][6]
            for i in range(3,-1, -1):
                if i != 0:
                    finalcube[eg4[i-1]][8],temp[0] = temp[0],finalcube[eg4[i-1]][8]
                    finalcube[eg4[i-1]][5],temp[1] = temp[1],finalcube[eg4[i-1]][5]
                    finalcube[eg4[i-1]][2],temp[2] = temp[2],finalcube[eg4[i-1]][2]
                elif i == 0:
                    finalcube[eg4[3]][0],temp[0] = temp[0],finalcube[eg4[3]][0]
                    finalcube[eg4[3]][3],temp[1] = temp[1],finalcube[eg4[3]][3]
                    finalcube[eg4[3]][6],temp[2] = temp[2],finalcube[eg4[3]][6]
            temp[0] = finalcube[right][6]
            temp[1] = finalcube[right][3]
            temp[2] = finalcube[right][0]
            finalcube[right][0],temp[0] = temp[0],finalcube[right][0]
            finalcube[right][1],temp[1] = temp[1],finalcube[right][1]
            finalcube[right][2],temp[2] = temp[2],finalcube[right][2]
            finalcube[right][5],temp[1] = temp[1],finalcube[right][5]
            finalcube[right][8],temp[2] = temp[2],finalcube[right][8]
            finalcube[right][7],temp[1] = temp[1],finalcube[right][7]
            finalcube[right][6],temp[2] = temp[2],finalcube[right][6]
            finalcube[right][3],temp[1] = temp[1],finalcube[right][3]
            finalcube[right][0],temp[2] = temp[2],finalcube[right][0]
        elif movetype == '1i':
            temp[0] = finalcube[eg4[0]][2]
            temp[1] = finalcube[eg4[0]][5]
            temp[2] = finalcube[eg4[0]][8]
            for i in range(0, 4):
                if i < 2:
                    finalcube[eg4[i+1]][2],temp[0] = temp[0],finalcube[eg4[i+1]][2]
                    finalcube[eg4[i+1]][5],temp[1] = temp[1],finalcube[eg4[i+1]][5]
                    finalcube[eg4[i+1]][8],temp[2] = temp[2],finalcube[eg4[i+1]][8]
                elif i == 2:
                    finalcube[eg4[i+1]][6],temp[0] = temp[0],finalcube[eg4[i+1]][6]
                    finalcube[eg4[i+1]][3],temp[1] = temp[1],finalcube[eg4[i+1]][3]
                    finalcube[eg4[i+1]][0],temp[2] = temp[2],finalcube[eg4[i+1]][0]
                elif i == 3:
                    finalcube[eg4[0]][2],temp[0] = temp[0],finalcube[eg4[0]][2]
                    finalcube[eg4[0]][5],temp[1] = temp[1],finalcube[eg4[0]][5]
                    finalcube[eg4[0]][8],temp[2] = temp[2],finalcube[eg4[0]][8]
            temp[0] = finalcube[right][0]
            temp[1] = finalcube[right][3]
            temp[2] = finalcube[right][6]
            finalcube[right][6],temp[0] = temp[0],finalcube[right][6]
            finalcube[right][7],temp[1] = temp[1],finalcube[right][7]
            finalcube[right][8],temp[2] = temp[2],finalcube[right][8]
            finalcube[right][5],temp[1] = temp[1],finalcube[right][5]
            finalcube[right][2],temp[2] = temp[2],finalcube[right][2]
            finalcube[right][1],temp[1] = temp[1],finalcube[right][1]
            finalcube[right][0],temp[2] = temp[2],finalcube[right][0]
            finalcube[right][3],temp[1] = temp[1],finalcube[right][3]
            finalcube[right][6],temp[2] = temp[2],finalcube[right][6]
        elif movetype == '2i':
            temp[0] = finalcube[eg4[0]][2]
            temp[1] = finalcube[eg4[0]][5]
            temp[2] = finalcube[eg4[0]][8]
            for i in range(0, 4):
                if i < 2:
                    finalcube[eg4[i+1]][2],temp[0] = temp[0],finalcube[eg4[i+1]][2]
                    finalcube[eg4[i+1]][5],temp[1] = temp[1],finalcube[eg4[i+1]][5]
                    finalcube[eg4[i+1]][8],temp[2] = temp[2],finalcube[eg4[i+1]][8]
                elif i == 2:
                    finalcube[eg4[i+1]][6],temp[0] = temp[0],finalcube[eg4[i+1]][6]
                    finalcube[eg4[i+1]][3],temp[1] = temp[1],finalcube[eg4[i+1]][3]
                    finalcube[eg4[i+1]][0],temp[2] = temp[2],finalcube[eg4[i+1]][0]
                elif i == 3:
                    finalcube[eg4[0]][2],temp[0] = temp[0],finalcube[eg4[0]][2]
                    finalcube[eg4[0]][5],temp[1] = temp[1],finalcube[eg4[0]][5]
                    finalcube[eg4[0]][8],temp[2] = temp[2],finalcube[eg4[0]][8]
            temp[0] = finalcube[right][0]
            temp[1] = finalcube[right][3]
            temp[2] = finalcube[right][6]
            finalcube[right][6],temp[0] = temp[0],finalcube[right][6]
            finalcube[right][7],temp[1] = temp[1],finalcube[right][7]
            finalcube[right][8],temp[2] = temp[2],finalcube[right][8]
            finalcube[right][5],temp[1] = temp[1],finalcube[right][5]
            finalcube[right][2],temp[2] = temp[2],finalcube[right][2]
            finalcube[right][1],temp[1] = temp[1],finalcube[right][1]
            finalcube[right][0],temp[2] = temp[2],finalcube[right][0]
            finalcube[right][3],temp[1] = temp[1],finalcube[right][3]
            finalcube[right][6],temp[2] = temp[2],finalcube[right][6]
            temp[0] = finalcube[eg4[0]][2]
            temp[1] = finalcube[eg4[0]][5]
            temp[2] = finalcube[eg4[0]][8]
            for i in range(0, 4):
                if i < 2:
                    finalcube[eg4[i+1]][2],temp[0] = temp[0],finalcube[eg4[i+1]][2]
                    finalcube[eg4[i+1]][5],temp[1] = temp[1],finalcube[eg4[i+1]][5]
                    finalcube[eg4[i+1]][8],temp[2] = temp[2],finalcube[eg4[i+1]][8]
                elif i == 2:
                    finalcube[eg4[i+1]][6],temp[0] = temp[0],finalcube[eg4[i+1]][6]
                    finalcube[eg4[i+1]][3],temp[1] = temp[1],finalcube[eg4[i+1]][3]
                    finalcube[eg4[i+1]][0],temp[2] = temp[2],finalcube[eg4[i+1]][0]
                elif i == 3:
                    finalcube[eg4[0]][2],temp[0] = temp[0],finalcube[eg4[0]][2]
                    finalcube[eg4[0]][5],temp[1] = temp[1],finalcube[eg4[0]][5]
                    finalcube[eg4[0]][8],temp[2] = temp[2],finalcube[eg4[0]][8]
            temp[0] = finalcube[right][0]
            temp[1] = finalcube[right][3]
            temp[2] = finalcube[right][6]
            finalcube[right][6],temp[0] = temp[0],finalcube[right][6]
            finalcube[right][7],temp[1] = temp[1],finalcube[right][7]
            finalcube[right][8],temp[2] = temp[2],finalcube[right][8]
            finalcube[right][5],temp[1] = temp[1],finalcube[right][5]
            finalcube[right][2],temp[2] = temp[2],finalcube[right][2]
            finalcube[right][1],temp[1] = temp[1],finalcube[right][1]
            finalcube[right][0],temp[2] = temp[2],finalcube[right][0]
            finalcube[right][3],temp[1] = temp[1],finalcube[right][3]
            finalcube[right][6],temp[2] = temp[2],finalcube[right][6]
    def U(movetype):
        temp = ['a', 'b', 'c']
        if movetype == 1:
            temp[0] = finalcube[eg2[0]][0]
            temp[1] = finalcube[eg2[0]][1]
            temp[2] = finalcube[eg2[0]][2]
            for i in range(0, 4):
                if i != 3:
                    finalcube[eg2[i+1]][0],temp[0] = temp[0],finalcube[eg2[i+1]][0]
                    finalcube[eg2[i+1]][1],temp[1] = temp[1],finalcube[eg2[i+1]][1]
                    finalcube[eg2[i+1]][2],temp[2] = temp[2],finalcube[eg2[i+1]][2]
                elif i == 3:
                    finalcube[eg2[0]][0],temp[0] = temp[0],finalcube[eg2[0]][0]
                    finalcube[eg2[0]][1],temp[1] = temp[1],finalcube[eg2[0]][1]
                    finalcube[eg2[0]][2],temp[2] = temp[2],finalcube[eg2[0]][2]
            temp[0] = finalcube[up][6]
            temp[1] = finalcube[up][3]
            temp[2] = finalcube[up][0]
            finalcube[up][0],temp[0] = temp[0],finalcube[up][0]
            finalcube[up][1],temp[1] = temp[1],finalcube[up][1]
            finalcube[up][2],temp[2] = temp[2],finalcube[up][2]
            finalcube[up][5],temp[1] = temp[1],finalcube[up][5]
            finalcube[up][8],temp[2] = temp[2],finalcube[up][8]
            finalcube[up][7],temp[1] = temp[1],finalcube[up][7]
            finalcube[up][6],temp[2] = temp[2],finalcube[up][6]
            finalcube[up][3],temp[1] = temp[1],finalcube[up][3]
            finalcube[up][0],temp[2] = temp[2],finalcube[up][0]
        elif movetype == 2:
            temp[0] = finalcube[eg2[0]][0]
            temp[1] = finalcube[eg2[0]][1]
            temp[2] = finalcube[eg2[0]][2]
            for i in range(0, 4):
                if i != 3:
                    finalcube[eg2[i+1]][0],temp[0] = temp[0],finalcube[eg2[i+1]][0]
                    finalcube[eg2[i+1]][1],temp[1] = temp[1],finalcube[eg2[i+1]][1]
                    finalcube[eg2[i+1]][2],temp[2] = temp[2],finalcube[eg2[i+1]][2]
                elif i == 3:
                    finalcube[eg2[0]][0],temp[0] = temp[0],finalcube[eg2[0]][0]
                    finalcube[eg2[0]][1],temp[1] = temp[1],finalcube[eg2[0]][1]
                    finalcube[eg2[0]][2],temp[2] = temp[2],finalcube[eg2[0]][2]
            temp[0] = finalcube[up][6]
            temp[1] = finalcube[up][3]
            temp[2] = finalcube[up][0]
            finalcube[up][0],temp[0] = temp[0],finalcube[up][0]
            finalcube[up][1],temp[1] = temp[1],finalcube[up][1]
            finalcube[up][2],temp[2] = temp[2],finalcube[up][2]
            finalcube[up][5],temp[1] = temp[1],finalcube[up][5]
            finalcube[up][8],temp[2] = temp[2],finalcube[up][8]
            finalcube[up][7],temp[1] = temp[1],finalcube[up][7]
            finalcube[up][6],temp[2] = temp[2],finalcube[up][6]
            finalcube[up][3],temp[1] = temp[1],finalcube[up][3]
            finalcube[up][0],temp[2] = temp[2],finalcube[up][0]
            temp[0] = finalcube[eg2[0]][0]
            temp[1] = finalcube[eg2[0]][1]
            temp[2] = finalcube[eg2[0]][2]
            for i in range(0, 4):
                if i != 3:
                    finalcube[eg2[i+1]][0],temp[0] = temp[0],finalcube[eg2[i+1]][0]
                    finalcube[eg2[i+1]][1],temp[1] = temp[1],finalcube[eg2[i+1]][1]
                    finalcube[eg2[i+1]][2],temp[2] = temp[2],finalcube[eg2[i+1]][2]
                elif i == 3:
                    finalcube[eg2[0]][0],temp[0] = temp[0],finalcube[eg2[0]][0]
                    finalcube[eg2[0]][1],temp[1] = temp[1],finalcube[eg2[0]][1]
                    finalcube[eg2[0]][2],temp[2] = temp[2],finalcube[eg2[0]][2]
            temp[0] = finalcube[up][6]
            temp[1] = finalcube[up][3]
            temp[2] = finalcube[up][0]
            finalcube[up][0],temp[0] = temp[0],finalcube[up][0]
            finalcube[up][1],temp[1] = temp[1],finalcube[up][1]
            finalcube[up][2],temp[2] = temp[2],finalcube[up][2]
            finalcube[up][5],temp[1] = temp[1],finalcube[up][5]
            finalcube[up][8],temp[2] = temp[2],finalcube[up][8]
            finalcube[up][7],temp[1] = temp[1],finalcube[up][7]
            finalcube[up][6],temp[2] = temp[2],finalcube[up][6]
            finalcube[up][3],temp[1] = temp[1],finalcube[up][3]
            finalcube[up][0],temp[2] = temp[2],finalcube[up][0]
        elif movetype == '1i':
            temp[0] = finalcube[eg2[3]][0]
            temp[1] = finalcube[eg2[3]][1]
            temp[2] = finalcube[eg2[3]][2]
            for i in range(3, -1, -1):
                if i != 0:
                    finalcube[eg2[i-1]][0],temp[0] = temp[0],finalcube[eg2[i-1]][0]
                    finalcube[eg2[i-1]][1],temp[1] = temp[1],finalcube[eg2[i-1]][1]
                    finalcube[eg2[i-1]][2],temp[2] = temp[2],finalcube[eg2[i-1]][2]
                elif i == 0:
                    finalcube[eg2[3]][0],temp[0] = temp[0],finalcube[eg2[3]][0]
                    finalcube[eg2[3]][1],temp[1] = temp[1],finalcube[eg2[3]][1]
                    finalcube[eg2[3]][2],temp[2] = temp[2],finalcube[eg2[3]][2]
            temp[0] = finalcube[up][0]
            temp[1] = finalcube[up][3]
            temp[2] = finalcube[up][6]
            finalcube[up][6],temp[0] = temp[0],finalcube[up][6]
            finalcube[up][7],temp[1] = temp[1],finalcube[up][7]
            finalcube[up][8],temp[2] = temp[2],finalcube[up][8]
            finalcube[up][5],temp[1] = temp[1],finalcube[up][5]
            finalcube[up][2],temp[2] = temp[2],finalcube[up][2]
            finalcube[up][1],temp[1] = temp[1],finalcube[up][1]
            finalcube[up][0],temp[2] = temp[2],finalcube[up][0]
            finalcube[up][3],temp[1] = temp[1],finalcube[up][3]
            finalcube[up][6],temp[2] = temp[2],finalcube[up][6]
        elif movetype == '2i':
            temp[0] = finalcube[eg2[3]][0]
            temp[1] = finalcube[eg2[3]][1]
            temp[2] = finalcube[eg2[3]][2]
            for i in range(3, -1, -1):
                if i != 0:
                    finalcube[eg2[i-1]][0],temp[0] = temp[0],finalcube[eg2[i-1]][0]
                    finalcube[eg2[i-1]][1],temp[1] = temp[1],finalcube[eg2[i-1]][1]
                    finalcube[eg2[i-1]][2],temp[2] = temp[2],finalcube[eg2[i-1]][2]
                elif i == 0:
                    finalcube[eg2[3]][0],temp[0] = temp[0],finalcube[eg2[3]][0]
                    finalcube[eg2[3]][1],temp[1] = temp[1],finalcube[eg2[3]][1]
                    finalcube[eg2[3]][2],temp[2] = temp[2],finalcube[eg2[3]][2]
            temp[0] = finalcube[up][0]
            temp[1] = finalcube[up][3]
            temp[2] = finalcube[up][6]
            finalcube[up][6],temp[0] = temp[0],finalcube[up][6]
            finalcube[up][7],temp[1] = temp[1],finalcube[up][7]
            finalcube[up][8],temp[2] = temp[2],finalcube[up][8]
            finalcube[up][5],temp[1] = temp[1],finalcube[up][5]
            finalcube[up][2],temp[2] = temp[2],finalcube[up][2]
            finalcube[up][1],temp[1] = temp[1],finalcube[up][1]
            finalcube[up][0],temp[2] = temp[2],finalcube[up][0]
            finalcube[up][3],temp[1] = temp[1],finalcube[up][3]
            finalcube[up][6],temp[2] = temp[2],finalcube[up][6]
            temp[0] = finalcube[eg2[3]][0]
            temp[1] = finalcube[eg2[3]][1]
            temp[2] = finalcube[eg2[3]][2]
            for i in range(3, -1, -1):
                if i != 0:
                    finalcube[eg2[i-1]][0],temp[0] = temp[0],finalcube[eg2[i-1]][0]
                    finalcube[eg2[i-1]][1],temp[1] = temp[1],finalcube[eg2[i-1]][1]
                    finalcube[eg2[i-1]][2],temp[2] = temp[2],finalcube[eg2[i-1]][2]
                elif i == 0:
                    finalcube[eg2[3]][0],temp[0] = temp[0],finalcube[eg2[3]][0]
                    finalcube[eg2[3]][1],temp[1] = temp[1],finalcube[eg2[3]][1]
                    finalcube[eg2[3]][2],temp[2] = temp[2],finalcube[eg2[3]][2]
            temp[0] = finalcube[up][0]
            temp[1] = finalcube[up][3]
            temp[2] = finalcube[up][6]
            finalcube[up][6],temp[0] = temp[0],finalcube[up][6]
            finalcube[up][7],temp[1] = temp[1],finalcube[up][7]
            finalcube[up][8],temp[2] = temp[2],finalcube[up][8]
            finalcube[up][5],temp[1] = temp[1],finalcube[up][5]
            finalcube[up][2],temp[2] = temp[2],finalcube[up][2]
            finalcube[up][1],temp[1] = temp[1],finalcube[up][1]
            finalcube[up][0],temp[2] = temp[2],finalcube[up][0]
            finalcube[up][3],temp[1] = temp[1],finalcube[up][3]
            finalcube[up][6],temp[2] = temp[2],finalcube[up][6]
    def F(movetype):
        temp = ['a', 'b', 'c']
        if movetype == 1:
            temp[0] = finalcube[eg1[3]][0]
            temp[1] = finalcube[eg1[3]][3]
            temp[2] = finalcube[eg1[3]][6]
            for i in range(3, -1, -1):
                if i == 3:
                    finalcube[eg1[i-1]][2],temp[0] = temp[0],finalcube[eg1[i-1]][2]
                    finalcube[eg1[i-1]][1],temp[1] = temp[1],finalcube[eg1[i-1]][1]
                    finalcube[eg1[i-1]][0],temp[2] = temp[2],finalcube[eg1[i-1]][0]
                elif i == 2:
                    finalcube[eg1[i-1]][8],temp[0] = temp[0],finalcube[eg1[i-1]][8]
                    finalcube[eg1[i-1]][5],temp[1] = temp[1],finalcube[eg1[i-1]][5]
                    finalcube[eg1[i-1]][2],temp[2] = temp[2],finalcube[eg1[i-1]][2]
                elif i == 1:
                    finalcube[eg1[i-1]][6],temp[0] = temp[0],finalcube[eg1[i-1]][6]
                    finalcube[eg1[i-1]][7],temp[1] = temp[1],finalcube[eg1[i-1]][7]
                    finalcube[eg1[i-1]][8],temp[2] = temp[2],finalcube[eg1[i-1]][8]
                elif i == 0:
                    finalcube[eg1[3]][0],temp[0] = temp[0],finalcube[eg1[3]][0]
                    finalcube[eg1[3]][3],temp[1] = temp[1],finalcube[eg1[3]][3]
                    finalcube[eg1[3]][6],temp[2] = temp[2],finalcube[eg1[3]][6]
            temp[0] = finalcube[top][0]
            temp[1] = finalcube[top][1]
            temp[2] = finalcube[top][2]
            finalcube[top][2],temp[0] = temp[0],finalcube[top][2]
            finalcube[top][5],temp[1] = temp[1],finalcube[top][5]
            finalcube[top][8],temp[2] = temp[2],finalcube[top][8]
            finalcube[top][7],temp[1] = temp[1],finalcube[top][7]
            finalcube[top][6],temp[2] = temp[2],finalcube[top][6]
            finalcube[top][3],temp[1] = temp[1],finalcube[top][3]
            finalcube[top][0],temp[2] = temp[2],finalcube[top][0]
            finalcube[top][1],temp[1] = temp[1],finalcube[top][1]
            finalcube[top][2],temp[2] = temp[2],finalcube[top][2]
        elif movetype == 2:
            temp[0] = finalcube[eg1[3]][0]
            temp[1] = finalcube[eg1[3]][3]
            temp[2] = finalcube[eg1[3]][6]
            for i in range(3, -1, -1):
                if i == 3:
                    finalcube[eg1[i-1]][2],temp[0] = temp[0],finalcube[eg1[i-1]][2]
                    finalcube[eg1[i-1]][1],temp[1] = temp[1],finalcube[eg1[i-1]][1]
                    finalcube[eg1[i-1]][0],temp[2] = temp[2],finalcube[eg1[i-1]][0]
                elif i == 2:
                    finalcube[eg1[i-1]][8],temp[0] = temp[0],finalcube[eg1[i-1]][8]
                    finalcube[eg1[i-1]][5],temp[1] = temp[1],finalcube[eg1[i-1]][5]
                    finalcube[eg1[i-1]][2],temp[2] = temp[2],finalcube[eg1[i-1]][2]
                elif i == 1:
                    finalcube[eg1[i-1]][6],temp[0] = temp[0],finalcube[eg1[i-1]][6]
                    finalcube[eg1[i-1]][7],temp[1] = temp[1],finalcube[eg1[i-1]][7]
                    finalcube[eg1[i-1]][8],temp[2] = temp[2],finalcube[eg1[i-1]][8]
                elif i == 0:
                    finalcube[eg1[3]][0],temp[0] = temp[0],finalcube[eg1[3]][0]
                    finalcube[eg1[3]][3],temp[1] = temp[1],finalcube[eg1[3]][3]
                    finalcube[eg1[3]][6],temp[2] = temp[2],finalcube[eg1[3]][6]
            temp[0] = finalcube[top][0]
            temp[1] = finalcube[top][1]
            temp[2] = finalcube[top][2]
            finalcube[top][2],temp[0] = temp[0],finalcube[top][2]
            finalcube[top][5],temp[1] = temp[1],finalcube[top][5]
            finalcube[top][8],temp[2] = temp[2],finalcube[top][8]
            finalcube[top][7],temp[1] = temp[1],finalcube[top][7]
            finalcube[top][6],temp[2] = temp[2],finalcube[top][6]
            finalcube[top][3],temp[1] = temp[1],finalcube[top][3]
            finalcube[top][0],temp[2] = temp[2],finalcube[top][0]
            finalcube[top][1],temp[1] = temp[1],finalcube[top][1]
            finalcube[top][2],temp[2] = temp[2],finalcube[top][2]
            temp[0] = finalcube[eg1[3]][0]
            temp[1] = finalcube[eg1[3]][3]
            temp[2] = finalcube[eg1[3]][6]
            for i in range(3, -1, -1):
                if i == 3:
                    finalcube[eg1[i-1]][2],temp[0] = temp[0],finalcube[eg1[i-1]][2]
                    finalcube[eg1[i-1]][1],temp[1] = temp[1],finalcube[eg1[i-1]][1]
                    finalcube[eg1[i-1]][0],temp[2] = temp[2],finalcube[eg1[i-1]][0]
                elif i == 2:
                    finalcube[eg1[i-1]][8],temp[0] = temp[0],finalcube[eg1[i-1]][8]
                    finalcube[eg1[i-1]][5],temp[1] = temp[1],finalcube[eg1[i-1]][5]
                    finalcube[eg1[i-1]][2],temp[2] = temp[2],finalcube[eg1[i-1]][2]
                elif i == 1:
                    finalcube[eg1[i-1]][6],temp[0] = temp[0],finalcube[eg1[i-1]][6]
                    finalcube[eg1[i-1]][7],temp[1] = temp[1],finalcube[eg1[i-1]][7]
                    finalcube[eg1[i-1]][8],temp[2] = temp[2],finalcube[eg1[i-1]][8]
                elif i == 0:
                    finalcube[eg1[3]][0],temp[0] = temp[0],finalcube[eg1[3]][0]
                    finalcube[eg1[3]][3],temp[1] = temp[1],finalcube[eg1[3]][3]
                    finalcube[eg1[3]][6],temp[2] = temp[2],finalcube[eg1[3]][6]
            temp[0] = finalcube[top][0]
            temp[1] = finalcube[top][1]
            temp[2] = finalcube[top][2]
            finalcube[top][2],temp[0] = temp[0],finalcube[top][2]
            finalcube[top][5],temp[1] = temp[1],finalcube[top][5]
            finalcube[top][8],temp[2] = temp[2],finalcube[top][8]
            finalcube[top][7],temp[1] = temp[1],finalcube[top][7]
            finalcube[top][6],temp[2] = temp[2],finalcube[top][6]
            finalcube[top][3],temp[1] = temp[1],finalcube[top][3]
            finalcube[top][0],temp[2] = temp[2],finalcube[top][0]
            finalcube[top][1],temp[1] = temp[1],finalcube[top][1]
            finalcube[top][2],temp[2] = temp[2],finalcube[top][2]
        elif movetype == '1i':
            temp[0] = finalcube[eg1[0]][6]
            temp[1] = finalcube[eg1[0]][7]
            temp[2] = finalcube[eg1[0]][8]
            for i in range(0, 4):
                if i == 0:
                    finalcube[eg1[i+1]][8],temp[0] = temp[0],finalcube[eg1[i+1]][8]
                    finalcube[eg1[i+1]][5],temp[1] = temp[1],finalcube[eg1[i+1]][5]
                    finalcube[eg1[i+1]][2],temp[2] = temp[2],finalcube[eg1[i+1]][2]
                elif i == 1:
                    finalcube[eg1[i+1]][2],temp[0] = temp[0],finalcube[eg1[i+1]][2]
                    finalcube[eg1[i+1]][1],temp[1] = temp[1],finalcube[eg1[i+1]][1]
                    finalcube[eg1[i+1]][0],temp[2] = temp[2],finalcube[eg1[i+1]][0]
                elif i == 2:
                    finalcube[eg1[i+1]][0],temp[0] = temp[0],finalcube[eg1[i+1]][0]
                    finalcube[eg1[i+1]][3],temp[1] = temp[1],finalcube[eg1[i+1]][3]
                    finalcube[eg1[i+1]][6],temp[2] = temp[2],finalcube[eg1[i+1]][6]
                elif i == 3:
                    finalcube[eg1[0]][6],temp[0] = temp[0],finalcube[eg1[0]][6]
                    finalcube[eg1[0]][7],temp[1] = temp[1],finalcube[eg1[0]][7]
                    finalcube[eg1[0]][8],temp[2] = temp[2],finalcube[eg1[0]][8]
            temp[0] = finalcube[top][2]
            temp[1] = finalcube[top][1]
            temp[2] = finalcube[top][0]
            finalcube[top][0],temp[0] = temp[0],finalcube[top][0]
            finalcube[top][3],temp[1] = temp[1],finalcube[top][3]
            finalcube[top][6],temp[2] = temp[2],finalcube[top][6]
            finalcube[top][7],temp[1] = temp[1],finalcube[top][7]
            finalcube[top][8],temp[2] = temp[2],finalcube[top][8]
            finalcube[top][5],temp[1] = temp[1],finalcube[top][5]
            finalcube[top][2],temp[2] = temp[2],finalcube[top][2]
            finalcube[top][1],temp[1] = temp[1],finalcube[top][1]
            finalcube[top][0],temp[2] = temp[2],finalcube[top][0]
        elif movetype == '2i':
            temp[0] = finalcube[eg1[0]][6]
            temp[1] = finalcube[eg1[0]][7]
            temp[2] = finalcube[eg1[0]][8]
            for i in range(0, 4):
                if i == 0:
                    finalcube[eg1[i+1]][8],temp[0] = temp[0],finalcube[eg1[i+1]][8]
                    finalcube[eg1[i+1]][5],temp[1] = temp[1],finalcube[eg1[i+1]][5]
                    finalcube[eg1[i+1]][2],temp[2] = temp[2],finalcube[eg1[i+1]][2]
                elif i == 1:
                    finalcube[eg1[i+1]][2],temp[0] = temp[0],finalcube[eg1[i+1]][2]
                    finalcube[eg1[i+1]][1],temp[1] = temp[1],finalcube[eg1[i+1]][1]
                    finalcube[eg1[i+1]][0],temp[2] = temp[2],finalcube[eg1[i+1]][0]
                elif i == 2:
                    finalcube[eg1[i+1]][0],temp[0] = temp[0],finalcube[eg1[i+1]][0]
                    finalcube[eg1[i+1]][3],temp[1] = temp[1],finalcube[eg1[i+1]][3]
                    finalcube[eg1[i+1]][6],temp[2] = temp[2],finalcube[eg1[i+1]][6]
                elif i == 3:
                    finalcube[eg1[0]][6],temp[0] = temp[0],finalcube[eg1[0]][6]
                    finalcube[eg1[0]][7],temp[1] = temp[1],finalcube[eg1[0]][7]
                    finalcube[eg1[0]][8],temp[2] = temp[2],finalcube[eg1[0]][8]
            temp[0] = finalcube[top][2]
            temp[1] = finalcube[top][1]
            temp[2] = finalcube[top][0]
            finalcube[top][0],temp[0] = temp[0],finalcube[top][0]
            finalcube[top][3],temp[1] = temp[1],finalcube[top][3]
            finalcube[top][6],temp[2] = temp[2],finalcube[top][6]
            finalcube[top][7],temp[1] = temp[1],finalcube[top][7]
            finalcube[top][8],temp[2] = temp[2],finalcube[top][8]
            finalcube[top][5],temp[1] = temp[1],finalcube[top][5]
            finalcube[top][2],temp[2] = temp[2],finalcube[top][2]
            finalcube[top][1],temp[1] = temp[1],finalcube[top][1]
            finalcube[top][0],temp[2] = temp[2],finalcube[top][0]
            temp[0] = finalcube[eg1[0]][6]
            temp[1] = finalcube[eg1[0]][7]
            temp[2] = finalcube[eg1[0]][8]
            for i in range(0, 4):
                if i == 0:
                    finalcube[eg1[i+1]][8],temp[0] = temp[0],finalcube[eg1[i+1]][8]
                    finalcube[eg1[i+1]][5],temp[1] = temp[1],finalcube[eg1[i+1]][5]
                    finalcube[eg1[i+1]][2],temp[2] = temp[2],finalcube[eg1[i+1]][2]
                elif i == 1:
                    finalcube[eg1[i+1]][2],temp[0] = temp[0],finalcube[eg1[i+1]][2]
                    finalcube[eg1[i+1]][1],temp[1] = temp[1],finalcube[eg1[i+1]][1]
                    finalcube[eg1[i+1]][0],temp[2] = temp[2],finalcube[eg1[i+1]][0]
                elif i == 2:
                    finalcube[eg1[i+1]][0],temp[0] = temp[0],finalcube[eg1[i+1]][0]
                    finalcube[eg1[i+1]][3],temp[1] = temp[1],finalcube[eg1[i+1]][3]
                    finalcube[eg1[i+1]][6],temp[2] = temp[2],finalcube[eg1[i+1]][6]
                elif i == 3:
                    finalcube[eg1[0]][6],temp[0] = temp[0],finalcube[eg1[0]][6]
                    finalcube[eg1[0]][7],temp[1] = temp[1],finalcube[eg1[0]][7]
                    finalcube[eg1[0]][8],temp[2] = temp[2],finalcube[eg1[0]][8]
            temp[0] = finalcube[top][2]
            temp[1] = finalcube[top][1]
            temp[2] = finalcube[top][0]
            finalcube[top][0],temp[0] = temp[0],finalcube[top][0]
            finalcube[top][3],temp[1] = temp[1],finalcube[top][3]
            finalcube[top][6],temp[2] = temp[2],finalcube[top][6]
            finalcube[top][7],temp[1] = temp[1],finalcube[top][7]
            finalcube[top][8],temp[2] = temp[2],finalcube[top][8]
            finalcube[top][5],temp[1] = temp[1],finalcube[top][5]
            finalcube[top][2],temp[2] = temp[2],finalcube[top][2]
            finalcube[top][1],temp[1] = temp[1],finalcube[top][1]
            finalcube[top][0],temp[2] = temp[2],finalcube[top][0]
    def D(movetype):
        temp = ['a', 'b', 'c']
        if movetype == 1:
            temp[0] = finalcube[eg2[3]][6]
            temp[1] = finalcube[eg2[3]][7]
            temp[2] = finalcube[eg2[3]][8]
            for i in range(3, -1, -1):
                if i != 0:
                    finalcube[eg2[i-1]][6],temp[0] = temp[0],finalcube[eg2[i-1]][6]
                    finalcube[eg2[i-1]][7],temp[1] = temp[1],finalcube[eg2[i-1]][7]
                    finalcube[eg2[i-1]][8],temp[2] = temp[2],finalcube[eg2[i-1]][8]
                elif i == 0:
                    finalcube[eg2[3]][6],temp[0] = temp[0],finalcube[eg2[3]][6]
                    finalcube[eg2[3]][7],temp[1] = temp[1],finalcube[eg2[3]][7]
                    finalcube[eg2[3]][8],temp[2] = temp[2],finalcube[eg2[3]][8]
            temp[0] = finalcube[down][6]
            temp[1] = finalcube[down][3]
            temp[2] = finalcube[down][0]
            finalcube[down][0],temp[0] = temp[0],finalcube[down][0]
            finalcube[down][1],temp[1] = temp[1],finalcube[down][1]
            finalcube[down][2],temp[2] = temp[2],finalcube[down][2]
            finalcube[down][5],temp[1] = temp[1],finalcube[down][5]
            finalcube[down][8],temp[2] = temp[2],finalcube[down][8]
            finalcube[down][7],temp[1] = temp[1],finalcube[down][7]
            finalcube[down][6],temp[2] = temp[2],finalcube[down][6]
            finalcube[down][3],temp[1] = temp[1],finalcube[down][3]
            finalcube[down][0],temp[2] = temp[2],finalcube[down][0]
        elif movetype == 2:
            temp[0] = finalcube[eg2[3]][6]
            temp[1] = finalcube[eg2[3]][7]
            temp[2] = finalcube[eg2[3]][8]
            for i in range(3, -1, -1):
                if i != 0:
                    finalcube[eg2[i-1]][6],temp[0] = temp[0],finalcube[eg2[i-1]][6]
                    finalcube[eg2[i-1]][7],temp[1] = temp[1],finalcube[eg2[i-1]][7]
                    finalcube[eg2[i-1]][8],temp[2] = temp[2],finalcube[eg2[i-1]][8]
                elif i == 0:
                    finalcube[eg2[3]][6],temp[0] = temp[0],finalcube[eg2[3]][6]
                    finalcube[eg2[3]][7],temp[1] = temp[1],finalcube[eg2[3]][7]
                    finalcube[eg2[3]][8],temp[2] = temp[2],finalcube[eg2[3]][8]
            temp[0] = finalcube[down][6]
            temp[1] = finalcube[down][3]
            temp[2] = finalcube[down][0]
            finalcube[down][0],temp[0] = temp[0],finalcube[down][0]
            finalcube[down][1],temp[1] = temp[1],finalcube[down][1]
            finalcube[down][2],temp[2] = temp[2],finalcube[down][2]
            finalcube[down][5],temp[1] = temp[1],finalcube[down][5]
            finalcube[down][8],temp[2] = temp[2],finalcube[down][8]
            finalcube[down][7],temp[1] = temp[1],finalcube[down][7]
            finalcube[down][6],temp[2] = temp[2],finalcube[down][6]
            finalcube[down][3],temp[1] = temp[1],finalcube[down][3]
            finalcube[down][0],temp[2] = temp[2],finalcube[down][0]
            temp[0] = finalcube[eg2[3]][6]
            temp[1] = finalcube[eg2[3]][7]
            temp[2] = finalcube[eg2[3]][8]
            for i in range(3, -1, -1):
                if i != 0:
                    finalcube[eg2[i-1]][6],temp[0] = temp[0],finalcube[eg2[i-1]][6]
                    finalcube[eg2[i-1]][7],temp[1] = temp[1],finalcube[eg2[i-1]][7]
                    finalcube[eg2[i-1]][8],temp[2] = temp[2],finalcube[eg2[i-1]][8]
                elif i == 0:
                    finalcube[eg2[3]][6],temp[0] = temp[0],finalcube[eg2[3]][6]
                    finalcube[eg2[3]][7],temp[1] = temp[1],finalcube[eg2[3]][7]
                    finalcube[eg2[3]][8],temp[2] = temp[2],finalcube[eg2[3]][8]
            temp[0] = finalcube[down][6]
            temp[1] = finalcube[down][3]
            temp[2] = finalcube[down][0]
            finalcube[down][0],temp[0] = temp[0],finalcube[down][0]
            finalcube[down][1],temp[1] = temp[1],finalcube[down][1]
            finalcube[down][2],temp[2] = temp[2],finalcube[down][2]
            finalcube[down][5],temp[1] = temp[1],finalcube[down][5]
            finalcube[down][8],temp[2] = temp[2],finalcube[down][8]
            finalcube[down][7],temp[1] = temp[1],finalcube[down][7]
            finalcube[down][6],temp[2] = temp[2],finalcube[down][6]
            finalcube[down][3],temp[1] = temp[1],finalcube[down][3]
            finalcube[down][0],temp[2] = temp[2],finalcube[down][0]
        elif movetype == '1i':
            temp[0] = finalcube[eg2[0]][6]
            temp[1] = finalcube[eg2[0]][7]
            temp[2] = finalcube[eg2[0]][8]
            for i in range(0, 4):
                if i != 3:
                    finalcube[eg2[i+1]][6],temp[0] = temp[0],finalcube[eg2[i+1]][6]
                    finalcube[eg2[i+1]][7],temp[1] = temp[1],finalcube[eg2[i+1]][7]
                    finalcube[eg2[i+1]][8],temp[2] = temp[2],finalcube[eg2[i+1]][8]
                elif i == 3:
                    finalcube[eg2[0]][6],temp[0] = temp[0],finalcube[eg2[0]][6]
                    finalcube[eg2[0]][7],temp[1] = temp[1],finalcube[eg2[0]][7]
                    finalcube[eg2[0]][8],temp[2] = temp[2],finalcube[eg2[0]][8]
            temp[0] = finalcube[down][0]
            temp[1] = finalcube[down][3]
            temp[2] = finalcube[down][6]
            finalcube[down][6],temp[0] = temp[0],finalcube[down][6]
            finalcube[down][7],temp[1] = temp[1],finalcube[down][7]
            finalcube[down][8],temp[2] = temp[2],finalcube[down][8]
            finalcube[down][5],temp[1] = temp[1],finalcube[down][5]
            finalcube[down][2],temp[2] = temp[2],finalcube[down][2]
            finalcube[down][1],temp[1] = temp[1],finalcube[down][1]
            finalcube[down][0],temp[2] = temp[2],finalcube[down][0]
            finalcube[down][3],temp[1] = temp[1],finalcube[down][3]
            finalcube[down][6],temp[2] = temp[2],finalcube[down][6]
        elif movetype == '2i':
            temp[0] = finalcube[eg2[0]][6]
            temp[1] = finalcube[eg2[0]][7]
            temp[2] = finalcube[eg2[0]][8]
            for i in range(0, 4):
                if i != 3:
                    finalcube[eg2[i+1]][6],temp[0] = temp[0],finalcube[eg2[i+1]][6]
                    finalcube[eg2[i+1]][7],temp[1] = temp[1],finalcube[eg2[i+1]][7]
                    finalcube[eg2[i+1]][8],temp[2] = temp[2],finalcube[eg2[i+1]][8]
                elif i == 3:
                    finalcube[eg2[0]][6],temp[0] = temp[0],finalcube[eg2[0]][6]
                    finalcube[eg2[0]][7],temp[1] = temp[1],finalcube[eg2[0]][7]
                    finalcube[eg2[0]][8],temp[2] = temp[2],finalcube[eg2[0]][8]
            temp[0] = finalcube[down][0]
            temp[1] = finalcube[down][3]
            temp[2] = finalcube[down][6]
            finalcube[down][6],temp[0] = temp[0],finalcube[down][6]
            finalcube[down][7],temp[1] = temp[1],finalcube[down][7]
            finalcube[down][8],temp[2] = temp[2],finalcube[down][8]
            finalcube[down][5],temp[1] = temp[1],finalcube[down][5]
            finalcube[down][2],temp[2] = temp[2],finalcube[down][2]
            finalcube[down][1],temp[1] = temp[1],finalcube[down][1]
            finalcube[down][0],temp[2] = temp[2],finalcube[down][0]
            finalcube[down][3],temp[1] = temp[1],finalcube[down][3]
            finalcube[down][6],temp[2] = temp[2],finalcube[down][6]
            temp[0] = finalcube[eg2[0]][6]
            temp[1] = finalcube[eg2[0]][7]
            temp[2] = finalcube[eg2[0]][8]
            for i in range(0, 4):
                if i != 3:
                    finalcube[eg2[i+1]][6],temp[0] = temp[0],finalcube[eg2[i+1]][6]
                    finalcube[eg2[i+1]][7],temp[1] = temp[1],finalcube[eg2[i+1]][7]
                    finalcube[eg2[i+1]][8],temp[2] = temp[2],finalcube[eg2[i+1]][8]
                elif i == 3:
                    finalcube[eg2[0]][6],temp[0] = temp[0],finalcube[eg2[0]][6]
                    finalcube[eg2[0]][7],temp[1] = temp[1],finalcube[eg2[0]][7]
                    finalcube[eg2[0]][8],temp[2] = temp[2],finalcube[eg2[0]][8]
            temp[0] = finalcube[down][0]
            temp[1] = finalcube[down][3]
            temp[2] = finalcube[down][6]
            finalcube[down][6],temp[0] = temp[0],finalcube[down][6]
            finalcube[down][7],temp[1] = temp[1],finalcube[down][7]
            finalcube[down][8],temp[2] = temp[2],finalcube[down][8]
            finalcube[down][5],temp[1] = temp[1],finalcube[down][5]
            finalcube[down][2],temp[2] = temp[2],finalcube[down][2]
            finalcube[down][1],temp[1] = temp[1],finalcube[down][1]
            finalcube[down][0],temp[2] = temp[2],finalcube[down][0]
            finalcube[down][3],temp[1] = temp[1],finalcube[down][3]
            finalcube[down][6],temp[2] = temp[2],finalcube[down][6]
    def L(movetype):
        temp = ['a', 'b', 'c']
        if movetype == 1:
            temp[0] = finalcube[eg4[0]][0]
            temp[1] = finalcube[eg4[0]][3]
            temp[2] = finalcube[eg4[0]][6]
            for i in range(0, 4):
                if i < 2:
                    finalcube[eg4[i+1]][0],temp[0] = temp[0],finalcube[eg4[i+1]][0]
                    finalcube[eg4[i+1]][3],temp[1] = temp[1],finalcube[eg4[i+1]][3]
                    finalcube[eg4[i+1]][6],temp[2] = temp[2],finalcube[eg4[i+1]][6]
                elif i == 2:
                    finalcube[eg4[i+1]][8],temp[0] = temp[0],finalcube[eg4[i+1]][8]
                    finalcube[eg4[i+1]][5],temp[1] = temp[1],finalcube[eg4[i+1]][5]
                    finalcube[eg4[i+1]][2],temp[2] = temp[2],finalcube[eg4[i+1]][2]
                elif i == 3:
                    finalcube[eg4[0]][0],temp[0] = temp[0],finalcube[eg4[0]][0]
                    finalcube[eg4[0]][3],temp[1] = temp[1],finalcube[eg4[0]][3]
                    finalcube[eg4[0]][6],temp[2] = temp[2],finalcube[eg4[0]][6]
            temp[0] = finalcube[left][6]
            temp[1] = finalcube[left][3]
            temp[2] = finalcube[left][0]
            finalcube[left][0],temp[0] = temp[0],finalcube[left][0]
            finalcube[left][1],temp[1] = temp[1],finalcube[left][1]
            finalcube[left][2],temp[2] = temp[2],finalcube[left][2]
            finalcube[left][5],temp[1] = temp[1],finalcube[left][5]
            finalcube[left][8],temp[2] = temp[2],finalcube[left][8]
            finalcube[left][7],temp[1] = temp[1],finalcube[left][7]
            finalcube[left][6],temp[2] = temp[2],finalcube[left][6]
            finalcube[left][3],temp[1] = temp[1],finalcube[left][3]
            finalcube[left][0],temp[2] = temp[2],finalcube[left][0]
        elif movetype == 2:
            temp[0] = finalcube[eg4[0]][0]
            temp[1] = finalcube[eg4[0]][3]
            temp[2] = finalcube[eg4[0]][6]
            for i in range(0, 4):
                if i < 2:
                    finalcube[eg4[i+1]][0],temp[0] = temp[0],finalcube[eg4[i+1]][0]
                    finalcube[eg4[i+1]][3],temp[1] = temp[1],finalcube[eg4[i+1]][3]
                    finalcube[eg4[i+1]][6],temp[2] = temp[2],finalcube[eg4[i+1]][6]
                elif i == 2:
                    finalcube[eg4[i+1]][8],temp[0] = temp[0],finalcube[eg4[i+1]][8]
                    finalcube[eg4[i+1]][5],temp[1] = temp[1],finalcube[eg4[i+1]][5]
                    finalcube[eg4[i+1]][2],temp[2] = temp[2],finalcube[eg4[i+1]][2]
                elif i == 3:
                    finalcube[eg4[0]][0],temp[0] = temp[0],finalcube[eg4[0]][0]
                    finalcube[eg4[0]][3],temp[1] = temp[1],finalcube[eg4[0]][3]
                    finalcube[eg4[0]][6],temp[2] = temp[2],finalcube[eg4[0]][6]
            temp[0] = finalcube[left][6]
            temp[1] = finalcube[left][3]
            temp[2] = finalcube[left][0]
            finalcube[left][0],temp[0] = temp[0],finalcube[left][0]
            finalcube[left][1],temp[1] = temp[1],finalcube[left][1]
            finalcube[left][2],temp[2] = temp[2],finalcube[left][2]
            finalcube[left][5],temp[1] = temp[1],finalcube[left][5]
            finalcube[left][8],temp[2] = temp[2],finalcube[left][8]
            finalcube[left][7],temp[1] = temp[1],finalcube[left][7]
            finalcube[left][6],temp[2] = temp[2],finalcube[left][6]
            finalcube[left][3],temp[1] = temp[1],finalcube[left][3]
            finalcube[left][0],temp[2] = temp[2],finalcube[left][0]
            temp[0] = finalcube[eg4[0]][0]
            temp[1] = finalcube[eg4[0]][3]
            temp[2] = finalcube[eg4[0]][6]
            for i in range(0, 4):
                if i < 2:
                    finalcube[eg4[i+1]][0],temp[0] = temp[0],finalcube[eg4[i+1]][0]
                    finalcube[eg4[i+1]][3],temp[1] = temp[1],finalcube[eg4[i+1]][3]
                    finalcube[eg4[i+1]][6],temp[2] = temp[2],finalcube[eg4[i+1]][6]
                elif i == 2:
                    finalcube[eg4[i+1]][8],temp[0] = temp[0],finalcube[eg4[i+1]][8]
                    finalcube[eg4[i+1]][5],temp[1] = temp[1],finalcube[eg4[i+1]][5]
                    finalcube[eg4[i+1]][2],temp[2] = temp[2],finalcube[eg4[i+1]][2]
                elif i == 3:
                    finalcube[eg4[0]][0],temp[0] = temp[0],finalcube[eg4[0]][0]
                    finalcube[eg4[0]][3],temp[1] = temp[1],finalcube[eg4[0]][3]
                    finalcube[eg4[0]][6],temp[2] = temp[2],finalcube[eg4[0]][6]
            temp[0] = finalcube[left][6]
            temp[1] = finalcube[left][3]
            temp[2] = finalcube[left][0]
            finalcube[left][0],temp[0] = temp[0],finalcube[left][0]
            finalcube[left][1],temp[1] = temp[1],finalcube[left][1]
            finalcube[left][2],temp[2] = temp[2],finalcube[left][2]
            finalcube[left][5],temp[1] = temp[1],finalcube[left][5]
            finalcube[left][8],temp[2] = temp[2],finalcube[left][8]
            finalcube[left][7],temp[1] = temp[1],finalcube[left][7]
            finalcube[left][6],temp[2] = temp[2],finalcube[left][6]
            finalcube[left][3],temp[1] = temp[1],finalcube[left][3]
            finalcube[left][0],temp[2] = temp[2],finalcube[left][0]
        elif movetype == '1i':
            temp[0] = finalcube[eg4[3]][8]
            temp[1] = finalcube[eg4[3]][5]
            temp[2] = finalcube[eg4[3]][2]
            for i in range(3, -1, -1):
                if i != 0:
                    finalcube[eg4[i-1]][0],temp[0] = temp[0],finalcube[eg4[i-1]][0]
                    finalcube[eg4[i-1]][3],temp[1] = temp[1],finalcube[eg4[i-1]][3]
                    finalcube[eg4[i-1]][6],temp[2] = temp[2],finalcube[eg4[i-1]][6]
                elif i == 0:
                    finalcube[eg4[3]][8],temp[0] = temp[0],finalcube[eg4[3]][8]
                    finalcube[eg4[3]][5],temp[1] = temp[1],finalcube[eg4[3]][5]
                    finalcube[eg4[3]][2],temp[2] = temp[2],finalcube[eg4[3]][2]
            temp[0] = finalcube[left][0]
            temp[1] = finalcube[left][3]
            temp[2] = finalcube[left][6]
            finalcube[left][6],temp[0] = temp[0],finalcube[left][6]
            finalcube[left][7],temp[1] = temp[1],finalcube[left][7]
            finalcube[left][8],temp[2] = temp[2],finalcube[left][8]
            finalcube[left][5],temp[1] = temp[1],finalcube[left][5]
            finalcube[left][2],temp[2] = temp[2],finalcube[left][2]
            finalcube[left][1],temp[1] = temp[1],finalcube[left][1]
            finalcube[left][0],temp[2] = temp[2],finalcube[left][0]
            finalcube[left][3],temp[1] = temp[1],finalcube[left][3]
            finalcube[left][6],temp[2] = temp[2],finalcube[left][6]
        elif movetype == '2i':
            temp[0] = finalcube[eg4[3]][8]
            temp[1] = finalcube[eg4[3]][5]
            temp[2] = finalcube[eg4[3]][2]
            for i in range(3, -1, -1):
                if i != 0:
                    finalcube[eg4[i-1]][0],temp[0] = temp[0],finalcube[eg4[i-1]][0]
                    finalcube[eg4[i-1]][3],temp[1] = temp[1],finalcube[eg4[i-1]][3]
                    finalcube[eg4[i-1]][6],temp[2] = temp[2],finalcube[eg4[i-1]][6]
                elif i == 0:
                    finalcube[eg4[3]][8],temp[0] = temp[0],finalcube[eg4[3]][8]
                    finalcube[eg4[3]][5],temp[1] = temp[1],finalcube[eg4[3]][5]
                    finalcube[eg4[3]][2],temp[2] = temp[2],finalcube[eg4[3]][2]
            temp[0] = finalcube[left][0]
            temp[1] = finalcube[left][3]
            temp[2] = finalcube[left][6]
            finalcube[left][6],temp[0] = temp[0],finalcube[left][6]
            finalcube[left][7],temp[1] = temp[1],finalcube[left][7]
            finalcube[left][8],temp[2] = temp[2],finalcube[left][8]
            finalcube[left][5],temp[1] = temp[1],finalcube[left][5]
            finalcube[left][2],temp[2] = temp[2],finalcube[left][2]
            finalcube[left][1],temp[1] = temp[1],finalcube[left][1]
            finalcube[left][0],temp[2] = temp[2],finalcube[left][0]
            finalcube[left][3],temp[1] = temp[1],finalcube[left][3]
            finalcube[left][6],temp[2] = temp[2],finalcube[left][6]
            temp[0] = finalcube[eg4[3]][8]
            temp[1] = finalcube[eg4[3]][5]
            temp[2] = finalcube[eg4[3]][2]
            for i in range(3, -1, -1):
                if i != 0:
                    finalcube[eg4[i-1]][0],temp[0] = temp[0],finalcube[eg4[i-1]][0]
                    finalcube[eg4[i-1]][3],temp[1] = temp[1],finalcube[eg4[i-1]][3]
                    finalcube[eg4[i-1]][6],temp[2] = temp[2],finalcube[eg4[i-1]][6]
                elif i == 0:
                    finalcube[eg4[3]][8],temp[0] = temp[0],finalcube[eg4[3]][8]
                    finalcube[eg4[3]][5],temp[1] = temp[1],finalcube[eg4[3]][5]
                    finalcube[eg4[3]][2],temp[2] = temp[2],finalcube[eg4[3]][2]
            temp[0] = finalcube[left][0]
            temp[1] = finalcube[left][3]
            temp[2] = finalcube[left][6]
            finalcube[left][6],temp[0] = temp[0],finalcube[left][6]
            finalcube[left][7],temp[1] = temp[1],finalcube[left][7]
            finalcube[left][8],temp[2] = temp[2],finalcube[left][8]
            finalcube[left][5],temp[1] = temp[1],finalcube[left][5]
            finalcube[left][2],temp[2] = temp[2],finalcube[left][2]
            finalcube[left][1],temp[1] = temp[1],finalcube[left][1]
            finalcube[left][0],temp[2] = temp[2],finalcube[left][0]
            finalcube[left][3],temp[1] = temp[1],finalcube[left][3]
            finalcube[left][6],temp[2] = temp[2],finalcube[left][6]
    def B(movetype):
        temp = ['a','b','c']
        if movetype == 1:
            temp[0] = finalcube[eg1[0]][2]
            temp[1] = finalcube[eg1[0]][1]
            temp[2] = finalcube[eg1[0]][0]
            for i in range(0, 4):
                if i == 0:
                    finalcube[eg1[i+1]][0],temp[0] = temp[0],finalcube[eg1[i+1]][0]
                    finalcube[eg1[i+1]][3],temp[1] = temp[1],finalcube[eg1[i+1]][3]
                    finalcube[eg1[i+1]][6],temp[2] = temp[2],finalcube[eg1[i+1]][6]
                elif i == 1:
                    finalcube[eg1[i+1]][6],temp[0] = temp[0],finalcube[eg1[i+1]][6]
                    finalcube[eg1[i+1]][7],temp[1] = temp[1],finalcube[eg1[i+1]][7]
                    finalcube[eg1[i+1]][8],temp[2] = temp[2],finalcube[eg1[i+1]][8]
                elif i == 2:
                    finalcube[eg1[i+1]][8],temp[0] = temp[0],finalcube[eg1[i+1]][8]
                    finalcube[eg1[i+1]][5],temp[1] = temp[1],finalcube[eg1[i+1]][5]
                    finalcube[eg1[i+1]][2],temp[2] = temp[2],finalcube[eg1[i+1]][2]
                elif i == 3:
                    finalcube[eg1[0]][2],temp[0] = temp[0],finalcube[eg1[0]][2]
                    finalcube[eg1[0]][1],temp[1] = temp[1],finalcube[eg1[0]][1]
                    finalcube[eg1[0]][0],temp[2] = temp[2],finalcube[eg1[0]][0]
            temp[0] = finalcube[bottom][0]
            temp[1] = finalcube[bottom][1]
            temp[2] = finalcube[bottom][2]
            finalcube[bottom][2],temp[0] = temp[0],finalcube[bottom][2]
            finalcube[bottom][5],temp[1] = temp[1],finalcube[bottom][5]
            finalcube[bottom][8],temp[2] = temp[2],finalcube[bottom][8]
            finalcube[bottom][7],temp[1] = temp[1],finalcube[bottom][7]
            finalcube[bottom][6],temp[2] = temp[2],finalcube[bottom][6]
            finalcube[bottom][3],temp[1] = temp[1],finalcube[bottom][3]
            finalcube[bottom][0],temp[2] = temp[2],finalcube[bottom][0]
            finalcube[bottom][1],temp[1] = temp[1],finalcube[bottom][1]
            finalcube[bottom][2],temp[2] = temp[2],finalcube[bottom][2]
        elif movetype == 2:
            temp[0] = finalcube[eg1[0]][2]
            temp[1] = finalcube[eg1[0]][1]
            temp[2] = finalcube[eg1[0]][0]
            for i in range(0, 4):
                if i == 0:
                    finalcube[eg1[i+1]][0],temp[0] = temp[0],finalcube[eg1[i+1]][0]
                    finalcube[eg1[i+1]][3],temp[1] = temp[1],finalcube[eg1[i+1]][3]
                    finalcube[eg1[i+1]][6],temp[2] = temp[2],finalcube[eg1[i+1]][6]
                elif i == 1:
                    finalcube[eg1[i+1]][6],temp[0] = temp[0],finalcube[eg1[i+1]][6]
                    finalcube[eg1[i+1]][7],temp[1] = temp[1],finalcube[eg1[i+1]][7]
                    finalcube[eg1[i+1]][8],temp[2] = temp[2],finalcube[eg1[i+1]][8]
                elif i == 2:
                    finalcube[eg1[i+1]][8],temp[0] = temp[0],finalcube[eg1[i+1]][8]
                    finalcube[eg1[i+1]][5],temp[1] = temp[1],finalcube[eg1[i+1]][5]
                    finalcube[eg1[i+1]][2],temp[2] = temp[2],finalcube[eg1[i+1]][2]
                elif i == 3:
                    finalcube[eg1[0]][2],temp[0] = temp[0],finalcube[eg1[0]][2]
                    finalcube[eg1[0]][1],temp[1] = temp[1],finalcube[eg1[0]][1]
                    finalcube[eg1[0]][0],temp[2] = temp[2],finalcube[eg1[0]][0]
            temp[0] = finalcube[bottom][0]
            temp[1] = finalcube[bottom][1]
            temp[2] = finalcube[bottom][2]
            finalcube[bottom][2],temp[0] = temp[0],finalcube[bottom][2]
            finalcube[bottom][5],temp[1] = temp[1],finalcube[bottom][5]
            finalcube[bottom][8],temp[2] = temp[2],finalcube[bottom][8]
            finalcube[bottom][7],temp[1] = temp[1],finalcube[bottom][7]
            finalcube[bottom][6],temp[2] = temp[2],finalcube[bottom][6]
            finalcube[bottom][3],temp[1] = temp[1],finalcube[bottom][3]
            finalcube[bottom][0],temp[2] = temp[2],finalcube[bottom][0]
            finalcube[bottom][1],temp[1] = temp[1],finalcube[bottom][1]
            finalcube[bottom][2],temp[2] = temp[2],finalcube[bottom][2]
            temp[0] = finalcube[eg1[0]][2]
            temp[1] = finalcube[eg1[0]][1]
            temp[2] = finalcube[eg1[0]][0]
            for i in range(0, 4):
                if i == 0:
                    finalcube[eg1[i+1]][0],temp[0] = temp[0],finalcube[eg1[i+1]][0]
                    finalcube[eg1[i+1]][3],temp[1] = temp[1],finalcube[eg1[i+1]][3]
                    finalcube[eg1[i+1]][6],temp[2] = temp[2],finalcube[eg1[i+1]][6]
                elif i == 1:
                    finalcube[eg1[i+1]][6],temp[0] = temp[0],finalcube[eg1[i+1]][6]
                    finalcube[eg1[i+1]][7],temp[1] = temp[1],finalcube[eg1[i+1]][7]
                    finalcube[eg1[i+1]][8],temp[2] = temp[2],finalcube[eg1[i+1]][8]
                elif i == 2:
                    finalcube[eg1[i+1]][8],temp[0] = temp[0],finalcube[eg1[i+1]][8]
                    finalcube[eg1[i+1]][5],temp[1] = temp[1],finalcube[eg1[i+1]][5]
                    finalcube[eg1[i+1]][2],temp[2] = temp[2],finalcube[eg1[i+1]][2]
                elif i == 3:
                    finalcube[eg1[0]][2],temp[0] = temp[0],finalcube[eg1[0]][2]
                    finalcube[eg1[0]][1],temp[1] = temp[1],finalcube[eg1[0]][1]
                    finalcube[eg1[0]][0],temp[2] = temp[2],finalcube[eg1[0]][0]
            temp[0] = finalcube[bottom][0]
            temp[1] = finalcube[bottom][1]
            temp[2] = finalcube[bottom][2]
            finalcube[bottom][2],temp[0] = temp[0],finalcube[bottom][2]
            finalcube[bottom][5],temp[1] = temp[1],finalcube[bottom][5]
            finalcube[bottom][8],temp[2] = temp[2],finalcube[bottom][8]
            finalcube[bottom][7],temp[1] = temp[1],finalcube[bottom][7]
            finalcube[bottom][6],temp[2] = temp[2],finalcube[bottom][6]
            finalcube[bottom][3],temp[1] = temp[1],finalcube[bottom][3]
            finalcube[bottom][0],temp[2] = temp[2],finalcube[bottom][0]
            finalcube[bottom][1],temp[1] = temp[1],finalcube[bottom][1]
            finalcube[bottom][2],temp[2] = temp[2],finalcube[bottom][2]
        elif movetype == '1i':
            temp[0] = finalcube[eg1[3]][2]
            temp[1] = finalcube[eg1[3]][5]
            temp[2] = finalcube[eg1[3]][8]
            for i in range(3, -1, -1):
                if i == 3:
                    finalcube[eg1[i-1]][8],temp[0] = temp[0],finalcube[eg1[i-1]][8]
                    finalcube[eg1[i-1]][7],temp[1] = temp[1],finalcube[eg1[i-1]][7]
                    finalcube[eg1[i-1]][6],temp[2] = temp[2],finalcube[eg1[i-1]][6]
                elif i == 2:
                    finalcube[eg1[i-1]][6],temp[0] = temp[0],finalcube[eg1[i-1]][6]
                    finalcube[eg1[i-1]][3],temp[1] = temp[1],finalcube[eg1[i-1]][3]
                    finalcube[eg1[i-1]][0],temp[2] = temp[2],finalcube[eg1[i-1]][0]
                elif i == 1:
                    finalcube[eg1[i-1]][0],temp[0] = temp[0],finalcube[eg1[i-1]][0]
                    finalcube[eg1[i-1]][1],temp[1] = temp[1],finalcube[eg1[i-1]][1]
                    finalcube[eg1[i-1]][2],temp[2] = temp[2],finalcube[eg1[i-1]][2]
                elif i == 0:
                    finalcube[eg1[3]][2],temp[0] = temp[0],finalcube[eg1[3]][2]
                    finalcube[eg1[3]][5],temp[1] = temp[1],finalcube[eg1[3]][5]
                    finalcube[eg1[3]][8],temp[2] = temp[2],finalcube[eg1[3]][8]
            temp[0] = finalcube[bottom][2]
            temp[1] = finalcube[bottom][1]
            temp[2] = finalcube[bottom][0]
            finalcube[bottom][0],temp[0] = temp[0],finalcube[bottom][0]
            finalcube[bottom][3],temp[1] = temp[1],finalcube[bottom][3]
            finalcube[bottom][6],temp[2] = temp[2],finalcube[bottom][6]
            finalcube[bottom][7],temp[1] = temp[1],finalcube[bottom][7]
            finalcube[bottom][8],temp[2] = temp[2],finalcube[bottom][8]
            finalcube[bottom][5],temp[1] = temp[1],finalcube[bottom][5]
            finalcube[bottom][2],temp[2] = temp[2],finalcube[bottom][2]
            finalcube[bottom][1],temp[1] = temp[1],finalcube[bottom][1]
            finalcube[bottom][0],temp[2] = temp[2],finalcube[bottom][0]
        elif movetype == '2i':
            temp[0] = finalcube[eg1[3]][2]
            temp[1] = finalcube[eg1[3]][5]
            temp[2] = finalcube[eg1[3]][8]
            for i in range(3, -1, -1):
                if i == 3:
                    finalcube[eg1[i-1]][8],temp[0] = temp[0],finalcube[eg1[i-1]][8]
                    finalcube[eg1[i-1]][7],temp[1] = temp[1],finalcube[eg1[i-1]][7]
                    finalcube[eg1[i-1]][6],temp[2] = temp[2],finalcube[eg1[i-1]][6]
                elif i == 2:
                    finalcube[eg1[i-1]][6],temp[0] = temp[0],finalcube[eg1[i-1]][6]
                    finalcube[eg1[i-1]][3],temp[1] = temp[1],finalcube[eg1[i-1]][3]
                    finalcube[eg1[i-1]][0],temp[2] = temp[2],finalcube[eg1[i-1]][0]
                elif i == 1:
                    finalcube[eg1[i-1]][0],temp[0] = temp[0],finalcube[eg1[i-1]][0]
                    finalcube[eg1[i-1]][1],temp[1] = temp[1],finalcube[eg1[i-1]][1]
                    finalcube[eg1[i-1]][2],temp[2] = temp[2],finalcube[eg1[i-1]][2]
                elif i == 0:
                    finalcube[eg1[3]][2],temp[0] = temp[0],finalcube[eg1[3]][2]
                    finalcube[eg1[3]][5],temp[1] = temp[1],finalcube[eg1[3]][5]
                    finalcube[eg1[3]][8],temp[2] = temp[2],finalcube[eg1[3]][8]
            temp[0] = finalcube[bottom][2]
            temp[1] = finalcube[bottom][1]
            temp[2] = finalcube[bottom][0]
            finalcube[bottom][0],temp[0] = temp[0],finalcube[bottom][0]
            finalcube[bottom][3],temp[1] = temp[1],finalcube[bottom][3]
            finalcube[bottom][6],temp[2] = temp[2],finalcube[bottom][6]
            finalcube[bottom][7],temp[1] = temp[1],finalcube[bottom][7]
            finalcube[bottom][8],temp[2] = temp[2],finalcube[bottom][8]
            finalcube[bottom][5],temp[1] = temp[1],finalcube[bottom][5]
            finalcube[bottom][2],temp[2] = temp[2],finalcube[bottom][2]
            finalcube[bottom][1],temp[1] = temp[1],finalcube[bottom][1]
            finalcube[bottom][0],temp[2] = temp[2],finalcube[bottom][0]
            temp[0] = finalcube[eg1[3]][2]
            temp[1] = finalcube[eg1[3]][5]
            temp[2] = finalcube[eg1[3]][8]
            for i in range(3, -1, -1):
                if i == 3:
                    finalcube[eg1[i-1]][8],temp[0] = temp[0],finalcube[eg1[i-1]][8]
                    finalcube[eg1[i-1]][7],temp[1] = temp[1],finalcube[eg1[i-1]][7]
                    finalcube[eg1[i-1]][6],temp[2] = temp[2],finalcube[eg1[i-1]][6]
                elif i == 2:
                    finalcube[eg1[i-1]][6],temp[0] = temp[0],finalcube[eg1[i-1]][6]
                    finalcube[eg1[i-1]][3],temp[1] = temp[1],finalcube[eg1[i-1]][3]
                    finalcube[eg1[i-1]][0],temp[2] = temp[2],finalcube[eg1[i-1]][0]
                elif i == 1:
                    finalcube[eg1[i-1]][0],temp[0] = temp[0],finalcube[eg1[i-1]][0]
                    finalcube[eg1[i-1]][1],temp[1] = temp[1],finalcube[eg1[i-1]][1]
                    finalcube[eg1[i-1]][2],temp[2] = temp[2],finalcube[eg1[i-1]][2]
                elif i == 0:
                    finalcube[eg1[3]][2],temp[0] = temp[0],finalcube[eg1[3]][2]
                    finalcube[eg1[3]][5],temp[1] = temp[1],finalcube[eg1[3]][5]
                    finalcube[eg1[3]][8],temp[2] = temp[2],finalcube[eg1[3]][8]
            temp[0] = finalcube[bottom][2]
            temp[1] = finalcube[bottom][1]
            temp[2] = finalcube[bottom][0]
            finalcube[bottom][0],temp[0] = temp[0],finalcube[bottom][0]
            finalcube[bottom][3],temp[1] = temp[1],finalcube[bottom][3]
            finalcube[bottom][6],temp[2] = temp[2],finalcube[bottom][6]
            finalcube[bottom][7],temp[1] = temp[1],finalcube[bottom][7]
            finalcube[bottom][8],temp[2] = temp[2],finalcube[bottom][8]
            finalcube[bottom][5],temp[1] = temp[1],finalcube[bottom][5]
            finalcube[bottom][2],temp[2] = temp[2],finalcube[bottom][2]
            finalcube[bottom][1],temp[1] = temp[1],finalcube[bottom][1]
            finalcube[bottom][0],temp[2] = temp[2],finalcube[bottom][0]
    def M(movetype):
        temp = ['a','b','c']
        if movetype == 1:
            temp[0] = finalcube[eg4[0]][1]
            temp[1] = finalcube[eg4[0]][4]
            temp[2] = finalcube[eg4[0]][7]
            for i in range(0, 4):
                if i < 2:
                    finalcube[eg4[i+1]][1],temp[0] = temp[0],finalcube[eg4[i+1]][1]
                    finalcube[eg4[i+1]][4],temp[1] = temp[1],finalcube[eg4[i+1]][4]
                    finalcube[eg4[i+1]][7],temp[2] = temp[2],finalcube[eg4[i+1]][7]
                elif i == 2:
                    finalcube[eg4[i+1]][7],temp[0] = temp[0],finalcube[eg4[i+1]][7]
                    finalcube[eg4[i+1]][4],temp[1] = temp[1],finalcube[eg4[i+1]][4]
                    finalcube[eg4[i+1]][1],temp[2] = temp[2],finalcube[eg4[i+1]][1]
                elif i == 3:
                    finalcube[eg4[0]][1],temp[0] = temp[0],finalcube[eg4[0]][1]
                    finalcube[eg4[0]][4],temp[1] = temp[1],finalcube[eg4[0]][4]
                    finalcube[eg4[0]][7],temp[2] = temp[2],finalcube[eg4[0]][7]
        elif movetype == 2:
            temp[0] = finalcube[eg4[0]][1]
            temp[1] = finalcube[eg4[0]][4]
            temp[2] = finalcube[eg4[0]][7]
            for i in range(0, 4):
                if i < 2:
                    finalcube[eg4[i+1]][1],temp[0] = temp[0],finalcube[eg4[i+1]][1]
                    finalcube[eg4[i+1]][4],temp[1] = temp[1],finalcube[eg4[i+1]][4]
                    finalcube[eg4[i+1]][7],temp[2] = temp[2],finalcube[eg4[i+1]][7]
                elif i == 2:
                    finalcube[eg4[i+1]][7],temp[0] = temp[0],finalcube[eg4[i+1]][7]
                    finalcube[eg4[i+1]][4],temp[1] = temp[1],finalcube[eg4[i+1]][4]
                    finalcube[eg4[i+1]][1],temp[2] = temp[2],finalcube[eg4[i+1]][1]
                elif i == 3:
                    finalcube[eg4[0]][1],temp[0] = temp[0],finalcube[eg4[0]][1]
                    finalcube[eg4[0]][4],temp[1] = temp[1],finalcube[eg4[0]][4]
                    finalcube[eg4[0]][7],temp[2] = temp[2],finalcube[eg4[0]][7]
            temp[0] = finalcube[eg4[0]][1]
            temp[1] = finalcube[eg4[0]][4]
            temp[2] = finalcube[eg4[0]][7]
            for i in range(0, 4):
                if i < 2:
                    finalcube[eg4[i+1]][1],temp[0] = temp[0],finalcube[eg4[i+1]][1]
                    finalcube[eg4[i+1]][4],temp[1] = temp[1],finalcube[eg4[i+1]][4]
                    finalcube[eg4[i+1]][7],temp[2] = temp[2],finalcube[eg4[i+1]][7]
                elif i == 2:
                    finalcube[eg4[i+1]][7],temp[0] = temp[0],finalcube[eg4[i+1]][7]
                    finalcube[eg4[i+1]][4],temp[1] = temp[1],finalcube[eg4[i+1]][4]
                    finalcube[eg4[i+1]][1],temp[2] = temp[2],finalcube[eg4[i+1]][1]
                elif i == 3:
                    finalcube[eg4[0]][1],temp[0] = temp[0],finalcube[eg4[0]][1]
                    finalcube[eg4[0]][4],temp[1] = temp[1],finalcube[eg4[0]][4]
                    finalcube[eg4[0]][7],temp[2] = temp[2],finalcube[eg4[0]][7]
        elif movetype == '1i':
            temp[0] = finalcube[eg4[3]][1]
            temp[1] = finalcube[eg4[3]][4]
            temp[2] = finalcube[eg4[3]][7]
            for i in range(3, -1, -1):
                if i == 3:
                    finalcube[eg4[i-1]][7],temp[0] = temp[0],finalcube[eg4[i-1]][7]
                    finalcube[eg4[i-1]][4],temp[1] = temp[1],finalcube[eg4[i-1]][4]
                    finalcube[eg4[i-1]][1],temp[2] = temp[2],finalcube[eg4[i-1]][1]
                elif i == 2 or i == 1:
                    finalcube[eg4[i-1]][7],temp[0] = temp[0],finalcube[eg4[i-1]][7]
                    finalcube[eg4[i-1]][4],temp[1] = temp[1],finalcube[eg4[i-1]][4]
                    finalcube[eg4[i-1]][1],temp[2] = temp[2],finalcube[eg4[i-1]][1]
                elif i == 0:
                    finalcube[eg4[3]][1],temp[0] = temp[0],finalcube[eg4[3]][1]
                    finalcube[eg4[3]][4],temp[1] = temp[1],finalcube[eg4[3]][4]
                    finalcube[eg4[3]][7],temp[2] = temp[2],finalcube[eg4[3]][7]
        elif movetype == '2i':
            temp[0] = finalcube[eg4[3]][1]
            temp[1] = finalcube[eg4[3]][4]
            temp[2] = finalcube[eg4[3]][7]
            for i in range(3, -1, -1):
                if i == 3:
                    finalcube[eg4[i-1]][7],temp[0] = temp[0],finalcube[eg4[i-1]][7]
                    finalcube[eg4[i-1]][4],temp[1] = temp[1],finalcube[eg4[i-1]][4]
                    finalcube[eg4[i-1]][1],temp[2] = temp[2],finalcube[eg4[i-1]][1]
                elif i == 2 or i == 1:
                    finalcube[eg4[i-1]][7],temp[0] = temp[0],finalcube[eg4[i-1]][7]
                    finalcube[eg4[i-1]][4],temp[1] = temp[1],finalcube[eg4[i-1]][4]
                    finalcube[eg4[i-1]][1],temp[2] = temp[2],finalcube[eg4[i-1]][1]
                elif i == 0:
                    finalcube[eg4[3]][1],temp[0] = temp[0],finalcube[eg4[3]][1]
                    finalcube[eg4[3]][4],temp[1] = temp[1],finalcube[eg4[3]][4]
                    finalcube[eg4[3]][7],temp[2] = temp[2],finalcube[eg4[3]][7]
            temp[0] = finalcube[eg4[3]][1]
            temp[1] = finalcube[eg4[3]][4]
            temp[2] = finalcube[eg4[3]][7]
            for i in range(3, -1, -1):
                if i == 3:
                    finalcube[eg4[i-1]][7],temp[0] = temp[0],finalcube[eg4[i-1]][7]
                    finalcube[eg4[i-1]][4],temp[1] = temp[1],finalcube[eg4[i-1]][4]
                    finalcube[eg4[i-1]][1],temp[2] = temp[2],finalcube[eg4[i-1]][1]
                elif i == 2 or i == 1:
                    finalcube[eg4[i-1]][7],temp[0] = temp[0],finalcube[eg4[i-1]][7]
                    finalcube[eg4[i-1]][4],temp[1] = temp[1],finalcube[eg4[i-1]][4]
                    finalcube[eg4[i-1]][1],temp[2] = temp[2],finalcube[eg4[i-1]][1]
                elif i == 0:
                    finalcube[eg4[3]][1],temp[0] = temp[0],finalcube[eg4[3]][1]
                    finalcube[eg4[3]][4],temp[1] = temp[1],finalcube[eg4[3]][4]
                    finalcube[eg4[3]][7],temp[2] = temp[2],finalcube[eg4[3]][7]
    def E(movetype):
        temp = ['a','b','c']
        if movetype == 1:
            temp[0] = finalcube[eg2[3]][3]
            temp[1] = finalcube[eg2[3]][4]
            temp[2] = finalcube[eg2[3]][5]
            for i in range(3, -1, -1):
                if i != 0:
                    finalcube[eg2[i-1]][3],temp[0] = temp[0],finalcube[eg2[i-1]][3]
                    finalcube[eg2[i-1]][4],temp[1] = temp[1],finalcube[eg2[i-1]][4]
                    finalcube[eg2[i-1]][5],temp[2] = temp[2],finalcube[eg2[i-1]][5]
                elif i == 0:
                    finalcube[eg2[3]][3],temp[0] = temp[0],finalcube[eg2[3]][3]
                    finalcube[eg2[3]][4],temp[1] = temp[1],finalcube[eg2[3]][4]
                    finalcube[eg2[3]][5],temp[2] = temp[2],finalcube[eg2[3]][5]
        elif movetype == 2:
            temp[0] = finalcube[eg2[3]][3]
            temp[1] = finalcube[eg2[3]][4]
            temp[2] = finalcube[eg2[3]][5]
            for i in range(3, -1, -1):
                if i != 0:
                    finalcube[eg2[i-1]][3],temp[0] = temp[0],finalcube[eg2[i-1]][3]
                    finalcube[eg2[i-1]][4],temp[1] = temp[1],finalcube[eg2[i-1]][4]
                    finalcube[eg2[i-1]][5],temp[2] = temp[2],finalcube[eg2[i-1]][5]
                elif i == 0:
                    finalcube[eg2[3]][3],temp[0] = temp[0],finalcube[eg2[3]][3]
                    finalcube[eg2[3]][4],temp[1] = temp[1],finalcube[eg2[3]][4]
                    finalcube[eg2[3]][5],temp[2] = temp[2],finalcube[eg2[3]][5]
            temp[0] = finalcube[eg2[3]][3]
            temp[1] = finalcube[eg2[3]][4]
            temp[2] = finalcube[eg2[3]][5]
            for i in range(3, -1, -1):
                if i != 0:
                    finalcube[eg2[i-1]][3],temp[0] = temp[0],finalcube[eg2[i-1]][3]
                    finalcube[eg2[i-1]][4],temp[1] = temp[1],finalcube[eg2[i-1]][4]
                    finalcube[eg2[i-1]][5],temp[2] = temp[2],finalcube[eg2[i-1]][5]
                elif i == 0:
                    finalcube[eg2[3]][3],temp[0] = temp[0],finalcube[eg2[3]][3]
                    finalcube[eg2[3]][4],temp[1] = temp[1],finalcube[eg2[3]][4]
                    finalcube[eg2[3]][5],temp[2] = temp[2],finalcube[eg2[3]][5]
        elif movetype == '1i':
            temp[0] = finalcube[eg2[0]][3]
            temp[1] = finalcube[eg2[0]][4]
            temp[2] = finalcube[eg2[0]][5]
            for i in range(0, 4):
                if i == 1 or i == 2:
                    finalcube[eg2[i+1]][3],temp[0] = temp[0],finalcube[eg2[i+1]][3]
                    finalcube[eg2[i+1]][4],temp[1] = temp[1],finalcube[eg2[i+1]][4]
                    finalcube[eg2[i+1]][5],temp[2] = temp[2],finalcube[eg2[i+1]][5]
                elif i == 0:
                    finalcube[eg2[i+1]][3],temp[0] = temp[0],finalcube[eg2[i+1]][3]
                    finalcube[eg2[i+1]][4],temp[1] = temp[1],finalcube[eg2[i+1]][4]
                    finalcube[eg2[i+1]][5],temp[2] = temp[2],finalcube[eg2[i+1]][5]
                elif i == 3:
                    finalcube[eg2[0]][5],temp[0] = temp[0],finalcube[eg2[0]][5]
                    finalcube[eg2[0]][4],temp[1] = temp[1],finalcube[eg2[0]][4]
                    finalcube[eg2[0]][3],temp[2] = temp[2],finalcube[eg2[0]][3]
        elif movetype == '2i':
            temp[0] = finalcube[eg2[0]][3]
            temp[1] = finalcube[eg2[0]][4]
            temp[2] = finalcube[eg2[0]][5]
            for i in range(0, 4):
                if i == 1 or i == 2:
                    finalcube[eg2[i+1]][3],temp[0] = temp[0],finalcube[eg2[i+1]][3]
                    finalcube[eg2[i+1]][4],temp[1] = temp[1],finalcube[eg2[i+1]][4]
                    finalcube[eg2[i+1]][5],temp[2] = temp[2],finalcube[eg2[i+1]][5]
                elif i == 0:
                    finalcube[eg2[i+1]][3],temp[0] = temp[0],finalcube[eg2[i+1]][3]
                    finalcube[eg2[i+1]][4],temp[1] = temp[1],finalcube[eg2[i+1]][4]
                    finalcube[eg2[i+1]][5],temp[2] = temp[2],finalcube[eg2[i+1]][5]
                elif i == 3:
                    finalcube[eg2[0]][5],temp[0] = temp[0],finalcube[eg2[0]][5]
                    finalcube[eg2[0]][4],temp[1] = temp[1],finalcube[eg2[0]][4]
                    finalcube[eg2[0]][3],temp[2] = temp[2],finalcube[eg2[0]][3]
            temp[0] = finalcube[eg2[0]][3]
            temp[1] = finalcube[eg2[0]][4]
            temp[2] = finalcube[eg2[0]][5]
            for i in range(0, 4):
                if i == 1 or i == 2:
                    finalcube[eg2[i+1]][3],temp[0] = temp[0],finalcube[eg2[i+1]][3]
                    finalcube[eg2[i+1]][4],temp[1] = temp[1],finalcube[eg2[i+1]][4]
                    finalcube[eg2[i+1]][5],temp[2] = temp[2],finalcube[eg2[i+1]][5]
                elif i == 0:
                    finalcube[eg2[i+1]][3],temp[0] = temp[0],finalcube[eg2[i+1]][3]
                    finalcube[eg2[i+1]][4],temp[1] = temp[1],finalcube[eg2[i+1]][4]
                    finalcube[eg2[i+1]][5],temp[2] = temp[2],finalcube[eg2[i+1]][5]
                elif i == 3:
                    finalcube[eg2[0]][5],temp[0] = temp[0],finalcube[eg2[0]][5]
                    finalcube[eg2[0]][4],temp[1] = temp[1],finalcube[eg2[0]][4]
                    finalcube[eg2[0]][3],temp[2] = temp[2],finalcube[eg2[0]][3]
    def S(movetype):
        temp = ['a','b','c']
        if movetype == 1:
            temp[0] = finalcube[eg1[3]][1]
            temp[1] = finalcube[eg1[3]][4]
            temp[2] = finalcube[eg1[3]][7]
            for i in range(3, -1, -1):
                if i == 3:
                    finalcube[eg1[i-1]][5],temp[0] = temp[0],finalcube[eg1[i-1]][5]
                    finalcube[eg1[i-1]][4],temp[1] = temp[1],finalcube[eg1[i-1]][4]
                    finalcube[eg1[i-1]][3],temp[2] = temp[2],finalcube[eg1[i-1]][3]
                elif i == 2:
                    finalcube[eg1[i-1]][7],temp[0] = temp[0],finalcube[eg1[i-1]][7]
                    finalcube[eg1[i-1]][4],temp[1] = temp[1],finalcube[eg1[i-1]][4]
                    finalcube[eg1[i-1]][1],temp[2] = temp[2],finalcube[eg1[i-1]][1]
                elif i == 1:
                    finalcube[eg1[i-1]][3],temp[0] = temp[0],finalcube[eg1[i-1]][3]
                    finalcube[eg1[i-1]][4],temp[1] = temp[1],finalcube[eg1[i-1]][4]
                    finalcube[eg1[i-1]][5],temp[2] = temp[2],finalcube[eg1[i-1]][5]
                elif i == 0:
                    finalcube[eg1[3]][1],temp[0] = temp[0],finalcube[eg1[3]][1]
                    finalcube[eg1[3]][4],temp[1] = temp[1],finalcube[eg1[3]][4]
                    finalcube[eg1[3]][7],temp[2] = temp[2],finalcube[eg1[3]][7]
        elif movetype == 2:
            temp[0] = finalcube[eg1[3]][1]
            temp[1] = finalcube[eg1[3]][4]
            temp[2] = finalcube[eg1[3]][7]
            for i in range(3, -1, -1):
                if i == 3:
                    finalcube[eg1[i-1]][5],temp[0] = temp[0],finalcube[eg1[i-1]][5]
                    finalcube[eg1[i-1]][4],temp[1] = temp[1],finalcube[eg1[i-1]][4]
                    finalcube[eg1[i-1]][3],temp[2] = temp[2],finalcube[eg1[i-1]][3]
                elif i == 2:
                    finalcube[eg1[i-1]][7],temp[0] = temp[0],finalcube[eg1[i-1]][7]
                    finalcube[eg1[i-1]][4],temp[1] = temp[1],finalcube[eg1[i-1]][4]
                    finalcube[eg1[i-1]][1],temp[2] = temp[2],finalcube[eg1[i-1]][1]
                elif i == 1:
                    finalcube[eg1[i-1]][3],temp[0] = temp[0],finalcube[eg1[i-1]][3]
                    finalcube[eg1[i-1]][4],temp[1] = temp[1],finalcube[eg1[i-1]][4]
                    finalcube[eg1[i-1]][5],temp[2] = temp[2],finalcube[eg1[i-1]][5]
                elif i == 0:
                    finalcube[eg1[3]][1],temp[0] = temp[0],finalcube[eg1[3]][1]
                    finalcube[eg1[3]][4],temp[1] = temp[1],finalcube[eg1[3]][4]
                    finalcube[eg1[3]][7],temp[2] = temp[2],finalcube[eg1[3]][7]
            temp[0] = finalcube[eg1[3]][1]
            temp[1] = finalcube[eg1[3]][4]
            temp[2] = finalcube[eg1[3]][7]
            for i in range(3, -1, -1):
                if i == 3:
                    finalcube[eg1[i-1]][5],temp[0] = temp[0],finalcube[eg1[i-1]][5]
                    finalcube[eg1[i-1]][4],temp[1] = temp[1],finalcube[eg1[i-1]][4]
                    finalcube[eg1[i-1]][3],temp[2] = temp[2],finalcube[eg1[i-1]][3]
                elif i == 2:
                    finalcube[eg1[i-1]][7],temp[0] = temp[0],finalcube[eg1[i-1]][7]
                    finalcube[eg1[i-1]][4],temp[1] = temp[1],finalcube[eg1[i-1]][4]
                    finalcube[eg1[i-1]][1],temp[2] = temp[2],finalcube[eg1[i-1]][1]
                elif i == 1:
                    finalcube[eg1[i-1]][3],temp[0] = temp[0],finalcube[eg1[i-1]][3]
                    finalcube[eg1[i-1]][4],temp[1] = temp[1],finalcube[eg1[i-1]][4]
                    finalcube[eg1[i-1]][5],temp[2] = temp[2],finalcube[eg1[i-1]][5]
                elif i == 0:
                    finalcube[eg1[3]][1],temp[0] = temp[0],finalcube[eg1[3]][1]
                    finalcube[eg1[3]][4],temp[1] = temp[1],finalcube[eg1[3]][4]
                    finalcube[eg1[3]][7],temp[2] = temp[2],finalcube[eg1[3]][7]
        elif movetype == '1i':
            temp[0] = finalcube[eg1[0]][5]
            temp[1] = finalcube[eg1[0]][4]
            temp[2] = finalcube[eg1[0]][3]
            for i in range(0, 4):
                if i == 0:
                    finalcube[eg1[i+1]][1],temp[0] = temp[0],finalcube[eg1[i+1]][1]
                    finalcube[eg1[i+1]][4],temp[1] = temp[1],finalcube[eg1[i+1]][4]
                    finalcube[eg1[i+1]][7],temp[2] = temp[2],finalcube[eg1[i+1]][7]
                elif i == 1:
                    finalcube[eg1[i+1]][3],temp[0] = temp[0],finalcube[eg1[i+1]][3]
                    finalcube[eg1[i+1]][4],temp[1] = temp[1],finalcube[eg1[i+1]][4]
                    finalcube[eg1[i+1]][5],temp[2] = temp[2],finalcube[eg1[i+1]][5]
                elif i == 2:
                    finalcube[eg1[i+1]][7],temp[0] = temp[0],finalcube[eg1[i+1]][7]
                    finalcube[eg1[i+1]][4],temp[1] = temp[1],finalcube[eg1[i+1]][4]
                    finalcube[eg1[i+1]][1],temp[2] = temp[2],finalcube[eg1[i+1]][1]
                elif i == 3:
                    finalcube[eg1[0]][5],temp[0] = temp[0],finalcube[eg1[0]][5]
                    finalcube[eg1[0]][4],temp[1] = temp[1],finalcube[eg1[0]][4]
                    finalcube[eg1[0]][3],temp[2] = temp[2],finalcube[eg1[0]][3]
        elif movetype == '2i':
            temp[0] = finalcube[eg1[0]][5]
            temp[1] = finalcube[eg1[0]][4]
            temp[2] = finalcube[eg1[0]][3]
            for i in range(0, 4):
                if i == 0:
                    finalcube[eg1[i+1]][1],temp[0] = temp[0],finalcube[eg1[i+1]][1]
                    finalcube[eg1[i+1]][4],temp[1] = temp[1],finalcube[eg1[i+1]][4]
                    finalcube[eg1[i+1]][7],temp[2] = temp[2],finalcube[eg1[i+1]][7]
                elif i == 1:
                    finalcube[eg1[i+1]][3],temp[0] = temp[0],finalcube[eg1[i+1]][3]
                    finalcube[eg1[i+1]][4],temp[1] = temp[1],finalcube[eg1[i+1]][4]
                    finalcube[eg1[i+1]][5],temp[2] = temp[2],finalcube[eg1[i+1]][5]
                elif i == 2:
                    finalcube[eg1[i+1]][7],temp[0] = temp[0],finalcube[eg1[i+1]][7]
                    finalcube[eg1[i+1]][4],temp[1] = temp[1],finalcube[eg1[i+1]][4]
                    finalcube[eg1[i+1]][1],temp[2] = temp[2],finalcube[eg1[i+1]][1]
                elif i == 3:
                    finalcube[eg1[0]][5],temp[0] = temp[0],finalcube[eg1[0]][5]
                    finalcube[eg1[0]][4],temp[1] = temp[1],finalcube[eg1[0]][4]
                    finalcube[eg1[0]][3],temp[2] = temp[2],finalcube[eg1[0]][3]
            temp[0] = finalcube[eg1[0]][5]
            temp[1] = finalcube[eg1[0]][4]
            temp[2] = finalcube[eg1[0]][3]
            for i in range(0, 4):
                if i == 0:
                    finalcube[eg1[i+1]][1],temp[0] = temp[0],finalcube[eg1[i+1]][1]
                    finalcube[eg1[i+1]][4],temp[1] = temp[1],finalcube[eg1[i+1]][4]
                    finalcube[eg1[i+1]][7],temp[2] = temp[2],finalcube[eg1[i+1]][7]
                elif i == 1:
                    finalcube[eg1[i+1]][3],temp[0] = temp[0],finalcube[eg1[i+1]][3]
                    finalcube[eg1[i+1]][4],temp[1] = temp[1],finalcube[eg1[i+1]][4]
                    finalcube[eg1[i+1]][5],temp[2] = temp[2],finalcube[eg1[i+1]][5]
                elif i == 2:
                    finalcube[eg1[i+1]][7],temp[0] = temp[0],finalcube[eg1[i+1]][7]
                    finalcube[eg1[i+1]][4],temp[1] = temp[1],finalcube[eg1[i+1]][4]
                    finalcube[eg1[i+1]][1],temp[2] = temp[2],finalcube[eg1[i+1]][1]
                elif i == 3:
                    finalcube[eg1[0]][5],temp[0] = temp[0],finalcube[eg1[0]][5]
                    finalcube[eg1[0]][4],temp[1] = temp[1],finalcube[eg1[0]][4]
                    finalcube[eg1[0]][3],temp[2] = temp[2],finalcube[eg1[0]][3]
    def X(movetype):
        temp = ['a','b','c']
        if movetype == 1:
            temp[0] = finalcube[eg4[3]][8]
            temp[1] = finalcube[eg4[3]][5]
            temp[2] = finalcube[eg4[3]][2]
            for i in range(3, -1, -1):
                if i != 0:
                    finalcube[eg4[i-1]][0],temp[0] = temp[0],finalcube[eg4[i-1]][0]
                    finalcube[eg4[i-1]][3],temp[1] = temp[1],finalcube[eg4[i-1]][3]
                    finalcube[eg4[i-1]][6],temp[2] = temp[2],finalcube[eg4[i-1]][6]
                elif i == 0:
                    finalcube[eg4[3]][8],temp[0] = temp[0],finalcube[eg4[3]][8]
                    finalcube[eg4[3]][5],temp[1] = temp[1],finalcube[eg4[3]][5]
                    finalcube[eg4[3]][2],temp[2] = temp[2],finalcube[eg4[3]][2]
            temp[0] = finalcube[left][0]
            temp[1] = finalcube[left][3]
            temp[2] = finalcube[left][6]
            finalcube[left][6],temp[0] = temp[0],finalcube[left][6]
            finalcube[left][7],temp[1] = temp[1],finalcube[left][7]
            finalcube[left][8],temp[2] = temp[2],finalcube[left][8]
            finalcube[left][5],temp[1] = temp[1],finalcube[left][5]
            finalcube[left][2],temp[2] = temp[2],finalcube[left][2]
            finalcube[left][1],temp[1] = temp[1],finalcube[left][1]
            finalcube[left][0],temp[2] = temp[2],finalcube[left][0]
            finalcube[left][3],temp[1] = temp[1],finalcube[left][3]
            finalcube[left][6],temp[2] = temp[2],finalcube[left][6]
            temp[0] = finalcube[eg4[3]][1]
            temp[1] = finalcube[eg4[3]][4]
            temp[2] = finalcube[eg4[3]][7]
            for i in range(3, -1, -1):
                if i == 3:
                    finalcube[eg4[i-1]][7],temp[0] = temp[0],finalcube[eg4[i-1]][7]
                    finalcube[eg4[i-1]][4],temp[1] = temp[1],finalcube[eg4[i-1]][4]
                    finalcube[eg4[i-1]][1],temp[2] = temp[2],finalcube[eg4[i-1]][1]
                elif i == 2 or i == 1:
                    finalcube[eg4[i-1]][7],temp[0] = temp[0],finalcube[eg4[i-1]][7]
                    finalcube[eg4[i-1]][4],temp[1] = temp[1],finalcube[eg4[i-1]][4]
                    finalcube[eg4[i-1]][1],temp[2] = temp[2],finalcube[eg4[i-1]][1]
                elif i == 0:
                    finalcube[eg4[3]][1],temp[0] = temp[0],finalcube[eg4[3]][1]
                    finalcube[eg4[3]][4],temp[1] = temp[1],finalcube[eg4[3]][4]
                    finalcube[eg4[3]][7],temp[2] = temp[2],finalcube[eg4[3]][7]
            temp[0] = finalcube[eg4[3]][0]
            temp[1] = finalcube[eg4[3]][3]
            temp[2] = finalcube[eg4[3]][6]
            for i in range(3,-1, -1):
                if i != 0:
                    finalcube[eg4[i-1]][8],temp[0] = temp[0],finalcube[eg4[i-1]][8]
                    finalcube[eg4[i-1]][5],temp[1] = temp[1],finalcube[eg4[i-1]][5]
                    finalcube[eg4[i-1]][2],temp[2] = temp[2],finalcube[eg4[i-1]][2]
                elif i == 0:
                    finalcube[eg4[3]][0],temp[0] = temp[0],finalcube[eg4[3]][0]
                    finalcube[eg4[3]][3],temp[1] = temp[1],finalcube[eg4[3]][3]
                    finalcube[eg4[3]][6],temp[2] = temp[2],finalcube[eg4[3]][6]
            temp[0] = finalcube[right][6]
            temp[1] = finalcube[right][3]
            temp[2] = finalcube[right][0]
            finalcube[right][0],temp[0] = temp[0],finalcube[right][0]
            finalcube[right][1],temp[1] = temp[1],finalcube[right][1]
            finalcube[right][2],temp[2] = temp[2],finalcube[right][2]
            finalcube[right][5],temp[1] = temp[1],finalcube[right][5]
            finalcube[right][8],temp[2] = temp[2],finalcube[right][8]
            finalcube[right][7],temp[1] = temp[1],finalcube[right][7]
            finalcube[right][6],temp[2] = temp[2],finalcube[right][6]
            finalcube[right][3],temp[1] = temp[1],finalcube[right][3]
            finalcube[right][0],temp[2] = temp[2],finalcube[right][0]
        elif movetype == 2:
            temp[0] = finalcube[eg4[3]][8]
            temp[1] = finalcube[eg4[3]][5]
            temp[2] = finalcube[eg4[3]][2]
            for i in range(3, -1, -1):
                if i != 0:
                    finalcube[eg4[i-1]][0],temp[0] = temp[0],finalcube[eg4[i-1]][0]
                    finalcube[eg4[i-1]][3],temp[1] = temp[1],finalcube[eg4[i-1]][3]
                    finalcube[eg4[i-1]][6],temp[2] = temp[2],finalcube[eg4[i-1]][6]
                elif i == 0:
                    finalcube[eg4[3]][8],temp[0] = temp[0],finalcube[eg4[3]][8]
                    finalcube[eg4[3]][5],temp[1] = temp[1],finalcube[eg4[3]][5]
                    finalcube[eg4[3]][2],temp[2] = temp[2],finalcube[eg4[3]][2]
            temp[0] = finalcube[left][0]
            temp[1] = finalcube[left][3]
            temp[2] = finalcube[left][6]
            finalcube[left][6],temp[0] = temp[0],finalcube[left][6]
            finalcube[left][7],temp[1] = temp[1],finalcube[left][7]
            finalcube[left][8],temp[2] = temp[2],finalcube[left][8]
            finalcube[left][5],temp[1] = temp[1],finalcube[left][5]
            finalcube[left][2],temp[2] = temp[2],finalcube[left][2]
            finalcube[left][1],temp[1] = temp[1],finalcube[left][1]
            finalcube[left][0],temp[2] = temp[2],finalcube[left][0]
            finalcube[left][3],temp[1] = temp[1],finalcube[left][3]
            finalcube[left][6],temp[2] = temp[2],finalcube[left][6]
            temp[0] = finalcube[eg4[3]][1]
            temp[1] = finalcube[eg4[3]][4]
            temp[2] = finalcube[eg4[3]][7]
            for i in range(3, -1, -1):
                if i == 3:
                    finalcube[eg4[i-1]][7],temp[0] = temp[0],finalcube[eg4[i-1]][7]
                    finalcube[eg4[i-1]][4],temp[1] = temp[1],finalcube[eg4[i-1]][4]
                    finalcube[eg4[i-1]][1],temp[2] = temp[2],finalcube[eg4[i-1]][1]
                elif i == 2 or i == 1:
                    finalcube[eg4[i-1]][7],temp[0] = temp[0],finalcube[eg4[i-1]][7]
                    finalcube[eg4[i-1]][4],temp[1] = temp[1],finalcube[eg4[i-1]][4]
                    finalcube[eg4[i-1]][1],temp[2] = temp[2],finalcube[eg4[i-1]][1]
                elif i == 0:
                    finalcube[eg4[3]][1],temp[0] = temp[0],finalcube[eg4[3]][1]
                    finalcube[eg4[3]][4],temp[1] = temp[1],finalcube[eg4[3]][4]
                    finalcube[eg4[3]][7],temp[2] = temp[2],finalcube[eg4[3]][7]
            temp[0] = finalcube[eg4[3]][0]
            temp[1] = finalcube[eg4[3]][3]
            temp[2] = finalcube[eg4[3]][6]
            for i in range(3,-1, -1):
                if i != 0:
                    finalcube[eg4[i-1]][8],temp[0] = temp[0],finalcube[eg4[i-1]][8]
                    finalcube[eg4[i-1]][5],temp[1] = temp[1],finalcube[eg4[i-1]][5]
                    finalcube[eg4[i-1]][2],temp[2] = temp[2],finalcube[eg4[i-1]][2]
                elif i == 0:
                    finalcube[eg4[3]][0],temp[0] = temp[0],finalcube[eg4[3]][0]
                    finalcube[eg4[3]][3],temp[1] = temp[1],finalcube[eg4[3]][3]
                    finalcube[eg4[3]][6],temp[2] = temp[2],finalcube[eg4[3]][6]
            temp[0] = finalcube[right][6]
            temp[1] = finalcube[right][3]
            temp[2] = finalcube[right][0]
            finalcube[right][0],temp[0] = temp[0],finalcube[right][0]
            finalcube[right][1],temp[1] = temp[1],finalcube[right][1]
            finalcube[right][2],temp[2] = temp[2],finalcube[right][2]
            finalcube[right][5],temp[1] = temp[1],finalcube[right][5]
            finalcube[right][8],temp[2] = temp[2],finalcube[right][8]
            finalcube[right][7],temp[1] = temp[1],finalcube[right][7]
            finalcube[right][6],temp[2] = temp[2],finalcube[right][6]
            finalcube[right][3],temp[1] = temp[1],finalcube[right][3]
            finalcube[right][0],temp[2] = temp[2],finalcube[right][0]
            temp[0] = finalcube[eg4[3]][8]
            temp[1] = finalcube[eg4[3]][5]
            temp[2] = finalcube[eg4[3]][2]
            for i in range(3, -1, -1):
                if i != 0:
                    finalcube[eg4[i-1]][0],temp[0] = temp[0],finalcube[eg4[i-1]][0]
                    finalcube[eg4[i-1]][3],temp[1] = temp[1],finalcube[eg4[i-1]][3]
                    finalcube[eg4[i-1]][6],temp[2] = temp[2],finalcube[eg4[i-1]][6]
                elif i == 0:
                    finalcube[eg4[3]][8],temp[0] = temp[0],finalcube[eg4[3]][8]
                    finalcube[eg4[3]][5],temp[1] = temp[1],finalcube[eg4[3]][5]
                    finalcube[eg4[3]][2],temp[2] = temp[2],finalcube[eg4[3]][2]
            temp[0] = finalcube[left][0]
            temp[1] = finalcube[left][3]
            temp[2] = finalcube[left][6]
            finalcube[left][6],temp[0] = temp[0],finalcube[left][6]
            finalcube[left][7],temp[1] = temp[1],finalcube[left][7]
            finalcube[left][8],temp[2] = temp[2],finalcube[left][8]
            finalcube[left][5],temp[1] = temp[1],finalcube[left][5]
            finalcube[left][2],temp[2] = temp[2],finalcube[left][2]
            finalcube[left][1],temp[1] = temp[1],finalcube[left][1]
            finalcube[left][0],temp[2] = temp[2],finalcube[left][0]
            finalcube[left][3],temp[1] = temp[1],finalcube[left][3]
            finalcube[left][6],temp[2] = temp[2],finalcube[left][6]
            temp[0] = finalcube[eg4[3]][1]
            temp[1] = finalcube[eg4[3]][4]
            temp[2] = finalcube[eg4[3]][7]
            for i in range(3, -1, -1):
                if i == 3:
                    finalcube[eg4[i-1]][7],temp[0] = temp[0],finalcube[eg4[i-1]][7]
                    finalcube[eg4[i-1]][4],temp[1] = temp[1],finalcube[eg4[i-1]][4]
                    finalcube[eg4[i-1]][1],temp[2] = temp[2],finalcube[eg4[i-1]][1]
                elif i == 2 or i == 1:
                    finalcube[eg4[i-1]][7],temp[0] = temp[0],finalcube[eg4[i-1]][7]
                    finalcube[eg4[i-1]][4],temp[1] = temp[1],finalcube[eg4[i-1]][4]
                    finalcube[eg4[i-1]][1],temp[2] = temp[2],finalcube[eg4[i-1]][1]
                elif i == 0:
                    finalcube[eg4[3]][1],temp[0] = temp[0],finalcube[eg4[3]][1]
                    finalcube[eg4[3]][4],temp[1] = temp[1],finalcube[eg4[3]][4]
                    finalcube[eg4[3]][7],temp[2] = temp[2],finalcube[eg4[3]][7]
            temp[0] = finalcube[eg4[3]][0]
            temp[1] = finalcube[eg4[3]][3]
            temp[2] = finalcube[eg4[3]][6]
            for i in range(3,-1, -1):
                if i != 0:
                    finalcube[eg4[i-1]][8],temp[0] = temp[0],finalcube[eg4[i-1]][8]
                    finalcube[eg4[i-1]][5],temp[1] = temp[1],finalcube[eg4[i-1]][5]
                    finalcube[eg4[i-1]][2],temp[2] = temp[2],finalcube[eg4[i-1]][2]
                elif i == 0:
                    finalcube[eg4[3]][0],temp[0] = temp[0],finalcube[eg4[3]][0]
                    finalcube[eg4[3]][3],temp[1] = temp[1],finalcube[eg4[3]][3]
                    finalcube[eg4[3]][6],temp[2] = temp[2],finalcube[eg4[3]][6]
            temp[0] = finalcube[right][6]
            temp[1] = finalcube[right][3]
            temp[2] = finalcube[right][0]
            finalcube[right][0],temp[0] = temp[0],finalcube[right][0]
            finalcube[right][1],temp[1] = temp[1],finalcube[right][1]
            finalcube[right][2],temp[2] = temp[2],finalcube[right][2]
            finalcube[right][5],temp[1] = temp[1],finalcube[right][5]
            finalcube[right][8],temp[2] = temp[2],finalcube[right][8]
            finalcube[right][7],temp[1] = temp[1],finalcube[right][7]
            finalcube[right][6],temp[2] = temp[2],finalcube[right][6]
            finalcube[right][3],temp[1] = temp[1],finalcube[right][3]
            finalcube[right][0],temp[2] = temp[2],finalcube[right][0]
        elif movetype == '1i':
            temp[0] = finalcube[eg4[0]][0]
            temp[1] = finalcube[eg4[0]][3]
            temp[2] = finalcube[eg4[0]][6]
            for i in range(0, 4):
                if i < 2:
                    finalcube[eg4[i+1]][0],temp[0] = temp[0],finalcube[eg4[i+1]][0]
                    finalcube[eg4[i+1]][3],temp[1] = temp[1],finalcube[eg4[i+1]][3]
                    finalcube[eg4[i+1]][6],temp[2] = temp[2],finalcube[eg4[i+1]][6]
                elif i == 2:
                    finalcube[eg4[i+1]][8],temp[0] = temp[0],finalcube[eg4[i+1]][8]
                    finalcube[eg4[i+1]][5],temp[1] = temp[1],finalcube[eg4[i+1]][5]
                    finalcube[eg4[i+1]][2],temp[2] = temp[2],finalcube[eg4[i+1]][2]
                elif i == 3:
                    finalcube[eg4[0]][0],temp[0] = temp[0],finalcube[eg4[0]][0]
                    finalcube[eg4[0]][3],temp[1] = temp[1],finalcube[eg4[0]][3]
                    finalcube[eg4[0]][6],temp[2] = temp[2],finalcube[eg4[0]][6]
            temp[0] = finalcube[left][6]
            temp[1] = finalcube[left][3]
            temp[2] = finalcube[left][0]
            finalcube[left][0],temp[0] = temp[0],finalcube[left][0]
            finalcube[left][1],temp[1] = temp[1],finalcube[left][1]
            finalcube[left][2],temp[2] = temp[2],finalcube[left][2]
            finalcube[left][5],temp[1] = temp[1],finalcube[left][5]
            finalcube[left][8],temp[2] = temp[2],finalcube[left][8]
            finalcube[left][7],temp[1] = temp[1],finalcube[left][7]
            finalcube[left][6],temp[2] = temp[2],finalcube[left][6]
            finalcube[left][3],temp[1] = temp[1],finalcube[left][3]
            finalcube[left][0],temp[2] = temp[2],finalcube[left][0]
            temp[0] = finalcube[eg4[0]][1]
            temp[1] = finalcube[eg4[0]][4]
            temp[2] = finalcube[eg4[0]][7]
            for i in range(0, 4):
                if i < 2:
                    finalcube[eg4[i+1]][1],temp[0] = temp[0],finalcube[eg4[i+1]][1]
                    finalcube[eg4[i+1]][4],temp[1] = temp[1],finalcube[eg4[i+1]][4]
                    finalcube[eg4[i+1]][7],temp[2] = temp[2],finalcube[eg4[i+1]][7]
                elif i == 2:
                    finalcube[eg4[i+1]][7],temp[0] = temp[0],finalcube[eg4[i+1]][7]
                    finalcube[eg4[i+1]][4],temp[1] = temp[1],finalcube[eg4[i+1]][4]
                    finalcube[eg4[i+1]][1],temp[2] = temp[2],finalcube[eg4[i+1]][1]
                elif i == 3:
                    finalcube[eg4[0]][1],temp[0] = temp[0],finalcube[eg4[0]][1]
                    finalcube[eg4[0]][4],temp[1] = temp[1],finalcube[eg4[0]][4]
                    finalcube[eg4[0]][7],temp[2] = temp[2],finalcube[eg4[0]][7]
            temp[0] = finalcube[eg4[0]][2]
            temp[1] = finalcube[eg4[0]][5]
            temp[2] = finalcube[eg4[0]][8]
            for i in range(0, 4):
                if i < 2:
                    finalcube[eg4[i+1]][2],temp[0] = temp[0],finalcube[eg4[i+1]][2]
                    finalcube[eg4[i+1]][5],temp[1] = temp[1],finalcube[eg4[i+1]][5]
                    finalcube[eg4[i+1]][8],temp[2] = temp[2],finalcube[eg4[i+1]][8]
                elif i == 2:
                    finalcube[eg4[i+1]][6],temp[0] = temp[0],finalcube[eg4[i+1]][6]
                    finalcube[eg4[i+1]][3],temp[1] = temp[1],finalcube[eg4[i+1]][3]
                    finalcube[eg4[i+1]][0],temp[2] = temp[2],finalcube[eg4[i+1]][0]
                elif i == 3:
                    finalcube[eg4[0]][2],temp[0] = temp[0],finalcube[eg4[0]][2]
                    finalcube[eg4[0]][5],temp[1] = temp[1],finalcube[eg4[0]][5]
                    finalcube[eg4[0]][8],temp[2] = temp[2],finalcube[eg4[0]][8]
            temp[0] = finalcube[right][0]
            temp[1] = finalcube[right][3]
            temp[2] = finalcube[right][6]
            finalcube[right][6],temp[0] = temp[0],finalcube[right][6]
            finalcube[right][7],temp[1] = temp[1],finalcube[right][7]
            finalcube[right][8],temp[2] = temp[2],finalcube[right][8]
            finalcube[right][5],temp[1] = temp[1],finalcube[right][5]
            finalcube[right][2],temp[2] = temp[2],finalcube[right][2]
            finalcube[right][1],temp[1] = temp[1],finalcube[right][1]
            finalcube[right][0],temp[2] = temp[2],finalcube[right][0]
            finalcube[right][3],temp[1] = temp[1],finalcube[right][3]
            finalcube[right][6],temp[2] = temp[2],finalcube[right][6]
        elif movetype == '2i':
            temp[0] = finalcube[eg4[0]][0]
            temp[1] = finalcube[eg4[0]][3]
            temp[2] = finalcube[eg4[0]][6]
            for i in range(0, 4):
                if i < 2:
                    finalcube[eg4[i+1]][0],temp[0] = temp[0],finalcube[eg4[i+1]][0]
                    finalcube[eg4[i+1]][3],temp[1] = temp[1],finalcube[eg4[i+1]][3]
                    finalcube[eg4[i+1]][6],temp[2] = temp[2],finalcube[eg4[i+1]][6]
                elif i == 2:
                    finalcube[eg4[i+1]][8],temp[0] = temp[0],finalcube[eg4[i+1]][8]
                    finalcube[eg4[i+1]][5],temp[1] = temp[1],finalcube[eg4[i+1]][5]
                    finalcube[eg4[i+1]][2],temp[2] = temp[2],finalcube[eg4[i+1]][2]
                elif i == 3:
                    finalcube[eg4[0]][0],temp[0] = temp[0],finalcube[eg4[0]][0]
                    finalcube[eg4[0]][3],temp[1] = temp[1],finalcube[eg4[0]][3]
                    finalcube[eg4[0]][6],temp[2] = temp[2],finalcube[eg4[0]][6]
            temp[0] = finalcube[left][6]
            temp[1] = finalcube[left][3]
            temp[2] = finalcube[left][0]
            finalcube[left][0],temp[0] = temp[0],finalcube[left][0]
            finalcube[left][1],temp[1] = temp[1],finalcube[left][1]
            finalcube[left][2],temp[2] = temp[2],finalcube[left][2]
            finalcube[left][5],temp[1] = temp[1],finalcube[left][5]
            finalcube[left][8],temp[2] = temp[2],finalcube[left][8]
            finalcube[left][7],temp[1] = temp[1],finalcube[left][7]
            finalcube[left][6],temp[2] = temp[2],finalcube[left][6]
            finalcube[left][3],temp[1] = temp[1],finalcube[left][3]
            finalcube[left][0],temp[2] = temp[2],finalcube[left][0]
            temp[0] = finalcube[eg4[0]][1]
            temp[1] = finalcube[eg4[0]][4]
            temp[2] = finalcube[eg4[0]][7]
            for i in range(0, 4):
                if i < 2:
                    finalcube[eg4[i+1]][1],temp[0] = temp[0],finalcube[eg4[i+1]][1]
                    finalcube[eg4[i+1]][4],temp[1] = temp[1],finalcube[eg4[i+1]][4]
                    finalcube[eg4[i+1]][7],temp[2] = temp[2],finalcube[eg4[i+1]][7]
                elif i == 2:
                    finalcube[eg4[i+1]][7],temp[0] = temp[0],finalcube[eg4[i+1]][7]
                    finalcube[eg4[i+1]][4],temp[1] = temp[1],finalcube[eg4[i+1]][4]
                    finalcube[eg4[i+1]][1],temp[2] = temp[2],finalcube[eg4[i+1]][1]
                elif i == 3:
                    finalcube[eg4[0]][1],temp[0] = temp[0],finalcube[eg4[0]][1]
                    finalcube[eg4[0]][4],temp[1] = temp[1],finalcube[eg4[0]][4]
                    finalcube[eg4[0]][7],temp[2] = temp[2],finalcube[eg4[0]][7]
            temp[0] = finalcube[eg4[0]][2]
            temp[1] = finalcube[eg4[0]][5]
            temp[2] = finalcube[eg4[0]][8]
            for i in range(0, 4):
                if i < 2:
                    finalcube[eg4[i+1]][2],temp[0] = temp[0],finalcube[eg4[i+1]][2]
                    finalcube[eg4[i+1]][5],temp[1] = temp[1],finalcube[eg4[i+1]][5]
                    finalcube[eg4[i+1]][8],temp[2] = temp[2],finalcube[eg4[i+1]][8]
                elif i == 2:
                    finalcube[eg4[i+1]][6],temp[0] = temp[0],finalcube[eg4[i+1]][6]
                    finalcube[eg4[i+1]][3],temp[1] = temp[1],finalcube[eg4[i+1]][3]
                    finalcube[eg4[i+1]][0],temp[2] = temp[2],finalcube[eg4[i+1]][0]
                elif i == 3:
                    finalcube[eg4[0]][2],temp[0] = temp[0],finalcube[eg4[0]][2]
                    finalcube[eg4[0]][5],temp[1] = temp[1],finalcube[eg4[0]][5]
                    finalcube[eg4[0]][8],temp[2] = temp[2],finalcube[eg4[0]][8]
            temp[0] = finalcube[right][0]
            temp[1] = finalcube[right][3]
            temp[2] = finalcube[right][6]
            finalcube[right][6],temp[0] = temp[0],finalcube[right][6]
            finalcube[right][7],temp[1] = temp[1],finalcube[right][7]
            finalcube[right][8],temp[2] = temp[2],finalcube[right][8]
            finalcube[right][5],temp[1] = temp[1],finalcube[right][5]
            finalcube[right][2],temp[2] = temp[2],finalcube[right][2]
            finalcube[right][1],temp[1] = temp[1],finalcube[right][1]
            finalcube[right][0],temp[2] = temp[2],finalcube[right][0]
            finalcube[right][3],temp[1] = temp[1],finalcube[right][3]
            finalcube[right][6],temp[2] = temp[2],finalcube[right][6]
            temp[0] = finalcube[eg4[0]][0]
            temp[1] = finalcube[eg4[0]][3]
            temp[2] = finalcube[eg4[0]][6]
            for i in range(0, 4):
                if i < 2:
                    finalcube[eg4[i+1]][0],temp[0] = temp[0],finalcube[eg4[i+1]][0]
                    finalcube[eg4[i+1]][3],temp[1] = temp[1],finalcube[eg4[i+1]][3]
                    finalcube[eg4[i+1]][6],temp[2] = temp[2],finalcube[eg4[i+1]][6]
                elif i == 2:
                    finalcube[eg4[i+1]][8],temp[0] = temp[0],finalcube[eg4[i+1]][8]
                    finalcube[eg4[i+1]][5],temp[1] = temp[1],finalcube[eg4[i+1]][5]
                    finalcube[eg4[i+1]][2],temp[2] = temp[2],finalcube[eg4[i+1]][2]
                elif i == 3:
                    finalcube[eg4[0]][0],temp[0] = temp[0],finalcube[eg4[0]][0]
                    finalcube[eg4[0]][3],temp[1] = temp[1],finalcube[eg4[0]][3]
                    finalcube[eg4[0]][6],temp[2] = temp[2],finalcube[eg4[0]][6]
            temp[0] = finalcube[left][6]
            temp[1] = finalcube[left][3]
            temp[2] = finalcube[left][0]
            finalcube[left][0],temp[0] = temp[0],finalcube[left][0]
            finalcube[left][1],temp[1] = temp[1],finalcube[left][1]
            finalcube[left][2],temp[2] = temp[2],finalcube[left][2]
            finalcube[left][5],temp[1] = temp[1],finalcube[left][5]
            finalcube[left][8],temp[2] = temp[2],finalcube[left][8]
            finalcube[left][7],temp[1] = temp[1],finalcube[left][7]
            finalcube[left][6],temp[2] = temp[2],finalcube[left][6]
            finalcube[left][3],temp[1] = temp[1],finalcube[left][3]
            finalcube[left][0],temp[2] = temp[2],finalcube[left][0]
            temp[0] = finalcube[eg4[0]][1]
            temp[1] = finalcube[eg4[0]][4]
            temp[2] = finalcube[eg4[0]][7]
            for i in range(0, 4):
                if i < 2:
                    finalcube[eg4[i+1]][1],temp[0] = temp[0],finalcube[eg4[i+1]][1]
                    finalcube[eg4[i+1]][4],temp[1] = temp[1],finalcube[eg4[i+1]][4]
                    finalcube[eg4[i+1]][7],temp[2] = temp[2],finalcube[eg4[i+1]][7]
                elif i == 2:
                    finalcube[eg4[i+1]][7],temp[0] = temp[0],finalcube[eg4[i+1]][7]
                    finalcube[eg4[i+1]][4],temp[1] = temp[1],finalcube[eg4[i+1]][4]
                    finalcube[eg4[i+1]][1],temp[2] = temp[2],finalcube[eg4[i+1]][1]
                elif i == 3:
                    finalcube[eg4[0]][1],temp[0] = temp[0],finalcube[eg4[0]][1]
                    finalcube[eg4[0]][4],temp[1] = temp[1],finalcube[eg4[0]][4]
                    finalcube[eg4[0]][7],temp[2] = temp[2],finalcube[eg4[0]][7]
            temp[0] = finalcube[eg4[0]][2]
            temp[1] = finalcube[eg4[0]][5]
            temp[2] = finalcube[eg4[0]][8]
            for i in range(0, 4):
                if i < 2:
                    finalcube[eg4[i+1]][2],temp[0] = temp[0],finalcube[eg4[i+1]][2]
                    finalcube[eg4[i+1]][5],temp[1] = temp[1],finalcube[eg4[i+1]][5]
                    finalcube[eg4[i+1]][8],temp[2] = temp[2],finalcube[eg4[i+1]][8]
                elif i == 2:
                    finalcube[eg4[i+1]][6],temp[0] = temp[0],finalcube[eg4[i+1]][6]
                    finalcube[eg4[i+1]][3],temp[1] = temp[1],finalcube[eg4[i+1]][3]
                    finalcube[eg4[i+1]][0],temp[2] = temp[2],finalcube[eg4[i+1]][0]
                elif i == 3:
                    finalcube[eg4[0]][2],temp[0] = temp[0],finalcube[eg4[0]][2]
                    finalcube[eg4[0]][5],temp[1] = temp[1],finalcube[eg4[0]][5]
                    finalcube[eg4[0]][8],temp[2] = temp[2],finalcube[eg4[0]][8]
            temp[0] = finalcube[right][0]
            temp[1] = finalcube[right][3]
            temp[2] = finalcube[right][6]
            finalcube[right][6],temp[0] = temp[0],finalcube[right][6]
            finalcube[right][7],temp[1] = temp[1],finalcube[right][7]
            finalcube[right][8],temp[2] = temp[2],finalcube[right][8]
            finalcube[right][5],temp[1] = temp[1],finalcube[right][5]
            finalcube[right][2],temp[2] = temp[2],finalcube[right][2]
            finalcube[right][1],temp[1] = temp[1],finalcube[right][1]
            finalcube[right][0],temp[2] = temp[2],finalcube[right][0]
            finalcube[right][3],temp[1] = temp[1],finalcube[right][3]
            finalcube[right][6],temp[2] = temp[2],finalcube[right][6]
    def Y(movetype):
        temp = ['a','b','c']
        if movetype == 1:
            temp[0] = finalcube[eg2[0]][0]
            temp[1] = finalcube[eg2[0]][1]
            temp[2] = finalcube[eg2[0]][2]
            for i in range(0, 4):
                if i != 3:
                    finalcube[eg2[i+1]][0],temp[0] = temp[0],finalcube[eg2[i+1]][0]
                    finalcube[eg2[i+1]][1],temp[1] = temp[1],finalcube[eg2[i+1]][1]
                    finalcube[eg2[i+1]][2],temp[2] = temp[2],finalcube[eg2[i+1]][2]
                elif i == 3:
                    finalcube[eg2[0]][0],temp[0] = temp[0],finalcube[eg2[0]][0]
                    finalcube[eg2[0]][1],temp[1] = temp[1],finalcube[eg2[0]][1]
                    finalcube[eg2[0]][2],temp[2] = temp[2],finalcube[eg2[0]][2]
            temp[0] = finalcube[up][6]
            temp[1] = finalcube[up][3]
            temp[2] = finalcube[up][0]
            finalcube[up][0],temp[0] = temp[0],finalcube[up][0]
            finalcube[up][1],temp[1] = temp[1],finalcube[up][1]
            finalcube[up][2],temp[2] = temp[2],finalcube[up][2]
            finalcube[up][5],temp[1] = temp[1],finalcube[up][5]
            finalcube[up][8],temp[2] = temp[2],finalcube[up][8]
            finalcube[up][7],temp[1] = temp[1],finalcube[up][7]
            finalcube[up][6],temp[2] = temp[2],finalcube[up][6]
            finalcube[up][3],temp[1] = temp[1],finalcube[up][3]
            finalcube[up][0],temp[2] = temp[2],finalcube[up][0]
            temp[0] = finalcube[eg2[0]][3]
            temp[1] = finalcube[eg2[0]][4]
            temp[2] = finalcube[eg2[0]][5]
            for i in range(0, 4):
                if i == 1 or i == 2:
                    finalcube[eg2[i+1]][3],temp[0] = temp[0],finalcube[eg2[i+1]][3]
                    finalcube[eg2[i+1]][4],temp[1] = temp[1],finalcube[eg2[i+1]][4]
                    finalcube[eg2[i+1]][5],temp[2] = temp[2],finalcube[eg2[i+1]][5]
                elif i == 0:
                    finalcube[eg2[i+1]][3],temp[0] = temp[0],finalcube[eg2[i+1]][3]
                    finalcube[eg2[i+1]][4],temp[1] = temp[1],finalcube[eg2[i+1]][4]
                    finalcube[eg2[i+1]][5],temp[2] = temp[2],finalcube[eg2[i+1]][5]
                elif i == 3:
                    finalcube[eg2[0]][5],temp[0] = temp[0],finalcube[eg2[0]][5]
                    finalcube[eg2[0]][4],temp[1] = temp[1],finalcube[eg2[0]][4]
                    finalcube[eg2[0]][3],temp[2] = temp[2],finalcube[eg2[0]][3]
            temp[0] = finalcube[eg2[0]][6]
            temp[1] = finalcube[eg2[0]][7]
            temp[2] = finalcube[eg2[0]][8]
            for i in range(0, 4):
                if i != 3:
                    finalcube[eg2[i+1]][6],temp[0] = temp[0],finalcube[eg2[i+1]][6]
                    finalcube[eg2[i+1]][7],temp[1] = temp[1],finalcube[eg2[i+1]][7]
                    finalcube[eg2[i+1]][8],temp[2] = temp[2],finalcube[eg2[i+1]][8]
                elif i == 3:
                    finalcube[eg2[0]][6],temp[0] = temp[0],finalcube[eg2[0]][6]
                    finalcube[eg2[0]][7],temp[1] = temp[1],finalcube[eg2[0]][7]
                    finalcube[eg2[0]][8],temp[2] = temp[2],finalcube[eg2[0]][8]
            temp[0] = finalcube[down][0]
            temp[1] = finalcube[down][3]
            temp[2] = finalcube[down][6]
            finalcube[down][6],temp[0] = temp[0],finalcube[down][6]
            finalcube[down][7],temp[1] = temp[1],finalcube[down][7]
            finalcube[down][8],temp[2] = temp[2],finalcube[down][8]
            finalcube[down][5],temp[1] = temp[1],finalcube[down][5]
            finalcube[down][2],temp[2] = temp[2],finalcube[down][2]
            finalcube[down][1],temp[1] = temp[1],finalcube[down][1]
            finalcube[down][0],temp[2] = temp[2],finalcube[down][0]
            finalcube[down][3],temp[1] = temp[1],finalcube[down][3]
            finalcube[down][6],temp[2] = temp[2],finalcube[down][6]
        elif movetype == 2:
            temp[0] = finalcube[eg2[0]][0]
            temp[1] = finalcube[eg2[0]][1]
            temp[2] = finalcube[eg2[0]][2]
            for i in range(0, 4):
                if i != 3:
                    finalcube[eg2[i+1]][0],temp[0] = temp[0],finalcube[eg2[i+1]][0]
                    finalcube[eg2[i+1]][1],temp[1] = temp[1],finalcube[eg2[i+1]][1]
                    finalcube[eg2[i+1]][2],temp[2] = temp[2],finalcube[eg2[i+1]][2]
                elif i == 3:
                    finalcube[eg2[0]][0],temp[0] = temp[0],finalcube[eg2[0]][0]
                    finalcube[eg2[0]][1],temp[1] = temp[1],finalcube[eg2[0]][1]
                    finalcube[eg2[0]][2],temp[2] = temp[2],finalcube[eg2[0]][2]
            temp[0] = finalcube[up][6]
            temp[1] = finalcube[up][3]
            temp[2] = finalcube[up][0]
            finalcube[up][0],temp[0] = temp[0],finalcube[up][0]
            finalcube[up][1],temp[1] = temp[1],finalcube[up][1]
            finalcube[up][2],temp[2] = temp[2],finalcube[up][2]
            finalcube[up][5],temp[1] = temp[1],finalcube[up][5]
            finalcube[up][8],temp[2] = temp[2],finalcube[up][8]
            finalcube[up][7],temp[1] = temp[1],finalcube[up][7]
            finalcube[up][6],temp[2] = temp[2],finalcube[up][6]
            finalcube[up][3],temp[1] = temp[1],finalcube[up][3]
            finalcube[up][0],temp[2] = temp[2],finalcube[up][0]
            temp[0] = finalcube[eg2[0]][3]
            temp[1] = finalcube[eg2[0]][4]
            temp[2] = finalcube[eg2[0]][5]
            for i in range(0, 4):
                if i == 1 or i == 2:
                    finalcube[eg2[i+1]][3],temp[0] = temp[0],finalcube[eg2[i+1]][3]
                    finalcube[eg2[i+1]][4],temp[1] = temp[1],finalcube[eg2[i+1]][4]
                    finalcube[eg2[i+1]][5],temp[2] = temp[2],finalcube[eg2[i+1]][5]
                elif i == 0:
                    finalcube[eg2[i+1]][3],temp[0] = temp[0],finalcube[eg2[i+1]][3]
                    finalcube[eg2[i+1]][4],temp[1] = temp[1],finalcube[eg2[i+1]][4]
                    finalcube[eg2[i+1]][5],temp[2] = temp[2],finalcube[eg2[i+1]][5]
                elif i == 3:
                    finalcube[eg2[0]][5],temp[0] = temp[0],finalcube[eg2[0]][5]
                    finalcube[eg2[0]][4],temp[1] = temp[1],finalcube[eg2[0]][4]
                    finalcube[eg2[0]][3],temp[2] = temp[2],finalcube[eg2[0]][3]
            temp[0] = finalcube[eg2[0]][6]
            temp[1] = finalcube[eg2[0]][7]
            temp[2] = finalcube[eg2[0]][8]
            for i in range(0, 4):
                if i != 3:
                    finalcube[eg2[i+1]][6],temp[0] = temp[0],finalcube[eg2[i+1]][6]
                    finalcube[eg2[i+1]][7],temp[1] = temp[1],finalcube[eg2[i+1]][7]
                    finalcube[eg2[i+1]][8],temp[2] = temp[2],finalcube[eg2[i+1]][8]
                elif i == 3:
                    finalcube[eg2[0]][6],temp[0] = temp[0],finalcube[eg2[0]][6]
                    finalcube[eg2[0]][7],temp[1] = temp[1],finalcube[eg2[0]][7]
                    finalcube[eg2[0]][8],temp[2] = temp[2],finalcube[eg2[0]][8]
            temp[0] = finalcube[down][0]
            temp[1] = finalcube[down][3]
            temp[2] = finalcube[down][6]
            finalcube[down][6],temp[0] = temp[0],finalcube[down][6]
            finalcube[down][7],temp[1] = temp[1],finalcube[down][7]
            finalcube[down][8],temp[2] = temp[2],finalcube[down][8]
            finalcube[down][5],temp[1] = temp[1],finalcube[down][5]
            finalcube[down][2],temp[2] = temp[2],finalcube[down][2]
            finalcube[down][1],temp[1] = temp[1],finalcube[down][1]
            finalcube[down][0],temp[2] = temp[2],finalcube[down][0]
            finalcube[down][3],temp[1] = temp[1],finalcube[down][3]
            finalcube[down][6],temp[2] = temp[2],finalcube[down][6]
            temp[0] = finalcube[eg2[0]][0]
            temp[1] = finalcube[eg2[0]][1]
            temp[2] = finalcube[eg2[0]][2]
            for i in range(0, 4):
                if i != 3:
                    finalcube[eg2[i+1]][0],temp[0] = temp[0],finalcube[eg2[i+1]][0]
                    finalcube[eg2[i+1]][1],temp[1] = temp[1],finalcube[eg2[i+1]][1]
                    finalcube[eg2[i+1]][2],temp[2] = temp[2],finalcube[eg2[i+1]][2]
                elif i == 3:
                    finalcube[eg2[0]][0],temp[0] = temp[0],finalcube[eg2[0]][0]
                    finalcube[eg2[0]][1],temp[1] = temp[1],finalcube[eg2[0]][1]
                    finalcube[eg2[0]][2],temp[2] = temp[2],finalcube[eg2[0]][2]
            temp[0] = finalcube[up][6]
            temp[1] = finalcube[up][3]
            temp[2] = finalcube[up][0]
            finalcube[up][0],temp[0] = temp[0],finalcube[up][0]
            finalcube[up][1],temp[1] = temp[1],finalcube[up][1]
            finalcube[up][2],temp[2] = temp[2],finalcube[up][2]
            finalcube[up][5],temp[1] = temp[1],finalcube[up][5]
            finalcube[up][8],temp[2] = temp[2],finalcube[up][8]
            finalcube[up][7],temp[1] = temp[1],finalcube[up][7]
            finalcube[up][6],temp[2] = temp[2],finalcube[up][6]
            finalcube[up][3],temp[1] = temp[1],finalcube[up][3]
            finalcube[up][0],temp[2] = temp[2],finalcube[up][0]
            temp[0] = finalcube[eg2[0]][3]
            temp[1] = finalcube[eg2[0]][4]
            temp[2] = finalcube[eg2[0]][5]
            for i in range(0, 4):
                if i == 1 or i == 2:
                    finalcube[eg2[i+1]][3],temp[0] = temp[0],finalcube[eg2[i+1]][3]
                    finalcube[eg2[i+1]][4],temp[1] = temp[1],finalcube[eg2[i+1]][4]
                    finalcube[eg2[i+1]][5],temp[2] = temp[2],finalcube[eg2[i+1]][5]
                elif i == 0:
                    finalcube[eg2[i+1]][3],temp[0] = temp[0],finalcube[eg2[i+1]][3]
                    finalcube[eg2[i+1]][4],temp[1] = temp[1],finalcube[eg2[i+1]][4]
                    finalcube[eg2[i+1]][5],temp[2] = temp[2],finalcube[eg2[i+1]][5]
                elif i == 3:
                    finalcube[eg2[0]][5],temp[0] = temp[0],finalcube[eg2[0]][5]
                    finalcube[eg2[0]][4],temp[1] = temp[1],finalcube[eg2[0]][4]
                    finalcube[eg2[0]][3],temp[2] = temp[2],finalcube[eg2[0]][3]
            temp[0] = finalcube[eg2[0]][6]
            temp[1] = finalcube[eg2[0]][7]
            temp[2] = finalcube[eg2[0]][8]
            for i in range(0, 4):
                if i != 3:
                    finalcube[eg2[i+1]][6],temp[0] = temp[0],finalcube[eg2[i+1]][6]
                    finalcube[eg2[i+1]][7],temp[1] = temp[1],finalcube[eg2[i+1]][7]
                    finalcube[eg2[i+1]][8],temp[2] = temp[2],finalcube[eg2[i+1]][8]
                elif i == 3:
                    finalcube[eg2[0]][6],temp[0] = temp[0],finalcube[eg2[0]][6]
                    finalcube[eg2[0]][7],temp[1] = temp[1],finalcube[eg2[0]][7]
                    finalcube[eg2[0]][8],temp[2] = temp[2],finalcube[eg2[0]][8]
            temp[0] = finalcube[down][0]
            temp[1] = finalcube[down][3]
            temp[2] = finalcube[down][6]
            finalcube[down][6],temp[0] = temp[0],finalcube[down][6]
            finalcube[down][7],temp[1] = temp[1],finalcube[down][7]
            finalcube[down][8],temp[2] = temp[2],finalcube[down][8]
            finalcube[down][5],temp[1] = temp[1],finalcube[down][5]
            finalcube[down][2],temp[2] = temp[2],finalcube[down][2]
            finalcube[down][1],temp[1] = temp[1],finalcube[down][1]
            finalcube[down][0],temp[2] = temp[2],finalcube[down][0]
            finalcube[down][3],temp[1] = temp[1],finalcube[down][3]
            finalcube[down][6],temp[2] = temp[2],finalcube[down][6]
        elif movetype == '1i':
            temp[0] = finalcube[eg2[3]][0]
            temp[1] = finalcube[eg2[3]][1]
            temp[2] = finalcube[eg2[3]][2]
            for i in range(3, -1, -1):
                if i != 0:
                    finalcube[eg2[i-1]][0],temp[0] = temp[0],finalcube[eg2[i-1]][0]
                    finalcube[eg2[i-1]][1],temp[1] = temp[1],finalcube[eg2[i-1]][1]
                    finalcube[eg2[i-1]][2],temp[2] = temp[2],finalcube[eg2[i-1]][2]
                elif i == 0:
                    finalcube[eg2[3]][0],temp[0] = temp[0],finalcube[eg2[3]][0]
                    finalcube[eg2[3]][1],temp[1] = temp[1],finalcube[eg2[3]][1]
                    finalcube[eg2[3]][2],temp[2] = temp[2],finalcube[eg2[3]][2]
            temp[0] = finalcube[up][0]
            temp[1] = finalcube[up][3]
            temp[2] = finalcube[up][6]
            finalcube[up][6],temp[0] = temp[0],finalcube[up][6]
            finalcube[up][7],temp[1] = temp[1],finalcube[up][7]
            finalcube[up][8],temp[2] = temp[2],finalcube[up][8]
            finalcube[up][5],temp[1] = temp[1],finalcube[up][5]
            finalcube[up][2],temp[2] = temp[2],finalcube[up][2]
            finalcube[up][1],temp[1] = temp[1],finalcube[up][1]
            finalcube[up][0],temp[2] = temp[2],finalcube[up][0]
            finalcube[up][3],temp[1] = temp[1],finalcube[up][3]
            finalcube[up][6],temp[2] = temp[2],finalcube[up][6]
            temp[0] = finalcube[eg2[3]][3]
            temp[1] = finalcube[eg2[3]][4]
            temp[2] = finalcube[eg2[3]][5]
            for i in range(3, -1, -1):
                if i != 0:
                    finalcube[eg2[i-1]][3],temp[0] = temp[0],finalcube[eg2[i-1]][3]
                    finalcube[eg2[i-1]][4],temp[1] = temp[1],finalcube[eg2[i-1]][4]
                    finalcube[eg2[i-1]][5],temp[2] = temp[2],finalcube[eg2[i-1]][5]
                elif i == 0:
                    finalcube[eg2[3]][3],temp[0] = temp[0],finalcube[eg2[3]][3]
                    finalcube[eg2[3]][4],temp[1] = temp[1],finalcube[eg2[3]][4]
                    finalcube[eg2[3]][5],temp[2] = temp[2],finalcube[eg2[3]][5]
            temp[0] = finalcube[eg2[3]][6]
            temp[1] = finalcube[eg2[3]][7]
            temp[2] = finalcube[eg2[3]][8]
            for i in range(3, -1, -1):
                if i != 0:
                    finalcube[eg2[i-1]][6],temp[0] = temp[0],finalcube[eg2[i-1]][6]
                    finalcube[eg2[i-1]][7],temp[1] = temp[1],finalcube[eg2[i-1]][7]
                    finalcube[eg2[i-1]][8],temp[2] = temp[2],finalcube[eg2[i-1]][8]
                elif i == 0:
                    finalcube[eg2[3]][6],temp[0] = temp[0],finalcube[eg2[3]][6]
                    finalcube[eg2[3]][7],temp[1] = temp[1],finalcube[eg2[3]][7]
                    finalcube[eg2[3]][8],temp[2] = temp[2],finalcube[eg2[3]][8]
            temp[0] = finalcube[down][6]
            temp[1] = finalcube[down][3]
            temp[2] = finalcube[down][0]
            finalcube[down][0],temp[0] = temp[0],finalcube[down][0]
            finalcube[down][1],temp[1] = temp[1],finalcube[down][1]
            finalcube[down][2],temp[2] = temp[2],finalcube[down][2]
            finalcube[down][5],temp[1] = temp[1],finalcube[down][5]
            finalcube[down][8],temp[2] = temp[2],finalcube[down][8]
            finalcube[down][7],temp[1] = temp[1],finalcube[down][7]
            finalcube[down][6],temp[2] = temp[2],finalcube[down][6]
            finalcube[down][3],temp[1] = temp[1],finalcube[down][3]
            finalcube[down][0],temp[2] = temp[2],finalcube[down][0]
        elif movetype == '2i':
            temp[0] = finalcube[eg2[3]][0]
            temp[1] = finalcube[eg2[3]][1]
            temp[2] = finalcube[eg2[3]][2]
            for i in range(3, -1, -1):
                if i != 0:
                    finalcube[eg2[i-1]][0],temp[0] = temp[0],finalcube[eg2[i-1]][0]
                    finalcube[eg2[i-1]][1],temp[1] = temp[1],finalcube[eg2[i-1]][1]
                    finalcube[eg2[i-1]][2],temp[2] = temp[2],finalcube[eg2[i-1]][2]
                elif i == 0:
                    finalcube[eg2[3]][0],temp[0] = temp[0],finalcube[eg2[3]][0]
                    finalcube[eg2[3]][1],temp[1] = temp[1],finalcube[eg2[3]][1]
                    finalcube[eg2[3]][2],temp[2] = temp[2],finalcube[eg2[3]][2]
            temp[0] = finalcube[up][0]
            temp[1] = finalcube[up][3]
            temp[2] = finalcube[up][6]
            finalcube[up][6],temp[0] = temp[0],finalcube[up][6]
            finalcube[up][7],temp[1] = temp[1],finalcube[up][7]
            finalcube[up][8],temp[2] = temp[2],finalcube[up][8]
            finalcube[up][5],temp[1] = temp[1],finalcube[up][5]
            finalcube[up][2],temp[2] = temp[2],finalcube[up][2]
            finalcube[up][1],temp[1] = temp[1],finalcube[up][1]
            finalcube[up][0],temp[2] = temp[2],finalcube[up][0]
            finalcube[up][3],temp[1] = temp[1],finalcube[up][3]
            finalcube[up][6],temp[2] = temp[2],finalcube[up][6]
            temp[0] = finalcube[eg2[3]][3]
            temp[1] = finalcube[eg2[3]][4]
            temp[2] = finalcube[eg2[3]][5]
            for i in range(3, -1, -1):
                if i != 0:
                    finalcube[eg2[i-1]][3],temp[0] = temp[0],finalcube[eg2[i-1]][3]
                    finalcube[eg2[i-1]][4],temp[1] = temp[1],finalcube[eg2[i-1]][4]
                    finalcube[eg2[i-1]][5],temp[2] = temp[2],finalcube[eg2[i-1]][5]
                elif i == 0:
                    finalcube[eg2[3]][3],temp[0] = temp[0],finalcube[eg2[3]][3]
                    finalcube[eg2[3]][4],temp[1] = temp[1],finalcube[eg2[3]][4]
                    finalcube[eg2[3]][5],temp[2] = temp[2],finalcube[eg2[3]][5]
            temp[0] = finalcube[eg2[3]][6]
            temp[1] = finalcube[eg2[3]][7]
            temp[2] = finalcube[eg2[3]][8]
            for i in range(3, -1, -1):
                if i != 0:
                    finalcube[eg2[i-1]][6],temp[0] = temp[0],finalcube[eg2[i-1]][6]
                    finalcube[eg2[i-1]][7],temp[1] = temp[1],finalcube[eg2[i-1]][7]
                    finalcube[eg2[i-1]][8],temp[2] = temp[2],finalcube[eg2[i-1]][8]
                elif i == 0:
                    finalcube[eg2[3]][6],temp[0] = temp[0],finalcube[eg2[3]][6]
                    finalcube[eg2[3]][7],temp[1] = temp[1],finalcube[eg2[3]][7]
                    finalcube[eg2[3]][8],temp[2] = temp[2],finalcube[eg2[3]][8]
            temp[0] = finalcube[down][6]
            temp[1] = finalcube[down][3]
            temp[2] = finalcube[down][0]
            finalcube[down][0],temp[0] = temp[0],finalcube[down][0]
            finalcube[down][1],temp[1] = temp[1],finalcube[down][1]
            finalcube[down][2],temp[2] = temp[2],finalcube[down][2]
            finalcube[down][5],temp[1] = temp[1],finalcube[down][5]
            finalcube[down][8],temp[2] = temp[2],finalcube[down][8]
            finalcube[down][7],temp[1] = temp[1],finalcube[down][7]
            finalcube[down][6],temp[2] = temp[2],finalcube[down][6]
            finalcube[down][3],temp[1] = temp[1],finalcube[down][3]
            finalcube[down][0],temp[2] = temp[2],finalcube[down][0]
            temp[0] = finalcube[eg2[3]][0]
            temp[1] = finalcube[eg2[3]][1]
            temp[2] = finalcube[eg2[3]][2]
            for i in range(3, -1, -1):
                if i != 0:
                    finalcube[eg2[i-1]][0],temp[0] = temp[0],finalcube[eg2[i-1]][0]
                    finalcube[eg2[i-1]][1],temp[1] = temp[1],finalcube[eg2[i-1]][1]
                    finalcube[eg2[i-1]][2],temp[2] = temp[2],finalcube[eg2[i-1]][2]
                elif i == 0:
                    finalcube[eg2[3]][0],temp[0] = temp[0],finalcube[eg2[3]][0]
                    finalcube[eg2[3]][1],temp[1] = temp[1],finalcube[eg2[3]][1]
                    finalcube[eg2[3]][2],temp[2] = temp[2],finalcube[eg2[3]][2]
            temp[0] = finalcube[up][0]
            temp[1] = finalcube[up][3]
            temp[2] = finalcube[up][6]
            finalcube[up][6],temp[0] = temp[0],finalcube[up][6]
            finalcube[up][7],temp[1] = temp[1],finalcube[up][7]
            finalcube[up][8],temp[2] = temp[2],finalcube[up][8]
            finalcube[up][5],temp[1] = temp[1],finalcube[up][5]
            finalcube[up][2],temp[2] = temp[2],finalcube[up][2]
            finalcube[up][1],temp[1] = temp[1],finalcube[up][1]
            finalcube[up][0],temp[2] = temp[2],finalcube[up][0]
            finalcube[up][3],temp[1] = temp[1],finalcube[up][3]
            finalcube[up][6],temp[2] = temp[2],finalcube[up][6]
            temp[0] = finalcube[eg2[3]][3]
            temp[1] = finalcube[eg2[3]][4]
            temp[2] = finalcube[eg2[3]][5]
            for i in range(3, -1, -1):
                if i != 0:
                    finalcube[eg2[i-1]][3],temp[0] = temp[0],finalcube[eg2[i-1]][3]
                    finalcube[eg2[i-1]][4],temp[1] = temp[1],finalcube[eg2[i-1]][4]
                    finalcube[eg2[i-1]][5],temp[2] = temp[2],finalcube[eg2[i-1]][5]
                elif i == 0:
                    finalcube[eg2[3]][3],temp[0] = temp[0],finalcube[eg2[3]][3]
                    finalcube[eg2[3]][4],temp[1] = temp[1],finalcube[eg2[3]][4]
                    finalcube[eg2[3]][5],temp[2] = temp[2],finalcube[eg2[3]][5]
            temp[0] = finalcube[eg2[3]][6]
            temp[1] = finalcube[eg2[3]][7]
            temp[2] = finalcube[eg2[3]][8]
            for i in range(3, -1, -1):
                if i != 0:
                    finalcube[eg2[i-1]][6],temp[0] = temp[0],finalcube[eg2[i-1]][6]
                    finalcube[eg2[i-1]][7],temp[1] = temp[1],finalcube[eg2[i-1]][7]
                    finalcube[eg2[i-1]][8],temp[2] = temp[2],finalcube[eg2[i-1]][8]
                elif i == 0:
                    finalcube[eg2[3]][6],temp[0] = temp[0],finalcube[eg2[3]][6]
                    finalcube[eg2[3]][7],temp[1] = temp[1],finalcube[eg2[3]][7]
                    finalcube[eg2[3]][8],temp[2] = temp[2],finalcube[eg2[3]][8]
            temp[0] = finalcube[down][6]
            temp[1] = finalcube[down][3]
            temp[2] = finalcube[down][0]
            finalcube[down][0],temp[0] = temp[0],finalcube[down][0]
            finalcube[down][1],temp[1] = temp[1],finalcube[down][1]
            finalcube[down][2],temp[2] = temp[2],finalcube[down][2]
            finalcube[down][5],temp[1] = temp[1],finalcube[down][5]
            finalcube[down][8],temp[2] = temp[2],finalcube[down][8]
            finalcube[down][7],temp[1] = temp[1],finalcube[down][7]
            finalcube[down][6],temp[2] = temp[2],finalcube[down][6]
            finalcube[down][3],temp[1] = temp[1],finalcube[down][3]
            finalcube[down][0],temp[2] = temp[2],finalcube[down][0]
    def Z(movetype):
        temp = ['a','b','c']
        if movetype == 1:
            temp[0] = finalcube[eg1[3]][0]
            temp[1] = finalcube[eg1[3]][3]
            temp[2] = finalcube[eg1[3]][6]
            for i in range(3, -1, -1):
                if i == 3:
                    finalcube[eg1[i-1]][2],temp[0] = temp[0],finalcube[eg1[i-1]][2]
                    finalcube[eg1[i-1]][1],temp[1] = temp[1],finalcube[eg1[i-1]][1]
                    finalcube[eg1[i-1]][0],temp[2] = temp[2],finalcube[eg1[i-1]][0]
                elif i == 2:
                    finalcube[eg1[i-1]][8],temp[0] = temp[0],finalcube[eg1[i-1]][8]
                    finalcube[eg1[i-1]][5],temp[1] = temp[1],finalcube[eg1[i-1]][5]
                    finalcube[eg1[i-1]][2],temp[2] = temp[2],finalcube[eg1[i-1]][2]
                elif i == 1:
                    finalcube[eg1[i-1]][6],temp[0] = temp[0],finalcube[eg1[i-1]][6]
                    finalcube[eg1[i-1]][7],temp[1] = temp[1],finalcube[eg1[i-1]][7]
                    finalcube[eg1[i-1]][8],temp[2] = temp[2],finalcube[eg1[i-1]][8]
                elif i == 0:
                    finalcube[eg1[3]][0],temp[0] = temp[0],finalcube[eg1[3]][0]
                    finalcube[eg1[3]][3],temp[1] = temp[1],finalcube[eg1[3]][3]
                    finalcube[eg1[3]][6],temp[2] = temp[2],finalcube[eg1[3]][6]
            temp[0] = finalcube[top][0]
            temp[1] = finalcube[top][1]
            temp[2] = finalcube[top][2]
            finalcube[top][2],temp[0] = temp[0],finalcube[top][2]
            finalcube[top][5],temp[1] = temp[1],finalcube[top][5]
            finalcube[top][8],temp[2] = temp[2],finalcube[top][8]
            finalcube[top][7],temp[1] = temp[1],finalcube[top][7]
            finalcube[top][6],temp[2] = temp[2],finalcube[top][6]
            finalcube[top][3],temp[1] = temp[1],finalcube[top][3]
            finalcube[top][0],temp[2] = temp[2],finalcube[top][0]
            finalcube[top][1],temp[1] = temp[1],finalcube[top][1]
            finalcube[top][2],temp[2] = temp[2],finalcube[top][2]
            temp[0] = finalcube[eg1[3]][2]
            temp[1] = finalcube[eg1[3]][5]
            temp[2] = finalcube[eg1[3]][8]
            for i in range(3, -1, -1):
                if i == 3:
                    finalcube[eg1[i-1]][8],temp[0] = temp[0],finalcube[eg1[i-1]][8]
                    finalcube[eg1[i-1]][7],temp[1] = temp[1],finalcube[eg1[i-1]][7]
                    finalcube[eg1[i-1]][6],temp[2] = temp[2],finalcube[eg1[i-1]][6]
                elif i == 2:
                    finalcube[eg1[i-1]][6],temp[0] = temp[0],finalcube[eg1[i-1]][6]
                    finalcube[eg1[i-1]][3],temp[1] = temp[1],finalcube[eg1[i-1]][3]
                    finalcube[eg1[i-1]][0],temp[2] = temp[2],finalcube[eg1[i-1]][0]
                elif i == 1:
                    finalcube[eg1[i-1]][0],temp[0] = temp[0],finalcube[eg1[i-1]][0]
                    finalcube[eg1[i-1]][1],temp[1] = temp[1],finalcube[eg1[i-1]][1]
                    finalcube[eg1[i-1]][2],temp[2] = temp[2],finalcube[eg1[i-1]][2]
                elif i == 0:
                    finalcube[eg1[3]][2],temp[0] = temp[0],finalcube[eg1[3]][2]
                    finalcube[eg1[3]][5],temp[1] = temp[1],finalcube[eg1[3]][5]
                    finalcube[eg1[3]][8],temp[2] = temp[2],finalcube[eg1[3]][8]
            temp[0] = finalcube[bottom][2]
            temp[1] = finalcube[bottom][1]
            temp[2] = finalcube[bottom][0]
            finalcube[bottom][0],temp[0] = temp[0],finalcube[bottom][0]
            finalcube[bottom][3],temp[1] = temp[1],finalcube[bottom][3]
            finalcube[bottom][6],temp[2] = temp[2],finalcube[bottom][6]
            finalcube[bottom][7],temp[1] = temp[1],finalcube[bottom][7]
            finalcube[bottom][8],temp[2] = temp[2],finalcube[bottom][8]
            finalcube[bottom][5],temp[1] = temp[1],finalcube[bottom][5]
            finalcube[bottom][2],temp[2] = temp[2],finalcube[bottom][2]
            finalcube[bottom][1],temp[1] = temp[1],finalcube[bottom][1]
            finalcube[bottom][0],temp[2] = temp[2],finalcube[bottom][0]
            temp[0] = finalcube[eg1[3]][1]
            temp[1] = finalcube[eg1[3]][4]
            temp[2] = finalcube[eg1[3]][7]
            for i in range(3, -1, -1):
                if i == 3:
                    finalcube[eg1[i-1]][5],temp[0] = temp[0],finalcube[eg1[i-1]][5]
                    finalcube[eg1[i-1]][4],temp[1] = temp[1],finalcube[eg1[i-1]][4]
                    finalcube[eg1[i-1]][3],temp[2] = temp[2],finalcube[eg1[i-1]][3]
                elif i == 2:
                    finalcube[eg1[i-1]][7],temp[0] = temp[0],finalcube[eg1[i-1]][7]
                    finalcube[eg1[i-1]][4],temp[1] = temp[1],finalcube[eg1[i-1]][4]
                    finalcube[eg1[i-1]][1],temp[2] = temp[2],finalcube[eg1[i-1]][1]
                elif i == 1:
                    finalcube[eg1[i-1]][3],temp[0] = temp[0],finalcube[eg1[i-1]][3]
                    finalcube[eg1[i-1]][4],temp[1] = temp[1],finalcube[eg1[i-1]][4]
                    finalcube[eg1[i-1]][5],temp[2] = temp[2],finalcube[eg1[i-1]][5]
                elif i == 0:
                    finalcube[eg1[3]][1],temp[0] = temp[0],finalcube[eg1[3]][1]
                    finalcube[eg1[3]][4],temp[1] = temp[1],finalcube[eg1[3]][4]
                    finalcube[eg1[3]][7],temp[2] = temp[2],finalcube[eg1[3]][7]
        elif movetype == 2:
            temp[0] = finalcube[eg1[3]][0]
            temp[1] = finalcube[eg1[3]][3]
            temp[2] = finalcube[eg1[3]][6]
            for i in range(3, -1, -1):
                if i == 3:
                    finalcube[eg1[i-1]][2],temp[0] = temp[0],finalcube[eg1[i-1]][2]
                    finalcube[eg1[i-1]][1],temp[1] = temp[1],finalcube[eg1[i-1]][1]
                    finalcube[eg1[i-1]][0],temp[2] = temp[2],finalcube[eg1[i-1]][0]
                elif i == 2:
                    finalcube[eg1[i-1]][8],temp[0] = temp[0],finalcube[eg1[i-1]][8]
                    finalcube[eg1[i-1]][5],temp[1] = temp[1],finalcube[eg1[i-1]][5]
                    finalcube[eg1[i-1]][2],temp[2] = temp[2],finalcube[eg1[i-1]][2]
                elif i == 1:
                    finalcube[eg1[i-1]][6],temp[0] = temp[0],finalcube[eg1[i-1]][6]
                    finalcube[eg1[i-1]][7],temp[1] = temp[1],finalcube[eg1[i-1]][7]
                    finalcube[eg1[i-1]][8],temp[2] = temp[2],finalcube[eg1[i-1]][8]
                elif i == 0:
                    finalcube[eg1[3]][0],temp[0] = temp[0],finalcube[eg1[3]][0]
                    finalcube[eg1[3]][3],temp[1] = temp[1],finalcube[eg1[3]][3]
                    finalcube[eg1[3]][6],temp[2] = temp[2],finalcube[eg1[3]][6]
            temp[0] = finalcube[top][0]
            temp[1] = finalcube[top][1]
            temp[2] = finalcube[top][2]
            finalcube[top][2],temp[0] = temp[0],finalcube[top][2]
            finalcube[top][5],temp[1] = temp[1],finalcube[top][5]
            finalcube[top][8],temp[2] = temp[2],finalcube[top][8]
            finalcube[top][7],temp[1] = temp[1],finalcube[top][7]
            finalcube[top][6],temp[2] = temp[2],finalcube[top][6]
            finalcube[top][3],temp[1] = temp[1],finalcube[top][3]
            finalcube[top][0],temp[2] = temp[2],finalcube[top][0]
            finalcube[top][1],temp[1] = temp[1],finalcube[top][1]
            finalcube[top][2],temp[2] = temp[2],finalcube[top][2]
            temp[0] = finalcube[eg1[3]][2]
            temp[1] = finalcube[eg1[3]][5]
            temp[2] = finalcube[eg1[3]][8]
            for i in range(3, -1, -1):
                if i == 3:
                    finalcube[eg1[i-1]][8],temp[0] = temp[0],finalcube[eg1[i-1]][8]
                    finalcube[eg1[i-1]][7],temp[1] = temp[1],finalcube[eg1[i-1]][7]
                    finalcube[eg1[i-1]][6],temp[2] = temp[2],finalcube[eg1[i-1]][6]
                elif i == 2:
                    finalcube[eg1[i-1]][6],temp[0] = temp[0],finalcube[eg1[i-1]][6]
                    finalcube[eg1[i-1]][3],temp[1] = temp[1],finalcube[eg1[i-1]][3]
                    finalcube[eg1[i-1]][0],temp[2] = temp[2],finalcube[eg1[i-1]][0]
                elif i == 1:
                    finalcube[eg1[i-1]][0],temp[0] = temp[0],finalcube[eg1[i-1]][0]
                    finalcube[eg1[i-1]][1],temp[1] = temp[1],finalcube[eg1[i-1]][1]
                    finalcube[eg1[i-1]][2],temp[2] = temp[2],finalcube[eg1[i-1]][2]
                elif i == 0:
                    finalcube[eg1[3]][2],temp[0] = temp[0],finalcube[eg1[3]][2]
                    finalcube[eg1[3]][5],temp[1] = temp[1],finalcube[eg1[3]][5]
                    finalcube[eg1[3]][8],temp[2] = temp[2],finalcube[eg1[3]][8]
            temp[0] = finalcube[bottom][2]
            temp[1] = finalcube[bottom][1]
            temp[2] = finalcube[bottom][0]
            finalcube[bottom][0],temp[0] = temp[0],finalcube[bottom][0]
            finalcube[bottom][3],temp[1] = temp[1],finalcube[bottom][3]
            finalcube[bottom][6],temp[2] = temp[2],finalcube[bottom][6]
            finalcube[bottom][7],temp[1] = temp[1],finalcube[bottom][7]
            finalcube[bottom][8],temp[2] = temp[2],finalcube[bottom][8]
            finalcube[bottom][5],temp[1] = temp[1],finalcube[bottom][5]
            finalcube[bottom][2],temp[2] = temp[2],finalcube[bottom][2]
            finalcube[bottom][1],temp[1] = temp[1],finalcube[bottom][1]
            finalcube[bottom][0],temp[2] = temp[2],finalcube[bottom][0]
            temp[0] = finalcube[eg1[3]][1]
            temp[1] = finalcube[eg1[3]][4]
            temp[2] = finalcube[eg1[3]][7]
            for i in range(3, -1, -1):
                if i == 3:
                    finalcube[eg1[i-1]][5],temp[0] = temp[0],finalcube[eg1[i-1]][5]
                    finalcube[eg1[i-1]][4],temp[1] = temp[1],finalcube[eg1[i-1]][4]
                    finalcube[eg1[i-1]][3],temp[2] = temp[2],finalcube[eg1[i-1]][3]
                elif i == 2:
                    finalcube[eg1[i-1]][7],temp[0] = temp[0],finalcube[eg1[i-1]][7]
                    finalcube[eg1[i-1]][4],temp[1] = temp[1],finalcube[eg1[i-1]][4]
                    finalcube[eg1[i-1]][1],temp[2] = temp[2],finalcube[eg1[i-1]][1]
                elif i == 1:
                    finalcube[eg1[i-1]][3],temp[0] = temp[0],finalcube[eg1[i-1]][3]
                    finalcube[eg1[i-1]][4],temp[1] = temp[1],finalcube[eg1[i-1]][4]
                    finalcube[eg1[i-1]][5],temp[2] = temp[2],finalcube[eg1[i-1]][5]
                elif i == 0:
                    finalcube[eg1[3]][1],temp[0] = temp[0],finalcube[eg1[3]][1]
                    finalcube[eg1[3]][4],temp[1] = temp[1],finalcube[eg1[3]][4]
                    finalcube[eg1[3]][7],temp[2] = temp[2],finalcube[eg1[3]][7]
            temp[0] = finalcube[eg1[3]][0]
            temp[1] = finalcube[eg1[3]][3]
            temp[2] = finalcube[eg1[3]][6]
            for i in range(3, -1, -1):
                if i == 3:
                    finalcube[eg1[i-1]][2],temp[0] = temp[0],finalcube[eg1[i-1]][2]
                    finalcube[eg1[i-1]][1],temp[1] = temp[1],finalcube[eg1[i-1]][1]
                    finalcube[eg1[i-1]][0],temp[2] = temp[2],finalcube[eg1[i-1]][0]
                elif i == 2:
                    finalcube[eg1[i-1]][8],temp[0] = temp[0],finalcube[eg1[i-1]][8]
                    finalcube[eg1[i-1]][5],temp[1] = temp[1],finalcube[eg1[i-1]][5]
                    finalcube[eg1[i-1]][2],temp[2] = temp[2],finalcube[eg1[i-1]][2]
                elif i == 1:
                    finalcube[eg1[i-1]][6],temp[0] = temp[0],finalcube[eg1[i-1]][6]
                    finalcube[eg1[i-1]][7],temp[1] = temp[1],finalcube[eg1[i-1]][7]
                    finalcube[eg1[i-1]][8],temp[2] = temp[2],finalcube[eg1[i-1]][8]
                elif i == 0:
                    finalcube[eg1[3]][0],temp[0] = temp[0],finalcube[eg1[3]][0]
                    finalcube[eg1[3]][3],temp[1] = temp[1],finalcube[eg1[3]][3]
                    finalcube[eg1[3]][6],temp[2] = temp[2],finalcube[eg1[3]][6]
            temp[0] = finalcube[top][0]
            temp[1] = finalcube[top][1]
            temp[2] = finalcube[top][2]
            finalcube[top][2],temp[0] = temp[0],finalcube[top][2]
            finalcube[top][5],temp[1] = temp[1],finalcube[top][5]
            finalcube[top][8],temp[2] = temp[2],finalcube[top][8]
            finalcube[top][7],temp[1] = temp[1],finalcube[top][7]
            finalcube[top][6],temp[2] = temp[2],finalcube[top][6]
            finalcube[top][3],temp[1] = temp[1],finalcube[top][3]
            finalcube[top][0],temp[2] = temp[2],finalcube[top][0]
            finalcube[top][1],temp[1] = temp[1],finalcube[top][1]
            finalcube[top][2],temp[2] = temp[2],finalcube[top][2]
            temp[0] = finalcube[eg1[3]][2]
            temp[1] = finalcube[eg1[3]][5]
            temp[2] = finalcube[eg1[3]][8]
            for i in range(3, -1, -1):
                if i == 3:
                    finalcube[eg1[i-1]][8],temp[0] = temp[0],finalcube[eg1[i-1]][8]
                    finalcube[eg1[i-1]][7],temp[1] = temp[1],finalcube[eg1[i-1]][7]
                    finalcube[eg1[i-1]][6],temp[2] = temp[2],finalcube[eg1[i-1]][6]
                elif i == 2:
                    finalcube[eg1[i-1]][6],temp[0] = temp[0],finalcube[eg1[i-1]][6]
                    finalcube[eg1[i-1]][3],temp[1] = temp[1],finalcube[eg1[i-1]][3]
                    finalcube[eg1[i-1]][0],temp[2] = temp[2],finalcube[eg1[i-1]][0]
                elif i == 1:
                    finalcube[eg1[i-1]][0],temp[0] = temp[0],finalcube[eg1[i-1]][0]
                    finalcube[eg1[i-1]][1],temp[1] = temp[1],finalcube[eg1[i-1]][1]
                    finalcube[eg1[i-1]][2],temp[2] = temp[2],finalcube[eg1[i-1]][2]
                elif i == 0:
                    finalcube[eg1[3]][2],temp[0] = temp[0],finalcube[eg1[3]][2]
                    finalcube[eg1[3]][5],temp[1] = temp[1],finalcube[eg1[3]][5]
                    finalcube[eg1[3]][8],temp[2] = temp[2],finalcube[eg1[3]][8]
            temp[0] = finalcube[bottom][2]
            temp[1] = finalcube[bottom][1]
            temp[2] = finalcube[bottom][0]
            finalcube[bottom][0],temp[0] = temp[0],finalcube[bottom][0]
            finalcube[bottom][3],temp[1] = temp[1],finalcube[bottom][3]
            finalcube[bottom][6],temp[2] = temp[2],finalcube[bottom][6]
            finalcube[bottom][7],temp[1] = temp[1],finalcube[bottom][7]
            finalcube[bottom][8],temp[2] = temp[2],finalcube[bottom][8]
            finalcube[bottom][5],temp[1] = temp[1],finalcube[bottom][5]
            finalcube[bottom][2],temp[2] = temp[2],finalcube[bottom][2]
            finalcube[bottom][1],temp[1] = temp[1],finalcube[bottom][1]
            finalcube[bottom][0],temp[2] = temp[2],finalcube[bottom][0]
            temp[0] = finalcube[eg1[3]][1]
            temp[1] = finalcube[eg1[3]][4]
            temp[2] = finalcube[eg1[3]][7]
            for i in range(3, -1, -1):
                if i == 3:
                    finalcube[eg1[i-1]][5],temp[0] = temp[0],finalcube[eg1[i-1]][5]
                    finalcube[eg1[i-1]][4],temp[1] = temp[1],finalcube[eg1[i-1]][4]
                    finalcube[eg1[i-1]][3],temp[2] = temp[2],finalcube[eg1[i-1]][3]
                elif i == 2:
                    finalcube[eg1[i-1]][7],temp[0] = temp[0],finalcube[eg1[i-1]][7]
                    finalcube[eg1[i-1]][4],temp[1] = temp[1],finalcube[eg1[i-1]][4]
                    finalcube[eg1[i-1]][1],temp[2] = temp[2],finalcube[eg1[i-1]][1]
                elif i == 1:
                    finalcube[eg1[i-1]][3],temp[0] = temp[0],finalcube[eg1[i-1]][3]
                    finalcube[eg1[i-1]][4],temp[1] = temp[1],finalcube[eg1[i-1]][4]
                    finalcube[eg1[i-1]][5],temp[2] = temp[2],finalcube[eg1[i-1]][5]
                elif i == 0:
                    finalcube[eg1[3]][1],temp[0] = temp[0],finalcube[eg1[3]][1]
                    finalcube[eg1[3]][4],temp[1] = temp[1],finalcube[eg1[3]][4]
                    finalcube[eg1[3]][7],temp[2] = temp[2],finalcube[eg1[3]][7]
        elif movetype == '1i':
            temp[0] = finalcube[eg1[0]][6]
            temp[1] = finalcube[eg1[0]][7]
            temp[2] = finalcube[eg1[0]][8]
            for i in range(0, 4):
                if i == 0:
                    finalcube[eg1[i+1]][8],temp[0] = temp[0],finalcube[eg1[i+1]][8]
                    finalcube[eg1[i+1]][5],temp[1] = temp[1],finalcube[eg1[i+1]][5]
                    finalcube[eg1[i+1]][2],temp[2] = temp[2],finalcube[eg1[i+1]][2]
                elif i == 1:
                    finalcube[eg1[i+1]][2],temp[0] = temp[0],finalcube[eg1[i+1]][2]
                    finalcube[eg1[i+1]][1],temp[1] = temp[1],finalcube[eg1[i+1]][1]
                    finalcube[eg1[i+1]][0],temp[2] = temp[2],finalcube[eg1[i+1]][0]
                elif i == 2:
                    finalcube[eg1[i+1]][0],temp[0] = temp[0],finalcube[eg1[i+1]][0]
                    finalcube[eg1[i+1]][3],temp[1] = temp[1],finalcube[eg1[i+1]][3]
                    finalcube[eg1[i+1]][6],temp[2] = temp[2],finalcube[eg1[i+1]][6]
                elif i == 3:
                    finalcube[eg1[0]][6],temp[0] = temp[0],finalcube[eg1[0]][6]
                    finalcube[eg1[0]][7],temp[1] = temp[1],finalcube[eg1[0]][7]
                    finalcube[eg1[0]][8],temp[2] = temp[2],finalcube[eg1[0]][8]
            temp[0] = finalcube[top][2]
            temp[1] = finalcube[top][1]
            temp[2] = finalcube[top][0]
            finalcube[top][0],temp[0] = temp[0],finalcube[top][0]
            finalcube[top][3],temp[1] = temp[1],finalcube[top][3]
            finalcube[top][6],temp[2] = temp[2],finalcube[top][6]
            finalcube[top][7],temp[1] = temp[1],finalcube[top][7]
            finalcube[top][8],temp[2] = temp[2],finalcube[top][8]
            finalcube[top][5],temp[1] = temp[1],finalcube[top][5]
            finalcube[top][2],temp[2] = temp[2],finalcube[top][2]
            finalcube[top][1],temp[1] = temp[1],finalcube[top][1]
            finalcube[top][0],temp[2] = temp[2],finalcube[top][0]
            temp[0] = finalcube[eg1[0]][5]
            temp[1] = finalcube[eg1[0]][4]
            temp[2] = finalcube[eg1[0]][3]
            for i in range(0, 4):
                if i == 0:
                    finalcube[eg1[i+1]][1],temp[0] = temp[0],finalcube[eg1[i+1]][1]
                    finalcube[eg1[i+1]][4],temp[1] = temp[1],finalcube[eg1[i+1]][4]
                    finalcube[eg1[i+1]][7],temp[2] = temp[2],finalcube[eg1[i+1]][7]
                elif i == 1:
                    finalcube[eg1[i+1]][3],temp[0] = temp[0],finalcube[eg1[i+1]][3]
                    finalcube[eg1[i+1]][4],temp[1] = temp[1],finalcube[eg1[i+1]][4]
                    finalcube[eg1[i+1]][5],temp[2] = temp[2],finalcube[eg1[i+1]][5]
                elif i == 2:
                    finalcube[eg1[i+1]][7],temp[0] = temp[0],finalcube[eg1[i+1]][7]
                    finalcube[eg1[i+1]][4],temp[1] = temp[1],finalcube[eg1[i+1]][4]
                    finalcube[eg1[i+1]][1],temp[2] = temp[2],finalcube[eg1[i+1]][1]
                elif i == 3:
                    finalcube[eg1[0]][5],temp[0] = temp[0],finalcube[eg1[0]][5]
                    finalcube[eg1[0]][4],temp[1] = temp[1],finalcube[eg1[0]][4]
                    finalcube[eg1[0]][3],temp[2] = temp[2],finalcube[eg1[0]][3]
            temp[0] = finalcube[eg1[0]][2]
            temp[1] = finalcube[eg1[0]][1]
            temp[2] = finalcube[eg1[0]][0]
            for i in range(0, 4):
                if i == 0:
                    finalcube[eg1[i+1]][0],temp[0] = temp[0],finalcube[eg1[i+1]][0]
                    finalcube[eg1[i+1]][3],temp[1] = temp[1],finalcube[eg1[i+1]][3]
                    finalcube[eg1[i+1]][6],temp[2] = temp[2],finalcube[eg1[i+1]][6]
                elif i == 1:
                    finalcube[eg1[i+1]][6],temp[0] = temp[0],finalcube[eg1[i+1]][6]
                    finalcube[eg1[i+1]][7],temp[1] = temp[1],finalcube[eg1[i+1]][7]
                    finalcube[eg1[i+1]][8],temp[2] = temp[2],finalcube[eg1[i+1]][8]
                elif i == 2:
                    finalcube[eg1[i+1]][8],temp[0] = temp[0],finalcube[eg1[i+1]][8]
                    finalcube[eg1[i+1]][5],temp[1] = temp[1],finalcube[eg1[i+1]][5]
                    finalcube[eg1[i+1]][2],temp[2] = temp[2],finalcube[eg1[i+1]][2]
                elif i == 3:
                    finalcube[eg1[0]][2],temp[0] = temp[0],finalcube[eg1[0]][2]
                    finalcube[eg1[0]][1],temp[1] = temp[1],finalcube[eg1[0]][1]
                    finalcube[eg1[0]][0],temp[2] = temp[2],finalcube[eg1[0]][0]
            temp[0] = finalcube[bottom][0]
            temp[1] = finalcube[bottom][1]
            temp[2] = finalcube[bottom][2]
            finalcube[bottom][2],temp[0] = temp[0],finalcube[bottom][2]
            finalcube[bottom][5],temp[1] = temp[1],finalcube[bottom][5]
            finalcube[bottom][8],temp[2] = temp[2],finalcube[bottom][8]
            finalcube[bottom][7],temp[1] = temp[1],finalcube[bottom][7]
            finalcube[bottom][6],temp[2] = temp[2],finalcube[bottom][6]
            finalcube[bottom][3],temp[1] = temp[1],finalcube[bottom][3]
            finalcube[bottom][0],temp[2] = temp[2],finalcube[bottom][0]
            finalcube[bottom][1],temp[1] = temp[1],finalcube[bottom][1]
            finalcube[bottom][2],temp[2] = temp[2],finalcube[bottom][2]
        elif movetype == '2i':
            temp[0] = finalcube[eg1[0]][6]
            temp[1] = finalcube[eg1[0]][7]
            temp[2] = finalcube[eg1[0]][8]
            for i in range(0, 4):
                if i == 0:
                    finalcube[eg1[i+1]][8],temp[0] = temp[0],finalcube[eg1[i+1]][8]
                    finalcube[eg1[i+1]][5],temp[1] = temp[1],finalcube[eg1[i+1]][5]
                    finalcube[eg1[i+1]][2],temp[2] = temp[2],finalcube[eg1[i+1]][2]
                elif i == 1:
                    finalcube[eg1[i+1]][2],temp[0] = temp[0],finalcube[eg1[i+1]][2]
                    finalcube[eg1[i+1]][1],temp[1] = temp[1],finalcube[eg1[i+1]][1]
                    finalcube[eg1[i+1]][0],temp[2] = temp[2],finalcube[eg1[i+1]][0]
                elif i == 2:
                    finalcube[eg1[i+1]][0],temp[0] = temp[0],finalcube[eg1[i+1]][0]
                    finalcube[eg1[i+1]][3],temp[1] = temp[1],finalcube[eg1[i+1]][3]
                    finalcube[eg1[i+1]][6],temp[2] = temp[2],finalcube[eg1[i+1]][6]
                elif i == 3:
                    finalcube[eg1[0]][6],temp[0] = temp[0],finalcube[eg1[0]][6]
                    finalcube[eg1[0]][7],temp[1] = temp[1],finalcube[eg1[0]][7]
                    finalcube[eg1[0]][8],temp[2] = temp[2],finalcube[eg1[0]][8]
            temp[0] = finalcube[top][2]
            temp[1] = finalcube[top][1]
            temp[2] = finalcube[top][0]
            finalcube[top][0],temp[0] = temp[0],finalcube[top][0]
            finalcube[top][3],temp[1] = temp[1],finalcube[top][3]
            finalcube[top][6],temp[2] = temp[2],finalcube[top][6]
            finalcube[top][7],temp[1] = temp[1],finalcube[top][7]
            finalcube[top][8],temp[2] = temp[2],finalcube[top][8]
            finalcube[top][5],temp[1] = temp[1],finalcube[top][5]
            finalcube[top][2],temp[2] = temp[2],finalcube[top][2]
            finalcube[top][1],temp[1] = temp[1],finalcube[top][1]
            finalcube[top][0],temp[2] = temp[2],finalcube[top][0]
            temp[0] = finalcube[eg1[0]][5]
            temp[1] = finalcube[eg1[0]][4]
            temp[2] = finalcube[eg1[0]][3]
            for i in range(0, 4):
                if i == 0:
                    finalcube[eg1[i+1]][1],temp[0] = temp[0],finalcube[eg1[i+1]][1]
                    finalcube[eg1[i+1]][4],temp[1] = temp[1],finalcube[eg1[i+1]][4]
                    finalcube[eg1[i+1]][7],temp[2] = temp[2],finalcube[eg1[i+1]][7]
                elif i == 1:
                    finalcube[eg1[i+1]][3],temp[0] = temp[0],finalcube[eg1[i+1]][3]
                    finalcube[eg1[i+1]][4],temp[1] = temp[1],finalcube[eg1[i+1]][4]
                    finalcube[eg1[i+1]][5],temp[2] = temp[2],finalcube[eg1[i+1]][5]
                elif i == 2:
                    finalcube[eg1[i+1]][7],temp[0] = temp[0],finalcube[eg1[i+1]][7]
                    finalcube[eg1[i+1]][4],temp[1] = temp[1],finalcube[eg1[i+1]][4]
                    finalcube[eg1[i+1]][1],temp[2] = temp[2],finalcube[eg1[i+1]][1]
                elif i == 3:
                    finalcube[eg1[0]][5],temp[0] = temp[0],finalcube[eg1[0]][5]
                    finalcube[eg1[0]][4],temp[1] = temp[1],finalcube[eg1[0]][4]
                    finalcube[eg1[0]][3],temp[2] = temp[2],finalcube[eg1[0]][3]
            temp[0] = finalcube[eg1[0]][2]
            temp[1] = finalcube[eg1[0]][1]
            temp[2] = finalcube[eg1[0]][0]
            for i in range(0, 4):
                if i == 0:
                    finalcube[eg1[i+1]][0],temp[0] = temp[0],finalcube[eg1[i+1]][0]
                    finalcube[eg1[i+1]][3],temp[1] = temp[1],finalcube[eg1[i+1]][3]
                    finalcube[eg1[i+1]][6],temp[2] = temp[2],finalcube[eg1[i+1]][6]
                elif i == 1:
                    finalcube[eg1[i+1]][6],temp[0] = temp[0],finalcube[eg1[i+1]][6]
                    finalcube[eg1[i+1]][7],temp[1] = temp[1],finalcube[eg1[i+1]][7]
                    finalcube[eg1[i+1]][8],temp[2] = temp[2],finalcube[eg1[i+1]][8]
                elif i == 2:
                    finalcube[eg1[i+1]][8],temp[0] = temp[0],finalcube[eg1[i+1]][8]
                    finalcube[eg1[i+1]][5],temp[1] = temp[1],finalcube[eg1[i+1]][5]
                    finalcube[eg1[i+1]][2],temp[2] = temp[2],finalcube[eg1[i+1]][2]
                elif i == 3:
                    finalcube[eg1[0]][2],temp[0] = temp[0],finalcube[eg1[0]][2]
                    finalcube[eg1[0]][1],temp[1] = temp[1],finalcube[eg1[0]][1]
                    finalcube[eg1[0]][0],temp[2] = temp[2],finalcube[eg1[0]][0]
            temp[0] = finalcube[bottom][0]
            temp[1] = finalcube[bottom][1]
            temp[2] = finalcube[bottom][2]
            finalcube[bottom][2],temp[0] = temp[0],finalcube[bottom][2]
            finalcube[bottom][5],temp[1] = temp[1],finalcube[bottom][5]
            finalcube[bottom][8],temp[2] = temp[2],finalcube[bottom][8]
            finalcube[bottom][7],temp[1] = temp[1],finalcube[bottom][7]
            finalcube[bottom][6],temp[2] = temp[2],finalcube[bottom][6]
            finalcube[bottom][3],temp[1] = temp[1],finalcube[bottom][3]
            finalcube[bottom][0],temp[2] = temp[2],finalcube[bottom][0]
            finalcube[bottom][1],temp[1] = temp[1],finalcube[bottom][1]
            finalcube[bottom][2],temp[2] = temp[2],finalcube[bottom][2]
            temp[0] = finalcube[eg1[0]][6]
            temp[1] = finalcube[eg1[0]][7]
            temp[2] = finalcube[eg1[0]][8]
            for i in range(0, 4):
                if i == 0:
                    finalcube[eg1[i+1]][8],temp[0] = temp[0],finalcube[eg1[i+1]][8]
                    finalcube[eg1[i+1]][5],temp[1] = temp[1],finalcube[eg1[i+1]][5]
                    finalcube[eg1[i+1]][2],temp[2] = temp[2],finalcube[eg1[i+1]][2]
                elif i == 1:
                    finalcube[eg1[i+1]][2],temp[0] = temp[0],finalcube[eg1[i+1]][2]
                    finalcube[eg1[i+1]][1],temp[1] = temp[1],finalcube[eg1[i+1]][1]
                    finalcube[eg1[i+1]][0],temp[2] = temp[2],finalcube[eg1[i+1]][0]
                elif i == 2:
                    finalcube[eg1[i+1]][0],temp[0] = temp[0],finalcube[eg1[i+1]][0]
                    finalcube[eg1[i+1]][3],temp[1] = temp[1],finalcube[eg1[i+1]][3]
                    finalcube[eg1[i+1]][6],temp[2] = temp[2],finalcube[eg1[i+1]][6]
                elif i == 3:
                    finalcube[eg1[0]][6],temp[0] = temp[0],finalcube[eg1[0]][6]
                    finalcube[eg1[0]][7],temp[1] = temp[1],finalcube[eg1[0]][7]
                    finalcube[eg1[0]][8],temp[2] = temp[2],finalcube[eg1[0]][8]
            temp[0] = finalcube[top][2]
            temp[1] = finalcube[top][1]
            temp[2] = finalcube[top][0]
            finalcube[top][0],temp[0] = temp[0],finalcube[top][0]
            finalcube[top][3],temp[1] = temp[1],finalcube[top][3]
            finalcube[top][6],temp[2] = temp[2],finalcube[top][6]
            finalcube[top][7],temp[1] = temp[1],finalcube[top][7]
            finalcube[top][8],temp[2] = temp[2],finalcube[top][8]
            finalcube[top][5],temp[1] = temp[1],finalcube[top][5]
            finalcube[top][2],temp[2] = temp[2],finalcube[top][2]
            finalcube[top][1],temp[1] = temp[1],finalcube[top][1]
            finalcube[top][0],temp[2] = temp[2],finalcube[top][0]
            temp[0] = finalcube[eg1[0]][5]
            temp[1] = finalcube[eg1[0]][4]
            temp[2] = finalcube[eg1[0]][3]
            for i in range(0, 4):
                if i == 0:
                    finalcube[eg1[i+1]][1],temp[0] = temp[0],finalcube[eg1[i+1]][1]
                    finalcube[eg1[i+1]][4],temp[1] = temp[1],finalcube[eg1[i+1]][4]
                    finalcube[eg1[i+1]][7],temp[2] = temp[2],finalcube[eg1[i+1]][7]
                elif i == 1:
                    finalcube[eg1[i+1]][3],temp[0] = temp[0],finalcube[eg1[i+1]][3]
                    finalcube[eg1[i+1]][4],temp[1] = temp[1],finalcube[eg1[i+1]][4]
                    finalcube[eg1[i+1]][5],temp[2] = temp[2],finalcube[eg1[i+1]][5]
                elif i == 2:
                    finalcube[eg1[i+1]][7],temp[0] = temp[0],finalcube[eg1[i+1]][7]
                    finalcube[eg1[i+1]][4],temp[1] = temp[1],finalcube[eg1[i+1]][4]
                    finalcube[eg1[i+1]][1],temp[2] = temp[2],finalcube[eg1[i+1]][1]
                elif i == 3:
                    finalcube[eg1[0]][5],temp[0] = temp[0],finalcube[eg1[0]][5]
                    finalcube[eg1[0]][4],temp[1] = temp[1],finalcube[eg1[0]][4]
                    finalcube[eg1[0]][3],temp[2] = temp[2],finalcube[eg1[0]][3]
            temp[0] = finalcube[eg1[0]][2]
            temp[1] = finalcube[eg1[0]][1]
            temp[2] = finalcube[eg1[0]][0]
            for i in range(0, 4):
                if i == 0:
                    finalcube[eg1[i+1]][0],temp[0] = temp[0],finalcube[eg1[i+1]][0]
                    finalcube[eg1[i+1]][3],temp[1] = temp[1],finalcube[eg1[i+1]][3]
                    finalcube[eg1[i+1]][6],temp[2] = temp[2],finalcube[eg1[i+1]][6]
                elif i == 1:
                    finalcube[eg1[i+1]][6],temp[0] = temp[0],finalcube[eg1[i+1]][6]
                    finalcube[eg1[i+1]][7],temp[1] = temp[1],finalcube[eg1[i+1]][7]
                    finalcube[eg1[i+1]][8],temp[2] = temp[2],finalcube[eg1[i+1]][8]
                elif i == 2:
                    finalcube[eg1[i+1]][8],temp[0] = temp[0],finalcube[eg1[i+1]][8]
                    finalcube[eg1[i+1]][5],temp[1] = temp[1],finalcube[eg1[i+1]][5]
                    finalcube[eg1[i+1]][2],temp[2] = temp[2],finalcube[eg1[i+1]][2]
                elif i == 3:
                    finalcube[eg1[0]][2],temp[0] = temp[0],finalcube[eg1[0]][2]
                    finalcube[eg1[0]][1],temp[1] = temp[1],finalcube[eg1[0]][1]
                    finalcube[eg1[0]][0],temp[2] = temp[2],finalcube[eg1[0]][0]
            temp[0] = finalcube[bottom][0]
            temp[1] = finalcube[bottom][1]
            temp[2] = finalcube[bottom][2]
            finalcube[bottom][2],temp[0] = temp[0],finalcube[bottom][2]
            finalcube[bottom][5],temp[1] = temp[1],finalcube[bottom][5]
            finalcube[bottom][8],temp[2] = temp[2],finalcube[bottom][8]
            finalcube[bottom][7],temp[1] = temp[1],finalcube[bottom][7]
            finalcube[bottom][6],temp[2] = temp[2],finalcube[bottom][6]
            finalcube[bottom][3],temp[1] = temp[1],finalcube[bottom][3]
            finalcube[bottom][0],temp[2] = temp[2],finalcube[bottom][0]
            finalcube[bottom][1],temp[1] = temp[1],finalcube[bottom][1]
            finalcube[bottom][2],temp[2] = temp[2],finalcube[bottom][2]
class check:
    def checkcross():
        tf = False
        if finalcube[down][1] == finalcube[down][3] == finalcube[down][4] == finalcube[down][5] == finalcube[down][7] and finalcube[top][4] == finalcube[top][7]  and finalcube[left][4] == finalcube[left][7] and finalcube[right][4] == finalcube[right][7]  and finalcube[bottom][4] == finalcube[bottom][7]:
            tf = True
        else:
            tf = False
        if tf == False:
            return 'No'
        else:
            return 'Yes'
        #not in place
        #nor on bottom   
    def checkoll():
        for i in range(0, 9):
            tf = True
            if finalcube[up][4] == finalcube[up][i] and tf == True:
                tf = True
            else:
                tf = False
                break
        if tf == True:
            return 'Yes'
        else:
            return 'No'
    def checkf2l():
        for i in range(0, 4):
            tf = True
            if finalcube[eg2[i]][4] == finalcube[eg2[i]][3] == finalcube[eg2[i]][6] == finalcube[eg2[i]][5] == finalcube[eg2[i]][8] and tf == True:
                tf = True
            else:
                tf = False
                break
        if tf == True:
            return 'Yes'
        else:
            return 'No'
    def checkpll():
        for i in range(0, 4):
            tf = True
            if finalcube[eg2[i]][4] == finalcube[eg2[i]][0] == finalcube[eg2[i]][1] == finalcube[eg2[i]][2] and tf == True:
                tf = True
            else:
                tf = False
                break
        if tf == True:
            return 'Yes'
        else:
            return 'No'
def algorithm(alg):
    for i in range(0, len(alg)):
        if len(alg[i]) == 1:
            if alg[i][0] == 'R':
                moves.R(1)
            elif alg[i][0] == 'L':
                moves.L(1)
            elif alg[i][0] == 'U':
                moves.U(1)
            elif alg[i][0] == 'F':
                moves.F(1)
            elif alg[i][0] == 'B':
                moves.B(1)
            elif alg[i][0] == 'D':
                moves.D(1)
            elif alg[i][0] == 'S':
                moves.S(1)
            elif alg[i][0] == 'E':
                moves.E(1)
            elif alg[i][0] == 'M':
                moves.M(1)
            elif alg[i][0] == 'X':
                moves.X(1)
            elif alg[i][0] == 'Y':
                moves.Y(1)
            elif alg[i][0] == 'Z':
                moves.Z(1)
        elif len(alg[i]) == 2:
            if alg[i][1] == 'i':
                if alg[i][0] == 'R':
                    moves.R('1i')
                elif alg[i][0] == 'L':
                    moves.L('1i')
                elif alg[i][0] == 'U':
                    moves.U('1i')
                elif alg[i][0] == 'F':
                    moves.F('1i')
                elif alg[i][0] == 'B':
                    moves.B('1i')
                elif alg[i][0] == 'D':
                    moves.D('1i')
                elif alg[i][0] == 'S':
                    moves.S('1i')
                elif alg[i][0] == 'E':
                    moves.E('1i')
                elif alg[i][0] == 'M':
                    moves.M('1i')
                elif alg[i][0] == 'X':
                    moves.X('1i')
                elif alg[i][0] == 'Y':
                    moves.Y('1i')
                elif alg[i][0] == 'Z':
                    moves.Z('1i')
            elif alg[i][1] == '2':
                if alg[i][0] == 'R':
                    moves.R(2)
                elif alg[i][0] == 'L':
                    moves.L(2)
                elif alg[i][0] == 'U':
                    moves.U(2)
                elif alg[i][0] == 'F':
                    moves.F(2)
                elif alg[i][0] == 'B':
                    moves.B(2)
                elif alg[i][0] == 'D':
                    moves.D(2)
                elif alg[i][0] == 'S':
                    moves.S(2)
                elif alg[i][0] == 'E':
                    moves.E(2)
                elif alg[i][0] == 'M':
                    moves.M(2)
                elif alg[i][0] == 'X':
                    moves.X(2)
                elif alg[i][0] == 'Y':
                    moves.Y(2)
                elif alg[i][0] == 'Z':
                    moves.Z(2)
        elif len(alg[i]) == 3:
            if alg[i][0] == 'R':
                moves.R('2i')
            elif alg[i][0] == 'L':
                moves.L('2i')
            elif alg[i][0] == 'U':
                moves.U('2i')
            elif alg[i][0] == 'F':
                moves.F('2i')
            elif alg[i][0] == 'B':
                moves.B('2i')
            elif alg[i][0] == 'D':
                moves.D('2i')
            elif alg[i][0] == 'S':
                moves.S('2i')
            elif alg[i][0] == 'E':
                moves.E('2i')
            elif alg[i][0] == 'M':
                moves.M('2i')
            elif alg[i][0] == 'X':
                moves.X('2i')
            elif alg[i][0] == 'Y':
                moves.Y('2i')
            elif alg[i][0] == 'Z':
                moves.Z('2i')
    for i in alg:
        ans.append(i)
class pll:
    def Uaperm():
        algorithm(['R', 'Ui', 'R', 'U', 'R', 'U', 'R', 'Ui', 'Ri','Ui', 'R2'])
    def Ubperm():
        algorithm(['R2', 'U', 'R', 'U', 'Ri', 'Ui', 'Ri', 'Ui', 'Ri', 'U', 'Ri'])
    def Zperm():
        algorithm(['M2i', 'U', 'M2i', 'U', 'Mi', 'U2', 'M2i', 'U2', 'Mi', 'U2'])
    def Hperm():
        algorithm(['M2i', 'U', 'M2i', 'U2', 'M2i', 'U', 'M2i'])
    def Aaperm():
        algorithm(['X', 'Ri', 'U', 'Ri', 'D2', 'R', 'Ui', 'R', 'D2', 'R2', 'Xi'] )
    def Abperm():
        algorithm(['X', 'R2i', 'D2', 'R', 'U', 'Ri', 'D2', 'R', 'Ui', 'R', 'Xi'] )
    def Eperm():
        algorithm( ['Xi', 'R', 'Ui', 'Ri', 'D', 'R', 'U', 'Ri', 'Di', 'R', 'U', 'Ri', 'D', 'R', 'Ui', 'Ri', 'Di', 'X'] )
    def Raperm():
        algorithm( ['R', 'Ui', 'Ri', 'Ui', 'R', 'U', 'R', 'D', 'Ri', 'Ui', 'R', 'Di', 'Ri', 'U2', 'Ri', 'Ui'] )
    def Rbperm():
        algorithm( ['Ri', 'U2', 'R', 'U2i', 'Ri', 'F', 'R', 'U', 'Ri', 'U', 'Ri', 'Fi', 'R2', 'Ui'] )
    def Japerm():
        algorithm( ['Ri', 'U', 'Li', 'U2', 'R', 'Ui', 'Ri', 'U2', 'R', 'L', 'Ui'] )
    def Jbperm():
        algorithm(['R', 'U', 'Ri', 'Fi', 'R', 'U', 'Ri', 'Ui', 'Ri', 'F', 'R2', 'Ui', 'Ri', 'Ui'] )
    def Tperm():
        algorithm( ['R', 'U', 'Ri', 'Ui', 'Ri', 'F', 'R2', 'Ui', 'Ri', 'Ui', 'R', 'U', 'Ri', 'Fi'] )
    def Fperm():
        algorithm( ['Ri', 'Ui', 'Fi', 'R', 'U', 'Ri', 'Ui', 'Ri', 'F', 'R2', 'Ui', 'Ri', 'Ui', 'R', 'U', 'Ri', 'U', 'R'] )
    def Vperm():
        algorithm( ['Ri', 'U', 'Ri', 'Ui', 'Y', 'Ri', 'Fi', 'R2', 'Ui', 'Ri', 'U', 'Ri', 'F', 'R', 'F'] )
    def Yperm():
        algorithm( ['F', 'R', 'Ui', 'Ri', 'Ui', 'R', 'U', 'Ri', 'Fi', 'R', 'U', 'Ri', 'Ui', 'Ri', 'F', 'R', 'Fi'] )
    def Naperm():
        algorithm( ['R', 'U', 'Ri', 'U', 'R', 'U', 'Ri', 'Fi', 'R', 'U', 'Ri', 'Ui', 'Ri', 'F', 'R2', 'Ui', 'Ri', 'U2', 'R', 'Ui', 'Ri'] )
    def Nbperm():
        algorithm( ['Ri', 'U', 'R', 'Ui', 'Ri', 'Fi', 'Ui', 'F', 'R', 'U', 'Ri', 'F', 'Ri', 'Fi', 'R', 'Ui', 'R'] )
    def Gaperm():
        algorithm( ['R2', 'U', 'Ri', 'U', 'Ri', 'Ui', 'R', 'Ui', 'R2', 'D', 'Ui', 'Ri', 'U', 'R', 'Di', 'U'] )
    def Gbperm():
        algorithm( ['Fi', 'Ui', 'F', 'R2', 'U', 'Ei', 'Ri', 'U', 'R', 'Ui', 'R', 'Ui', 'E', 'R2i'] )
    def Gcperm():
        algorithm( ['R2', 'Ui', 'R', 'Ui', 'R', 'U', 'Ri', 'U', 'R2', 'Di', 'U', 'R', 'Ui', 'Ri', 'D', 'Ui'] )
    def Gdperm():
        algorithm( ['Di', 'R', 'U', 'Ri', 'Ui', 'D', 'R2', 'Ui', 'R', 'Ui', 'Ri', 'U', 'Ri', 'U', 'R2', 'U'] )
    def isbar():
        for i in range(0,4):
            if finalcube[eg2[i]][0] == finalcube[eg2[i]][1] == finalcube[eg2[i]][2]:
                tf = True
                return True
                break
            else:
                tf = False
        if tf == False:
            return False
    def isheadlight():
        count = 0
        for i in range(0,4):
            if finalcube[eg2[i]][0] != finalcube[eg2[i]][1] and finalcube[eg2[i]][0] == finalcube[eg2[i]][2]:
                count += 1
        if count == 1:
            return True
        else:
            return False
    def isnone():
        if pll.isbar() == False and pll.isheadlight() == False:
            return True
        else:
            return False
    def numheadlight():
        for i in range(0,4):
            if finalcube[eg2[i]][0] != finalcube[eg2[i]][1] and finalcube[eg2[i]][0] == finalcube[eg2[i]][2]:
                count += 1
        return count
algorithm(['Ui', 'Li', 'D2', 'F','D','L2','Ui','Ri','L2','Di','F2','Ui','R2','F2','L2','B2','Ui','L'])
#class cross:
#class oll:
#class f2l:
class driver:
    def pll():
        if pll.isbar() == True:
            if numheadlight() == 3:
                if
                elif
            elif
            elif
            elif
        elif pll.isheadlight() == True
        elif pll.isnone() == True
#    def oll():
#    def f2l():
 #   def cross:
#solvedcube = {up = [up,up,up,up,up], left = [left,left,left,left,left,left], right = [right,right,right,right,right,right], top = [top,top,top,top,top,top],bottom = [bottom,bottom,bottom,bottom,bottom,bottom],down = [down,down,down,down,down,down]}
#while cmp(finalcube, solvedcube) != 0:
   # if check.checkpll == 'No':
    
