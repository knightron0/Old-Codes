print("First, enter the color scheme of the puzzle")
top = input("Enter the color facing you:")
bottom = input("Enter the color opposite to the color facing you:")
right = input("Enter the color to the right of the color facing you:")
left = input("Enter the color to the left of the color facing you:")
up = input("Enter the color facing up:")
down = input("Enter the color opposite to the color facing up:")
cube = {}
count = 0
eg1 = [up, left, down, right]
eg2 = [top, left, bottom, right]
eg3 = [top, left, bottom, right]
eg4 = [up, top, down, bottom]
eg5 = [up, top, down, bottom]
eg6 = [up, left, down, right]
for i in top, up, down, left, right, bottom:
    # get data for each of the six faces now
    cube[i] = {}
    cube[i]["edges"] = []
    cube[i]["corners"] = []
for i in top, up, down, left, right, bottom:
    count += 1
    print("Please tell the edges and corners of the",i,"face")
    for j in range(0, 4):
        if count == 1:
            cube[i]["edges"].append(input("Enter the edge with the " + eg1[j] + " face:"))
            if j == 3:
                cube[i]["corners"].append(input("Enter the corner with the " + eg1[j] + "," + eg1[0] + " face:"))
            else:
                cube[i]["corners"].append(input("Enter the corner with the " + eg1[j] + "," + eg1[j+1] + " face:"))
        elif count == 2:
            cube[i]["edges"].append(input("Enter the edge with the " + eg2[j] + " face:"))
            if j == 3:
                cube[i]["corners"].append(input("Enter the corner with the " + eg2[j] + "," + eg2[0] + " face:"))
            else:
                cube[i]["corners"].append(input("Enter the corner with the " + eg2[j] + "," + eg2[j+1] + " face:"))
        elif count == 3:
            cube[i]["edges"].append(input("Enter the edge with the " + eg3[j] + " face:"))
            if j == 3:
                cube[i]["corners"].append(input("Enter the corner with the " + eg3[j] + "," + eg3[0] + " face:"))
            else:
                cube[i]["corners"].append(input("Enter the corner with the " + eg3[j] + "," + eg3[j+1] + " face:"))
        elif count == 4:
            cube[i]["edges"].append(input("Enter the edge with the " + eg4[j] + " face:"))
            if j == 3:
                cube[i]["corners"].append(input("Enter the corner with the " + eg4[j] + "," + eg4[0] + " face:"))
            else:
                cube[i]["corners"].append(input("Enter the corner with the " + eg4[j] + "," + eg4[j+1] + " face:"))
        elif count == 5:
            cube[i]["edges"].append(input("Enter the edge with the " + eg5[j] + " face:"))
            if j == 3:
                cube[i]["corners"].append(input("Enter the corner with the " + eg5[j] + "," + eg5[0] + " face:"))
            else:
                cube[i]["corners"].append(input("Enter the corner with the " + eg5[j] + "," + eg5[j+1] + " face:"))
        elif count == 6:
            cube[i]["edges"].append(input("Enter the edge with the " + eg6[j] + " face:"))
            if j == 3:
                cube[i]["corners"].append(input("Enter the corner with the " + eg6[j] + "," + eg6[0] + " face:"))
            else:
                cube[i]["corners"].append(input("Enter the corner with the " + eg6[j] + "," +  eg6[j+1] + " face:"))
print("      "+cube[up]["corners"][1],cube[up]["edges"][2],cube[up]["corners"][2])
print("      "+cube[up]["edges"][1], up,cube[up]["edges"][3])
print("      "+cube[up]["corners"][0],cube[up]["edges"][0],cube[up]["corners"][3])
print(cube[left]["corners"][3],cube[left]["edges"][0],cube[left]["corners"][0] , cube[top]["corners"][0],cube[top]["edges"][0],cube[top]["corners"][3], cube[right]["corners"][0],cube[right]["edges"][0],cube[right]["corners"][3], cube[bottom]["corners"][3],cube[bottom]["edges"][0],cube[bottom]["corners"][0])
print(cube[left]["edges"][3], left,cube[left]["edges"][1]                      ,cube[top]["edges"][1], top,cube[top]["edges"][3]                     , cube[right]["edges"][1], right,cube[right]["edges"][3]                      ,cube[bottom]["edges"][3], bottom,cube[bottom]["edges"][1])
print(cube[left]["corners"][2],cube[left]["edges"][2],cube[left]["corners"][1] , cube[top]["corners"][1],cube[top]["edges"][2],cube[top]["corners"][2], cube[right]["corners"][1],cube[right]["edges"][2],cube[right]["corners"][2], cube[bottom]["corners"][2],cube[bottom]["edges"][2],cube[bottom]["corners"][1])
print("      "+cube[down]["corners"][0],cube[down]["edges"][0],cube[down]["corners"][3])
print("      "+cube[down]["edges"][1], down,cube[down]["edges"][3])
print("      "+cube[down]["corners"][1],cube[down]["edges"][2],cube[down]["corners"][2])
finalcube = {}
for i in top, up, down, left, right, bottom:
    finalcube[i] = []
for i in range(0,6):
    if i == 0:
        finalcube[up].append(cube[up]["corners"][1])
        finalcube[up].append(cube[up]["edges"][2])
        finalcube[up].append(cube[up]["corners"][2])
        finalcube[up].append(cube[up]["edges"][1])
        finalcube[up].append(up)
        finalcube[up].append(cube[up]["edges"][3])
        finalcube[up].append(cube[up]["corners"][0])
        finalcube[up].append(cube[up]["edges"][0])
        finalcube[up].append(cube[up]["corners"][3])
    elif i == 1:
        finalcube[left].append(cube[left]["corners"][3])
        finalcube[left].append(cube[left]["edges"][0])
        finalcube[left].append(cube[left]["corners"][0])
        finalcube[left].append(cube[left]["edges"][3])
        finalcube[left].append(left)
        finalcube[left].append(cube[left]["edges"][1])
        finalcube[left].append(cube[left]["corners"][2])
        finalcube[left].append(cube[left]["edges"][2])
        finalcube[left].append(cube[left]["corners"][1])
    elif i == 2:
        finalcube[top].append(cube[top]["corners"][0])
        finalcube[top].append(cube[top]["edges"][0])
        finalcube[top].append(cube[top]["corners"][3])
        finalcube[top].append(cube[top]["edges"][1])
        finalcube[top].append(top)
        finalcube[top].append(cube[top]["edges"][3])
        finalcube[top].append(cube[top]["corners"][1])
        finalcube[top].append(cube[top]["edges"][2])
        finalcube[top].append(cube[top]["corners"][2])
    elif i == 3:
        finalcube[right].append(cube[right]["corners"][0])
        finalcube[right].append(cube[right]["edges"][0])
        finalcube[right].append(cube[right]["corners"][3])
        finalcube[right].append(cube[right]["edges"][1])
        finalcube[right].append(right)
        finalcube[right].append(cube[right]["edges"][3])
        finalcube[right].append(cube[right]["corners"][1])
        finalcube[right].append(cube[right]["edges"][2])
        finalcube[right].append(cube[right]["corners"][2])
    elif i == 4:
        finalcube[bottom].append(cube[bottom]["corners"][3])
        finalcube[bottom].append(cube[bottom]["edges"][0])
        finalcube[bottom].append(cube[bottom]["corners"][0])
        finalcube[bottom].append(cube[bottom]["edges"][3])
        finalcube[bottom].append(bottom)
        finalcube[bottom].append(cube[bottom]["edges"][1])
        finalcube[bottom].append(cube[bottom]["corners"][2])
        finalcube[bottom].append(cube[bottom]["edges"][2])
        finalcube[bottom].append(cube[bottom]["corners"][1])
    elif i == 5:
        finalcube[down].append(cube[down]["corners"][0])
        finalcube[down].append(cube[down]["edges"][0])
        finalcube[down].append(cube[down]["corners"][3])
        finalcube[down].append(cube[down]["edges"][1])
        finalcube[down].append(down)
        finalcube[down].append(cube[down]["edges"][3])
        finalcube[down].append(cube[down]["corners"][1])
        finalcube[down].append(cube[down]["edges"][2])
        finalcube[down].append(cube[down]["corners"][2])


